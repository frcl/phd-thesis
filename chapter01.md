# Theory of elasticity
In the following chapter concepts from elasticity theory that will be used throughout this text are introduced.
We start from an atomistic view of displacements and lattice vibrations,
then move on to develop an understanding of continuum mechanics.
Many of these concepts can also be found in classic textbooks
such as [@LandauLifshitz7],
but also some non-standard notation used in later chapters is introduced here.

## Lattice oscillations in the harmonic approximation
In an idealized picture, a crystal consists of a perfectly regular lattice of atoms.
Classically, we describe the positions of atoms in the solid with a vector $\r_n$, with some label $n$.
Regularity here means it can be created by repetition of an arrangement of atoms,
called the *unit cell*.
If the arrangement with the smallest number of atom that can be used to create the lattice in this way,
called the *primitive unit cell*, still contains more than one atom,
it is convenient to label the positions as $\r_{n,\alpha}$,
where the Greek letter indices label the atoms within one unit cell,
while the index $n$ labels the unit cells within the crystal.
The vector decomposes as follows
$$
    \r_{n,\alpha} = \R_n + \T_\alpha + \u_{n,\alpha} \;.
$$
$\R$ refers to the *Bravais lattice* vectors, which in turn can be decomposed in basis vectors
$$
    \R_n \equiv \R_{n_a,n_b,n_c} = n_a \vec{a} + n_b \vec{b} + n_c \vec{c}
$$
(in three dimensions), while $\T$ refers to the equilibrium positions of the atoms within the unit cell
and $\u$ is the deviation from the equilibrium position due to its momentary *displacement*.
<!--We call $\u$ the displacement vector.-->

The atoms have arranged themselves in this way because of some effective potential
in which they have found an equilibrium.
The displacements are merely small perturbations around the equilibrium arrangement.
We can model the system with the Hamiltonian
$$
    H = \sum_{n,\alpha} \frac{\p_{n,\alpha}^2}{2 m_\alpha} + U(\{\r\}) \;,
$$
where $U$ represents the effective potential, depending on the position of all atoms.
In general, the form of the potential is unknown and may be quite complicated.
However, assuming displacements are small compared to the lattice spacing, we expand $U$ in $\u$
$$
%\begin{aligned}
    U(\{\r\}) %&
        = U|_{\u = 0}
        %+ \sum_{n,\alpha} \left(\frac{\partial U}{\partial \r_{n,\alpha}}\right)_{\u = 0}
        %    \cdot \u_{n,\alpha} \\
        %&\quad
        + \frac12 \sum_{m,n,\alpha,\beta,i,j}
            \left(\frac{\partial^2 U}{\partial r_{m,\alpha,i} \partial r_{n,\beta,j}}\right)_{\u = 0}
                u_{m,\alpha,i} u_{n,\beta,j}
        + \mathcal{O}(u^3) \;.
%\end{aligned}
$$ {#eq:potential-expansion-quadratic}
The $0^\text{th}$ order is simply a constant and can be ignored in the following discussion.
The $1^\text{st}$ order would contradict the assumption that we expand around an equilibrium position
and therefore has to vanish,
so the first non-trivial order is the quadratic one.
We define the *Dynamical Matrix*
$$
    D_{mn,\alpha\beta,ij} = \frac{1}{\sqrt{m_\alpha m_\beta}}
        \left(\frac{\partial^2 U}{\partial r_{m,\alpha,i} \partial r_{n,\beta,j}}\right)_{\u=0} \;.
$$

If we stop the expansion at this point, we end up with a quadratic Hamiltonian.
We can then learn everything there is to know about the above Hamiltonian
by diagonalizing the dynamical matrix and looking at the eigenmodes,
effectively solving the classical equations of motion
$$
    \ddot{v}_{m,\alpha,i} = - \sum_{n,\beta,j} D_{mn,\alpha\beta,ij} v_{n,\beta,j}
$$ {#eq:crystal-equation-of-motion}
where we introduced rescaled displacements to remove the masses from the equation
$$
    v_{n,\alpha,i} = \sqrt{m_\alpha} u_{n,\alpha,i}\;.
$$
Assuming an infinite crystal or applying periodic boundary conditions,
we can reduce the size of the matrix that we have to diagonalize considerably
by using translational invariance with respect to Bravais lattice vectors
$$
    D_{mn,\alpha\beta,ij} = D_{\alpha\beta,ij}(\R_m-\R_n) \;,
$$
which enables us to define the Fourier transform of the dynamical matrix
$$
    D_{\alpha\beta,ij}(\q) = \sum_n D_{\alpha\beta,ij}(\R_n) e^{-i\q\cdot\R_n} \;.
$$
The result is a $(d\cdot\mu)\times(d\cdot\mu)$ matrix
(with $\mu$ atoms per unit cell)
for every $\q$ vector in the first Brillouin zone.
It has at most $d\cdot\mu$ distinct eigenmodes or *branches*.

To simplify notation, we combine the coordinate and atomic spices
to a single branch index $\lambda = (\alpha,i)$.
May $\omega_\lambda^2(\q)$ be the eigenvalues of $D(\q)$ and $\phi_\lambda(\q)$ the eigenvectors.
Then we can write
$$
    H = \frac12 \sum_{\q\in 1^\text{st}\text{BZ}} \sum_\lambda \left(\pi_\lambda(\q)\pi_\lambda(-\q)
        + \omega_\lambda^2(\q) \phi_\lambda(\q) \phi_\lambda(-\q)\right)
$$
using an appropriate definition of the canonical momentum $\pi$ in terms of $\p$
$$
    \boldsymbol\pi_\alpha(\q) = \sum_n \frac{\p_{n,\alpha}}{\sqrt{m_\alpha}} e^{-i\q\cdot\R_n} \;.
$$
Such a theory could now be quantized by declaring $\phi$ and $\pi$ to be field operators
and assuming canonical commutation relations.
For the purpose of this text however, the classical description will suffice.

In order to understand the character of the eigenmodes of the dynamical matrix,
let us think about a plane displacement wave traveling through the crystal.
Consider the equation of motion [@eq:crystal-equation-of-motion],
with the ansatz
$$
	v_{n,\alpha,i} = w^{(\lambda)}_{\alpha,i} e^{i\q\cdot\R_n} \;,
$$
where $w^{(\lambda)}_{\mu,i}$ is a polarization vector belonging to a certain branch $\lambda$.

If all atoms in the unit cell are displaced in phase,
such that $w$ no longer depends on $\alpha$,
the mode is called an acoustic mode.
From the equation of motion we then get
$$
    \omega_{\lambda}^2(\q) w^{(\lambda)}_i e^{i\q\cdot\R_n}
        = \sum_{m,\beta} D_{mn,\alpha\beta,ij} w^{(\lambda)}_j e^{i\q\cdot\R_m} \;.
$$
The above equation can be solved for $\omega_\lambda$
using the fact that $\mathrm{w}$ is an eigenvector of the $d\times d$-matrix $D_{mn,\alpha\beta}$
$$
    \omega_{\lambda}^2(\q) = \sum_{m,\beta} D_{mn,\alpha\beta,ij}
        e^{i\q\cdot\left(\R_m-\R_n\right)} \;.
$$ {#eq:acoustic-phonon-lattice-dispersion}
Our strategy is now to expand both sides in $\q$.
In $0^\mathrm{th}$ order we have
$$
    \omega_\lambda^2(0) =  \sum_{m,\beta,j} D_{mn,\alpha\beta,ij} = 0
$$ {#eq:acoustic-phonon-order-zero}
where in the last step it was used that a constant displacement
(independent of $m$ and $\beta$) can not cause a force in between the atoms,
since it represents simply a shift of the position of the crystal as a whole.
[@eq:acoustic-phonon-order-zero] shows that *the acoustic mode can not have a gap*.
This is a fundamental consequence of the fact
that the energy is invariant under global translations,
the symmetry for which the phonon is the *Goldstone mode*.

The $1^\text{st}$ order of the expansion of [@eq:acoustic-phonon-lattice-dispersion] vanishes for symmetry reasons.
In $2^\text{nd}$ order we have
$$
    \big(\nabla_{\q}\omega_\lambda(0) \cdot \q\big)^2 = - \frac12 \sum_{n,\beta,j} D_{mn,\alpha\beta,ij} ((\R_n-\R_m) \cdot \q)^2
$$ {#eq:acoustic-phonon-order-two}
where we usually write $(\nabla_{\q}\omega(0) \cdot \q)^2 = c_\lambda^2(\hat{\q}) q^2$
with the speed of sound $c_\lambda$.
[@eq:acoustic-phonon-order-two] shows that *the acoustic branches go linear with $q$ at small $q$*.
The speed of sound is then given by
$$
    c_\lambda^2(\hat{\q}) = -\sum_{\R\beta} D_{\alpha\beta,ii}(\R) (\R\cdot\hat{\q})^2
$$
For the optical branches a similar expansion is possible, but they acquire a gap in the $0^\mathrm{th}$ order.
The linear order on the other hand still vanishes with the same symmetry argument.
As a result we can write the following dispersion relations for a long wavelength theory
$$
    \omega_\gamma(\q) = \begin{cases}
        c_\gamma(\hat{\q}) q & \text{for acoustic branches} \\
        \sqrt{\omega_{0,\gamma}^2 + s_\gamma^2(\hat{\q}) q^2}\ & \text{for optical branches.}
    \end{cases}
$$
<!--TODO: make connection to continuum limit explicit?-->

## Continuum mechanics
In the context of a sufficiently long wavelength theory it is justified to "zoom out" and consider the continuum version of elasticity.
Here, the system is treated as a continuous medium rather than focusing on individual atomic displacements.
We analyze the shifts and distortions of small volume elements that contain many unit cells,
neglecting the relative displacements of atoms within a unit cell (optical modes) and focusing on the low-energy theory for acoustic modes.

### Displacement and strain
The displacement field $\u(\r)$ represents the displacement vector of a volume element, which at equilibrium would be at a point $\r$, but has been moved to a different point $\r'$
$$
    \r \to \r' = \r + \u(\r) \;.
$$
The displacement itself has a gauge degree of freedom that corresponds to global shifts,
since shifting the crystal as a whole should not change the fundamental physics happening inside the crystal.
Usually we choose a frame of reference where the volume average of $\u$ vanishes
$$
    \left\langle\u(\r)\right\rangle_\mathcal{V} = 0\;,
$$
which for a material of homogeneous mass density corresponds to the center of mass frame.
Therefore the quantities of interest are the derivatives of the displacement field.
The gradients of the displacement field $\partial_i u_j$ can be decomposed
into three physically meaningful components:
$$
    \partial_i u_j = \frac{1}{d} \nabla \cdot \u \;\delta_{ij}
        + \frac{1}{2} \left(\partial_i u_j + \partial_j u_i - \frac{2}{d} \nabla \cdot \u \;\delta_{ij}\right)
        + \frac{1}{2} (\partial_i u_j - \partial_j u_i)
$$
The first term represents dilation, the second term shear, and the third term rotation.
The *strain tensor*, which captures the first two terms, includes the symmetric part of the displacement gradient
$$
    \strain_{ij} = E_{ij} + \frac{1}{2} (\partial_i u_j + \partial_j u_i) \;.
	% TODO: u -> tilde u?
$$
where the diagonal components describe the dilation and the off-diagonal ones the shear.
Here, we made another decomposition of the strain into a spatially constant tensor $E$,
corresponding to a linear term in the displacement, and a purely local strain.
This allows us to apply the additional constraint
$$
    \left\langle\partial_i u_j + \partial_i u_j\right\rangle_\mathcal{V} = 0\;.
$$ {#eq:local-displacement-average-zero}
On the other hand, rotation is represented by the antisymmetric tensor
$$
    \omega_{ij} = \frac{1}{2} (\partial_i u_j - \partial_j u_i) \;.
$$ {#eq:elastic-rotations}
Here, we do not make the distinction between global and local rotations,
since the spatially constant part $\Omega = \left\langle \omega \right\rangle_\mathcal{V}$
represents a global rotation and this is required to be a symmetry.
In other words, in a consistent lattice theory the energy can not depend on $\Omega$.
<!--TODO: maybe cite papers by Keating?-->
Therefore we can enforce the constraint
$$
	\left\langle\partial_i u_j - \partial_i u_j\right\rangle_\mathcal{V} = 0\;.
$$
In combination with [@eq:local-displacement-average-zero]
we have established that $\left\langle\partial_i u_j\right\rangle_\mathcal{V} = 0$.

### Energy and stress
Linear elasticity is the continuum limit of a theory based on the harmonic approximation of the interatomic potential,
evoking the image of point-like masses connected by springs.
The equations governing such a model are therefore given by a generalized *Hooke's law*,
that connects the *stress tensor* $\sigma$, the equivalent of force,
to the strain tensor $\strain$, the continuum description of displacement,
through a set of generalized spring constants, given by the *stiffness tensor* $C_{ijkl}$
$$
    \sigma_{ij} = C_{ijkl} \strain_{kl} \;,
$$ {#eq:hooks-law}
where Einstein sum notation is implied.
This linear equation can be derived by minimizing a quadratic energy functional
$$
    f = \frac{1}{2} \strain_{ij} C_{ijkl} \strain_{kl} - \sigma_{ij} \strain_{ij}
$$ {#eq:continuum-lattice-energy}
using the condition
$$
    \frac{\delta f}{\delta \strain_{ij}} = 0 \;.
$$
For more compact notation and avoiding the introduction of an overwhelming amount of indices
we can use the $:$-product that contracts two indices of the involved tensors
$$
    f = \frac12\; \strain : C : \strain - \stress : \strain \;,
$$
where $(C:\strain)_{ij} = C_{ijkl} \strain_{kl}$, $\stress:\strain = \stress_{ij} \strain_{ij}$ and later we use $(C:D)_{ijkl} = C_{ijmn}D_{mnkl}$.

In the special case of an isotropic solid the energy can be expressed using only rotationally invariant quantities.
Since the strain tensor transforms like a tensor under rotations,
at the quadratic level there are only two such objects, which are $\tr\{\strain\}^2$ and $\tr\{\strain\strain\}$.
<!--TODO: maybe introduce lame parameters kappa and mu?-->
The energy is then typically parametrized as
$$
    f = \frac12 \left(K - \frac2d\, \mu\right) \tr\{\strain\}^2 + \mu \tr\{\strain\strain\} \;,
$$ {#eq:isotropic-strain-energy}
where $K$ is called bulk modulus and
$\mu$ is called shear modulus.
The corresponding stiffness tensor is of the form
$$
    C_{ijkl} = \left(K - \frac2d\, \mu\right) \delta_{ij} \delta_{kl}
        + \mu (\delta_{ik} \delta_{jl} + \delta_{jk} \delta_{il}) \;.
$$ {#eq:isotropic-stiffness-tensor}
Even in the presence of anisotropy the Bulk modulus can be defined as
$$
	K = \frac{1}{d^2} C_{iijj} \;.
$$ {#eq:bulk-modulus-definition}

<!--
   -There are many ways to parametrize the above energy in terms of elastic constants
   -TODO: connection with Poisson ratio $\nu = -s_{12}/s_{11}$ (with complience tensor $s = C^{-1}$) and Zener ratio $A = 2C_{44}/(C_{11} - C_{12})$
   -
   -complete polarizations
   -
   -* $C_{11}$ $C_{12}$ $C_{44}$
   -* $\kappa$ $\nu$ $A$
   -* $K$ $\mu$ $A$
   -->

### Voigt and Mandel notation

Taking a close look at the energy functional [@eq:continuum-lattice-energy] we notice that the stiffness tensor $C_{ijkl}$ necessarily has to satisfy the symmetries
$$
    C_{ijkl} = C_{jikl} = C_{klij} \;.
$$ {#eq:stiffness-tensor-symmetry}
The first equality is a consequence of the symmetry of the strain tensor.
In fact $\strain$ has only six independent components in three dimensions.
The idea of Voigt notation is to rewrite symmetric second-rank tensors as six component vectors
$$
    \strain^V = (\strain_{11}, \strain_{22}, \strain_{33}, 2\strain_{23}, 2\strain_{13}, 2\strain_{12})
$$ {#eq:voigt-vector}
effectively traversing the matrix in the pattern

\begin{center}
\input{inc/matrix_mnemonic} \;.
\end{center}

Consequently, fourth-rank tensors can then be written as $6 \times 6$-matrices
$$
    C^V = \begin{pmatrix}
        C_{1111} & C_{1122} & C_{1133} & C_{1123} & C_{1113} & C_{1112} \\
        C_{2211} & C_{2222} & C_{2233} & C_{2223} & C_{2213} & C_{2212} \\
        C_{3311} & C_{3322} & C_{3333} & C_{3323} & C_{3313} & C_{3312} \\
        C_{2311} & C_{2322} & C_{2333} & C_{2323} & C_{2313} & C_{2312} \\
        C_{1311} & C_{1322} & C_{1333} & C_{1323} & C_{1313} & C_{1312} \\
        C_{1211} & C_{1222} & C_{1233} & C_{1223} & C_{1213} & C_{1212} \\
    \end{pmatrix} \;.
$$
We will simply write $C_{ij} \equiv C^V_{ij}$. There is no ambiguity because of the different number of indices.
The second equality in [@eq:stiffness-tensor-symmetry] tells us that $C^V$ must also be a symmetric matrix, reducing the number of independent components further to a maximum of 21.
For specific crystal structures the corresponding symmetries allow more detailed statements about the form of such tensors.
<!--TODO: maybe cite source for specific forms?-->
For example, in a cubic crystal there are only three independent parameters $C_{11}$, $C_{12}$ and $C_{44}$.
The cubic anisotropy can then be quantified using the *Zener ratio*
$$
    A = \frac{2C_{44}}{C_{11} - C_{12}}
$$
where $A = 1$ corresponds to the case of an isotropic crystal of the form [@eq:isotropic-strain-energy],
in which case we can identify $K = (C_{11} + 2C_{12})/3$ and $\mu = C_{44}$.

Voigt notation is constructed to preserve quadratic forms like the one in the free energy
$$
    \strain : C : \strain = (\strain^V)^T C^V \strain^V \;.
$$
This is ensured by the factors of 2 in front of the shear strain components in [@eq:voigt-vector].
However, one needs to be careful when dealing with more than one fourth-rank tensors,
because products between these are in general not preserved under Voigt notation:
$$
    C_1^V C_2^V\neq (C_1 : C_2)^V \qquad \text{and} \qquad (C^{-1})^V \neq (C^V)^{-1}\;.
$$
For these scenarios we introduce Mandel notation,
effectively shifting a factor of $\sqrt{2}$ from shear vector components
to the corresponding matrix components, defining
$$
    \strain^M = (\strain_{11}, \strain_{22}, \strain_{33},
        \sqrt{2}\strain_{23}, \sqrt{2}\strain_{13}, \sqrt{2}\strain_{12})
$$
and
$$
    C^M = \begin{pmatrix}
        C_{1111} & C_{1122} & C_{1133} & \sqrt{2}C_{1123} & \sqrt{2}C_{1113} & \sqrt{2}C_{1112} \\
        C_{2211} & C_{2222} & C_{2233} & \sqrt{2}C_{2223} & \sqrt{2}C_{2213} & \sqrt{2}C_{2212} \\
        C_{3311} & C_{3322} & C_{3333} & \sqrt{2}C_{3323} & \sqrt{2}C_{3313} & \sqrt{2}C_{3312} \\
        \sqrt{2}C_{2311} & \sqrt{2}C_{2322} & \sqrt{2}C_{2333} & 2C_{2323} & 2C_{2313} & 2C_{2312} \\
        \sqrt{2}C_{1311} & \sqrt{2}C_{1322} & \sqrt{2}C_{1333} & 2C_{1323} & 2C_{1313} & 2C_{1312} \\
        \sqrt{2}C_{1211} & \sqrt{2}C_{1222} & \sqrt{2}C_{1233} & 2C_{1223} & 2C_{1213} & 2C_{1212} \\
    \end{pmatrix} \;.
$$
Mandel notation also leaves quadratic forms invariant
$$
    (\strain^M)^T C^M \strain^M = (\strain^V)^T C^V \strain^V = \strain : C : \strain
$$
but now we can use it for matrix products and inversion as well
$$
    C_1^M C_2^M = (C_1 : C_2)^M \qquad (C^{-1})^M = (C^M)^{-1} \;.
$$
In conclusion, Mandel notation is more general and more convenient.
However, the literature standard is Voigt notation.
To avoid confusion with literature values
in this text the component without explicit superscript $C_{ij}$
will still refer to Voigt notation.
For example, in a cubic crystal the stiffness tensor component $C_{44} = \mu$ refers to the shear modulus,
not twice the shear modulus.

### Dynamics, microscopic and macroscopic modes {#sec:dynamics-micro-macro}

Ignoring externally applied stress we have
$$
\begin{aligned}
	\mathcal{S}
	% TODO: talk about free energy vs. energy density earlier
		&= \int dtd^dx\;\frac12 \left((\partial_t u_i)^2 + \partial_i u_j C_{ijkl} \partial_k u_l\right)\\
		&= \int_{q,\omega} \frac12 u_i(q,\omega) \left(\rho\omega^2\delta_{ij} + D_{ij}(q)\right) u_j(-q,-\omega))
\end{aligned}
$$
where we used the shortcut
$$
    \int_{q,\omega} = \int_{-\infty}^\infty \frac{d\omega}{2\pi}\int \frac{d^dq}{(2\pi)^d} \;.
$$ {#eq:q-int-shortcut}
<!--TODO: Fourier transform formula for $u$-->
We have also defined the *dynamical matrix* $D$ in the continuum theory as
$$
	D_{ij}(q) = C_{iklj} q_k q_l
$$ {#eq:dynamical-matrix-continuum}
The equation of motion for $u$ is then simply the eigenvalue equation for $D$
$$
	D_{ij} u_j = \rho \omega^2 u_j = \rho c(\hat{q})^2 q^2 u_j \;,
$$
with some phonon velocity $c$ for each branch
that in general depends on the direction of the momentum
in a way that is determined by the exact form of the stiffness tensor $C$.
In the isotropic case the dependence of $c$ on the direction of $q$ disappears and we obtain
$$
\begin{aligned}
	c_l &= \sqrt{\frac{K + (2 - \frac2d) \mu}{\rho}} \\
	c_t &= \sqrt{\frac{\mu}{\rho}}
\end{aligned}
$$ {#eq:isotropic-sound-velocities}
It is important to note that the eigenvalue equation for the phonon does not tell the whole story,
as it is only valid at finite $q$.
The macroscopic strain $E$ represents the $q = 0$ mode and follows a very different eigenvalue equation
$$
	C : E = \kappa E\;,
$$
which in general has $6$ eigenmodes.
As an example, in a cubic solid in three dimensions
there is a singlet with eigenvalue $(C_{11} + 2C_{12})/3$ (the bulk modulus),
a duplet with $C_{11} - C_{12}$, and a triplet with $2C_{44}$
(the eigenvalues are the ones of $C^M$, not $C^V$).
In the isotropic case where $A = 1$ the duplet and triplet merge to a 5-fold mode with eigenvalue $2\mu$.
<!--TODO: point out that these modes are not dynamic modes-->

This introduces and important subtlety with the handling of acoustic phonon in field theory:
If one was to write the global and local strain as a single field,
the corresponding Green's function would be non-analytic around $q = 0$,
causing problems when integrating said function over a domain that contains the origin.
The Green's function for the macroscopic mode---the acoustic phonon---vanishes at $q = 0$.
The macroscopic mode fills this hole with a different elastic constant.
In other words, on a microscopic level the medium appears to have different elastic properties than on the macroscopic level.

### Coupling of strain to vector fields

TODO: expand to section "Coupling of strain to vector fields", also mention linear coupling
	- emphesize different symmetry, in general $\lambda_{ijkl} \neq \lambda_{klij}$

Throughout this work we will consider coupling of different degrees of freedom to elasticity.
Let us assume that these degrees of freedom are represented by some generic vector field $\vphi$.
For example in the case of magnetoelastic coupling it would be the magnetization $\vphi = \M$
or, in the case of a ferroelectric, the polarization $\vphi = \vec{P}$.
Both of these cases will be considered in later chapters,
but for now we remain agnostic about the exact nature $\vphi$.

From a phenomenological standpoint $\vphi$ and $\strain$ can couple in any way that is allowed by symmetry.
In this approach one may formally expand the energy functional in these fields
and keeps at every order only the terms that are invariant under symmetry transformations.
Of course the contributions get smaller at higher orders in this expansion
and for topics covered in this work we will only need to go to quadratic order in $\vphi$.
There can in principle be a linear coupling, most generally written as an energy term of the form
$$
	\strain_{ij} \iota_{ijk} \phi_k \;.
$$
This coupling may be forbidden, if the vector field obeys an Ising type symmetry,
i.e. invariance under $\vphi \to -\vphi$, e.g. time reversal symmetry in the case of magnetism.
On the other hand, if $\vphi$ is a polarization, the above term is known as piezoelectricity
and is common in materials with sufficiently low symmetry.
One should also note that the only rank three tensor that is invariant under rotations
is the antisymmetric tensor $\epsilon_{ijk}$.
However, since the strain tensor is symmetric, such a contribution to $\iota_{ijk}$ would vanish.
The above term is therefore necessarily anisotropic.

TODO: coupling to gradients? flexoelectricity?
$$
	\strain_{ij} \kappa_{ijkl} \partial_k\phi_l \;.
$$

If the linear coupling is forbidden by symmetry,
the leading coupling is expected to be the quadratic one
$$
    \strain_{ij} \lambda_{ijkl} \phi_k\phi_l \;,
$$
In contrast to the linear coupling, the quadratic coupling is generally present,
in some form or another, even for highly symmetric systems.

The coupling is parametrized by a fourth-rank tensor $\lambda$.
Similarly, to the stiffness tensor, it has to satisfy $\lambda_{ijkl} = \lambda_{jikl} = \lambda_{ijlk}$,
which allows us to use Voigt notation.
However, in general $\lambda_{ijkl} \neq \lambda_{klij}$.
In other words, $\lambda^V$ may not be a symmetric matrix.

Specifically, in the case of point group 23, which describes many bulk chiral magnets,
it is of the form
$$
    \lambda^{23,V} = \begin{pmatrix}
        \lambda_{11} & \lambda_{12} & \lambda_{21} & 0 & 0 & 0 \\
        \lambda_{21} & \lambda_{11} & \lambda_{12} & 0 & 0 & 0 \\
        \lambda_{12} & \lambda_{21} & \lambda_{11} & 0 & 0 & 0 \\
        0 & 0 & 0 & \lambda_{44} & 0 & 0 \\
        0 & 0 & 0 & 0 & \lambda_{44} & 0 \\
        0 & 0 & 0 & 0 & 0 & \lambda_{44}
    \end{pmatrix} \;.
$$
with four independent components.

In case of an isotropic solid the number of components of further reduced by the conditions
$\lambda_{21} = \lambda_{12}$ and $\lambda_{11} - \lambda_{12} = 2\lambda_{44}$.
As a result $\lambda$ can be written be written in the form as [@eq:isotropic-stiffness-tensor]
$$
	\lambda^\mathrm{iso}_{ijkl} = \lambda_{11} \delta_{ij}\delta_{kl}
		+ \lambda_{44} (\delta_{ik}\delta_{jl} + \delta_{il}\delta_{jk})
$$
All of the above assume that $\vphi$ is a vector,
that transforms with the same representation as the strain tensor.
We can also imagine an in a sense even more symmetric case,
where $\vphi$ can be transformed independently from spatial degrees of freedom.
In this case we can simply write
$$
	\lambda_{ij\alpha\beta} = \lambda \delta_{ij}\delta_{\alpha\beta}
$$
where the indices $\alpha$ and $\beta$ may be summed over a different range than $i$ and $j$.
