\def\E{\mathcal{E}}
\def\F{\mathcal{F}}

# Classical criticality on compressible lattices: A historical review {#sec:classical-criticality-review}

TODO: talk about linear vs. quadratic coupling

This chapter is concerned with the different stages of development
of the theory of classical criticality on a compressible lattice,
at least as far as we can learn something from it in pursuit of our later goal of describing the corresponding case for quantum phase transitions.
We will at first approach the subject from the most general and generic considerations
about the thermodynamics of phase transitions coupling to elastic degrees of freedom,
treating them effectively as hidden variables [@Fisher1968].
These early developments are discussed in @sec:thermodynamics-compressible-lattice
together with arguments about the non-perturbative nature of the elastic coupling [@Rice1954;@Domb1956].
Going further to specific microscopic models, <!--TODO: baker-essam?-->
we will look at Larkin and Pikins approach of integrating out strain [@LarkinPikin1969],
mapping the problem on an effective Landau theory that suggests a first order transition at constant pressure,
while confirming Fisher's theory in the case of constant volume.
Here, the subtleties of the macroscopic elastic mode become important and lead to a strong dependence on boundary conditions.
Later in @sec:larkin-pikin-quantum-breakdown we also discuss why this approach fails for the quantum phase transition.
Finally, in @sec:rg-anisotropic we discuss the renormalization group analysis done by Bergman and Halperin [@BergmanHalperin1976],
which allows to treat both constant pressure and constant volume in one theory
and will be used as a blueprint for the RG analysis of quantum criticality
in @sec:quantum-criticality-on-a-compressible-lattice.
Additionally, the Bergman-Halperin approach has a relatively simple way of incorporating lattice anisotropy into the classical theory.
Even though it was not always realized at the time,
all these different approaches give essentially the same result,
but not each one of them are telling the whole story.
<!--TODO-->


## Thermodynamics of phase transitions on compressible lattices {#sec:thermodynamics-compressible-lattice}

In the limiting case of no elastic coupling,
we assume this "rigid" system shows a continuous phase transition of a certain universality class,
associated with critical exponents system $\alpha_0$, $\beta_0$, $\gamma_0$, $\dots$.
<!--
   -It is often the case for continuous phase transitions that some thermodynamic quantities may diverge
   -upon approaching the critical point.
   -If the critical exponent $\alpha_0$ is positive, this is the case for the specific heat.
   -->
For simplicity we consider the transition at zero field.
The free energy as a function of the tuning parameter is then generically of the form
$$
	\mathcal{F}_0(r) =- A r^{2-\alpha_0} + B r + \dots
$$ {#eq:non-analytic-landau-theory}
such that $\C_0 = -\partial_r^2 \mathcal{F}_0 \propto r^{-\alpha_0}$.
There may be many other terms in the free energy,
we write down only the ones that are important for the follow arguments.
We have also omitted logarithmic corrections, which only become important when $\alpha_0 = 0$,
such is the case for an $O(N)$ model in $d=4$ dimensions, as seen in [@sec:example-phi4].
In such cases we may write $\C \propto (\log r)^{-\tilde{\alpha}}$
and if $\tilde\alpha > 0$ the specific head diverges logarithmically.
Here, we only discuss $\alpha_0 \neq 0$, but the results are strait forwardly generalized [@Fisher1968;@LarkinPikin1969].

After taking into account elastic coupling the system is described by the full free energy $\mathcal{F}(r, \sigma)$,
which is now a function of applied stress $\sigma$.
This raises the question: How is $\mathcal{F}$ connected to $\mathcal{F}_0$ and 
does $\mathcal{F}$ come with modified critical exponents $\alpha$, $\beta$, $\gamma$, $\dots$?
In other words, does elastic coupling change the universality class of the transition?
<!--TODO: move this part to the beginning?-->

The free energy depends on the usual thermodynamic variables.
These variables generally come in pairs of extensive variables (entropy, magnetization, volume)
also called generalized coordinates
and intensive variables (temperature, magnetic field, pressure)
or generalized forces.
In the coupled system the new pair of variables is stress and strain,
where stress is the generalized force/extensive variable.
They can be viewed as a generalization of pressure and volume,
which would be sufficient in the absence of shear forces.
We usually assign special meaning to the derivative of the generalized coordinate
<!--TODO: state special meaning: stiffness/stability-->
with respect to the generalized force,
such as the specific heat, the magnetic susceptibility
or in the case stress and strain the compressibility, which we now proceed to calculate.

When applying hydrostatic pressure,
equivalent to a uniform stress $\sigma_{ij} = \sigma \delta_{ij}$,
the resulting volume change is given by
$$
	\E \equiv \langle\tr\{\strain\}\rangle
		= \frac{\partial\mathcal{F}}{\partial\sigma}
		\sim \frac{\Delta V}{V} \;.
$$
Here, we use the symbol $\E$ for the thermodynamic strain variable,
while $\strain$ is reserved for the microscopic field.
At constant pressure/stress we can define the compressibility
which is essentially the strain-strain correlation function
$$
	\kappa = - \frac{\partial\E}{\partial\sigma}
		= -\frac{\partial^2 \mathcal{F}}{\partial \sigma^2}
		= \langle\tr\{\strain\}^2\rangle - \langle\tr\{\strain\}\rangle^2 \;.
$$
When operating at constant volume however,
the strain $\E$ of the system has to be set by ensuring appropriate boundary conditions,
which causes internal stresses $\sigma(\E)$ to develop.
The free energy $\F = \F(r, \sigma(\E))$ in now indirectly a function $\E$
$$
	\frac{\partial\F}{\partial\E}
		%= \left(\frac{\partial\F}{\partial\sigma}\right)_\E \frac{\partial\sigma}{\partial\E}
		= \frac{\partial\F}{\partial\sigma} \frac{\partial\sigma}{\partial\E}
		\equiv -\E K \;.
$$
Where we defined the thermal bulk modulus $K$
$$
	K = -\frac{\partial\sigma}{\partial \E} = -\frac{\partial^2 \mathcal{F}}{\partial \E^2} \;.
$$
It can be identified with the microscopic bulk modulus using hooks law ([@eq:hooks-law]).
In either case, constant pressure or volume, the condition of thermodynamic stability
requires $\F$ to be convex and therefore $\kappa$ and $K$ need to be positive.
In other words, when the pressure is increased at the boundaries,
the system must shrink in volume.

We come to the question of how the singular character of $\F_0$ is reflected in $\F$.
In order to make progress we need to make some reasonable assumption about their relation.
Historically, @Domb1956 and @Fisher1968
assumed the free energy $\mathcal{F}$ of the coupled system to be of the form
$$
	\mathcal{F}(r, \sigma) = \mathcal{F}_0(f(r, \sigma)) + g(r, \sigma)
$$
where, crucially, $f$ and $g$ have to be analytic functions that have a series expansion.
We will question this assumption later on.

First, we consider the system at constant $\sigma$.
The critical point is now shifted to a $\sigma$-dependent value of the tuning parameter $r = r_c(\sigma)$
defined by $f(r_c, \sigma) = 0$.
We expand in $\tilde{r} = r - r_c$
$$
	f(r, \sigma) = f(r_c + \tilde{r}, \sigma) = \frac{\partial f}{\partial r}(r_c, \sigma)\tilde{r} + \O(\tilde{r}^2) \;.
$$ {#eq:effective-tuning-expansion}
We make another assumption that the linear term does not vanish,
which is equivalent to $\partial r_c/\partial \sigma \neq 0$.
<!--
   -$$
   -    \bar{f}(\tilde{r}, \sigma) = f(r_c + \tilde{r}, \sigma) = f(r_c, \sigma)
   -        + \frac{\partial f}{\partial r}(r_c, \sigma)\tilde{r} + \O(\tilde{r}^2) \;.
   -$$
   -->

We can now check if the exponent $\alpha$ of the coupled system is different from $\alpha_0$
by calculating the specific heat at constant stress
$$
	\C_\sigma = -\left(\frac{\partial^2 \mathcal{F}}{\partial \tilde{r}^2}\right)_\sigma
		= -\mathcal{F}_0''(f) \left(\frac{\partial f}{\partial \tilde{r}}\right)_\sigma^2
			- \mathcal{F}_0'(f) \left(\frac{\partial^2 f}{\partial \tilde{r}^2}\right)_\sigma
			- \left(\frac{\partial^2 g}{\partial \tilde{r}^2}\right)_\sigma \;.
$$
Now using the expansion of $f$
and the known form of $\mathcal{F}_0$ in [@eq:non-analytic-landau-theory] we can see
that the first term is still the only singular one and goes like $\tilde{r}^{-\alpha_0}$ in leading order,
hence the universal critical behavior is unchanged.
However, this holds only if the transition remains continuous.
As was pointed out by @Domb1956,
if we insert the assumed form of the free energy into the definition of the compressibility,
we obtain the analogous expression
$$
	\kappa = -\left(\frac{\partial^2 \mathcal{F}}{\partial \sigma^2}\right)_r
		= - \mathcal{F}_0''(f) \left(\frac{\partial f}{\partial \sigma}\right)_r^2
			- \mathcal{F}_0'(f) \frac{\partial^2 f}{\partial \sigma^2}
			- \frac{\partial^2 g}{\partial \sigma^2} \;.
$$
A similar line of reasoning as for $\C_\sigma$ can be used here
to show that $\kappa \propto \tilde{r}^{-\alpha_0}$.
This follows from $\partial r_c/\partial \sigma \neq 0$,
since that implies $\partial f/\partial \sigma(r_c, \sigma)) \neq 0$.
We can deduce that if $\alpha_0 > 0$ the compressibility will become infinite,
or equivalently the thermal bulk modulus $K = \kappa^{-1}$ will vanish,
a signature of a first order transition.

Note that even if $\alpha_0 < 0$,
the first term may just happen to be significant enough to destabilize the system,
but it is not guarantied and will depend on non-universal factors.

Going back to the case of constant strain,
we consider the constrained system at a particular strain $\E_c$.
The situation is now somewhat more complicated.
Even though, the expansion [@eq:effective-tuning-expansion]
is still valid for the direct dependence of $f$ on $\tilde{r}$,
there is now also an indirect dependence via $\sigma(r, \E)$.
We can extract the overall relation between $f$ and $\tilde{r}$ using
$$
	\E_c = \frac{\partial \mathcal{F}}{\partial \sigma}
		= \mathcal{F}_0'(f) \frac{\partial f}{\partial \sigma}
			+ \frac{\partial g}{\partial \sigma}
$$
<!--
   -Since $\E_c$ is a constant (or even any analytic function of $r$ and $\sigma$),
   -it can be absorbed into the $\sigma$-linear part of $g$.
   -->
<!--Actually, $\sigma$ is now a function of $r$ and $\E_0$-->
<!--
   -[Actually, 
   -$$
   -    \frac{\partial}{\partial \sigma} \mathcal{F}(f(r_c(\sigma) + \tilde{r}, \sigma))
   -        = \mathcal{F}_0'(f) \left(\frac{\partial f}{\partial r} \frac{\partial r_c}{\partial \sigma}
   -            + \frac{\partial f}{\partial \sigma}\right) + \frac{\partial g}{\partial \sigma}
   -$$
   -]
   -->
Hence, we write using [@eq:non-analytic-landau-theory]
$$
		\mathcal{F}_0'(f) \frac{\partial f}{\partial \sigma}
			= \left(B -(2-\alpha_0) A f^{1-\alpha_0}\right) \frac{\partial f}{\partial \sigma}
			= \E_c - \frac{\partial g}{\partial \sigma}
$$
Expanding both $\partial f/\partial \sigma$ and the right hand side in $\tilde{r}$
$$
	\frac{\partial f}{\partial \sigma} = b_0(\sigma) + b_1(\sigma) \tilde{r} + \O(\tilde{r}^2) \qquad
	\frac{\partial g}{\partial \sigma} = c_0(\sigma) + c_1(\sigma) \tilde{r} + \O(\tilde{r}^2)
$$
and discarding the zeroth order because $\tilde{r} = 0$ implies $f = 0$
we obtain
$$
	-(2-\alpha_0) A f^{1-\alpha_0} b_0(\sigma)
		= (B b_1(\sigma) - c_1(\sigma))\tilde{r}
$$
We conclude that the effective tuning parameter $f$ is related to the bare tuning parameter $\tilde{r}$ via
$$
	f \propto \tilde{r}^\frac{1}{1-\alpha_0}
$$
As a result the coupled system at constant strain
then shows Fisher-renormalized critical exponents
TODO: why sign for $\alpha$?
$$
	\alpha = -\frac{\alpha_0}{1-\alpha_0} \qquad
	\beta = \frac{\beta_0}{1-\alpha_0} \qquad
	\gamma = \frac{\gamma_0}{1-\alpha_0} \qquad
	\dots
$$

TODO: there is an asymmetry between generalized force and generalized coordinate

In summary, if at constant stress/pressure the system has a diverging specific heat ($\alpha_0 > 0$),
then at constant strain/volume the specific heat has the Fisher-renormalized exponent $\alpha$
and will remain finite.
If $\alpha_0 < 0$ then the critical exponents are unaffected.

TODO: mention $\alpha_0 < 0$ already in the derivation

This is essentially equivalent to the Harris criterion [@Harris1974],
which states that the nature of the criticality is unaffected by disorder,
if $\nu d < 2$,
assuming the hyperscaling relation $2 - \alpha = \nu d$ is obeyed.
This suggests that fluctuations in strain can be thought of as a kind of disorder.

![Constant pressure vs. constant volume. Image from [@Fisher1968]](figures/Fisher_renormalization.svg)

## Larkin-Pikin procedure
<!--TODO: better title?-->

Fisher's theory predicts the critical behavior of the coupled system depending on the thermodynamic ensemble.
However, this result is based on the assumption
that the free energy depends in a well behaved way on the free energy of the uncoupled system.
Naturally, one would like to test these assumptions on a specific microscopic model.

Sticking to the principle already employed in [@sec:intro-critical-phenomena] of always choosing the most generic option,
we write down a theory that is $O(N)$ symmetric in $\vphi$ and rotationally symmetric in space, i.e. isotropic.
As explained in [@sec:continuum-mechanics] the action may then only depend on the trace of tensors constructed from $\strain$.
All these constraints result in the action
$$
\begin{aligned}
    \mathcal{S} = \int d^dx \bigg(\frac12 &\vphi^T(- c^2\nabla^2 + r)\vphi + \frac{u}{4!} (\phi^2)^2 \\
     &+ \frac12 \left(K - \frac2d\, \mu\right) \tr\{\strain\}^2 + \mu \tr\{\strain\strain\}
     + \lambda \tr\{\strain\} \phi^2
    \bigg) \;,
\end{aligned}
$$ {#eq:larkin-pikin-action}
equivalent to the model employed in [@LarkinPikin1969]
and we will follow their approach here.
The plan is to integrate out strain and derive an effective Landau theory,
which may be identified with the form assumed by Fisher.

Integrating out strain has some subtlety here as was already hinted at in [@sec:continuum-mechanics].
First, let us write the action in terms of the global strain as well as the longitudinal and transversal phonon
with the traceless strain tensor $\tilde{\strain} = \strain - \frac1d \tr\{\strain\} \one$
$$
\begin{aligned}
    \mathcal{S}_\strain &= \int d^dx \left(
			\frac12 \left(K - \frac2d\, \mu\right) \tr\{\strain\}^2 + \mu \tr\{\strain\strain\}
		\right) \\
		&= \frac{VK}{2}  \tr\{E\}^2 + \int d^dx \left(
			\frac{\rho c_l^2}{2}  (\nabla\cdot\u)^2 + \rho c_t^2 \tr\{\tilde\strain^2\}
		\right) \;.
\end{aligned}
$$
In the following we ignore the transversal/shear terms,
because the coupling is only with the trace of $\strain$.

First, we integrate out the global strain macroscopic strain $\E = \tr\{E\}$.
Now the distinction between ensembles becomes again important.
At constant volume $\E$ is just a constant
and its contribution to the interaction can be accounted for by a shift in the tuning parameter $r$.

At constant pressure, however, $\E$ is a fluctuating degree of freedom that has to be integrated to obtain the partition function
$$
	\mathcal{Z} = \int \mathcal{D}\vphi \;\mathcal{D}u \;d\E \;e^{-\mathcal{S}}
$$
We define
$$
	\psi^2 = \int d^dx \phi^2
$$
and complete the square to rewrite
$$
    \frac{K}{2} \E^2 + \lambda \E \psi^2
		= \frac{K}{2} \left(\E + \frac{\lambda}{K} \psi^2\right)^2
			- \frac{\lambda^2}{2K} (\psi^2)^2 \;.
$$ {#eq:complete-square-global-strain}
Now we can eliminate $\psi^2$ in the first term
on the right hand side by a simple shift in the integration variable.
The result of the $\E$ integral is a constant factor that can be ignored
and only the non-local interaction $(\psi^2)^2$ remains.

A similar procedure may be performed to integrate out $\u$,
regardless of boundary conditions.
Completing the square for the longitudinal phonon energy, we obtain
$$
    \frac{\rho c_l^2}{2} (\nabla\u)^2
			+ \lambda \nabla\u \,\phi^2
			= \frac{\rho c_l^2}{2} \left(\nabla\u + \frac{\lambda}{\rho c_l^2} \phi^2\right)^2
			- \frac{\lambda^2}{2\rho c_l^2} (\phi^2)^2 \; .
$$
One might naively think that the path integral over $\u$ can again be handled by a shift of the integration variable,
but this can not account for the spatially constant contribution of $\phi^2$.
This is easier seen in momentum space, where the shift 
$$
	\u_q \to \u_q - \frac{1}{iq} \frac{\lambda}{\rho c_l^2} \tilde{(\phi^2)}_q
$$
is not possible for $q = 0$, leaving a "bosonic hole"
$$
%\begin{aligned}
	%&\int \mathcal{D}u \exp\left(-\int d^dx \frac{\rho c_l^2}{2} \left(\nabla\u + \frac{\lambda}{\rho c_l^2} \phi^2\right)^2 \right) \\
		\int \mathcal{D}u \exp\left(-\int_q \frac{\rho c_l^2}{2} \left(i\q\cdot \u + \frac{\lambda}{\rho c_l^2} \tilde{(\phi^2)}_q\right)^2 \right)
		= \mathcal{Z}_u \exp\left(-\frac{\lambda^2}{2\rho c_l^2} (\psi^2)^2\right)
%\end{aligned}
$$
with a constant $\mathcal{Z}_u$.
<!--TODO: diagrams? (tree level, see chandra 2020)-->
In summary, the longitudinal phonon generates both an effective local and non-local self-interaction of the critical modes,
while the macroscopic mode generates an effective non-local self-interaction only at constant pressure.
As a result, the effective action of the critical modes is
$$
    \mathcal{S} = \int d^dx \bigg(\frac12 \vphi^T(- c^2\nabla^2 + r)\vphi
		+ \bar{u} (\phi^2)^2\bigg)
     + v V^{-1}\left(\int d^dx \phi^2\right)^2 \;,
$$ {#eq:larkin-pikin-effective-action}
where the local interaction has been renormalized by the longitudinal phonon
$$
	\bar{u} = \frac{u}{4!} - \frac{\lambda^2}{2\rho c_l^2}
$$ {#eq:renormalized-self-interaction}
and a new non-local interaction has appeared with a coupling parameter
$$
	v = \begin{dcases}
		-\frac{\lambda^2}{2\rho c_l^2} < 0 & \sigma = \text{const.} \\
		\left(\frac{\lambda^2}{2K} - \frac{\lambda^2}{2\rho c_l^2}\right) > 0 & \E = \text{const.}
	\end{dcases}
$$ {#eq:non-local-interaction-constant}
Note that the sign of $v$ depends on the ensemble,
since $\rho c_l^2 = K + \frac43 \mu > K$ and therefore in the constant $\E$ case $v>0$.

The first term in [@eq:larkin-pikin-effective-action] is differs from the original $\phi^4$-theory
only by a modified bare value of the coupling constant and hence contributes to the free energy
a term with the same singular behavior.
The question is then: How is the universal behavior changed by the non-local interaction?

[@LarkinPikin1969] answer this question by performing a
Hubbard-Stratonovich transformation,
introducing an auxiliary field, 
which is essentially just reversing the integration of $\E$
$$
	\exp\left(v V^{-1} (\psi^2)^2\right) = \int d\E \exp\left(-\frac{\lambda^2\E^2V}{4v} - \lambda\E\psi^2\right) \;.
$$
This allows writing the action in terms of the already solved problem of the $\phi^4$ theory,
representing the uncoupled $\F_0$ from the previous section
$$
	\mathcal{Z} = \int d\E \int \mathcal{D}\vphi\; e^{-\mathcal{S}_0(r+\lambda\E) -\frac{\lambda^2\E^2V}{4v}}
		= \int d\E\; e^{-\mathcal{F}_0(r+\lambda\E) - \frac{\lambda^2\E^2V}{4v}} \;.
$$
The effective free energy can be identified with Fishers assumed free energy
with the functions $f(r, \E) = r + \lambda\E$ and $g(r, \E) = - \lambda^2\E^2V/4v$.
Clearly the assumption of analyticity is fulfilled for both of them,
hence, we know to expect Fisher-renormalized exponents, if $\alpha_0 > 0$,
which is the case for the $O(N)$ model in three dimensions for $N < 4$ in one-loop order,
but only for $N = 1$ when including higher orders.

TODO: first order transition? graphs

So we have learned that a classical Ising transition in three spatial dimensions,
on a compressible lattice will at constant pressure show a first order transition before it reaches the critical point,
at constant volume it will show Fisher-renormalized Ising exponents instead of the normal Ising exponents.

Instead of the introducing the Hubbard-Stratonovich field,
one could take the theory as framed in [@eq:larkin-pikin-effective-action] and perform an RG analysis.
This was done in @Sak1974 and @BrunoSak1980, obtaining essentially the same result as outlined here.
Although the expressions given only match the constant pressure case,
a new stable fixed point with Fisher-renormalized exponents can be identified,
but seems inaccessible because it is at a value of $v$ with the opposite sign.
We do not go into their calculation in detail and instead focus on the more general analysis in the next section.
However, readers trying to follow @Sak1974 should be advised of the typo in eq. (7),
which becomes the second case in [@eq:non-local-interaction-constant] of this work after exchanging $-$ with $\times$.

## Renormalization group approach on an anisotropic cubic lattice {#sec:rg-anisotropic}
So far both the thermodynamic approach and the microscopic theory for the isotropic solid
suggests Fisher-renormalized exponents at constant volume.
However, it is worthwhile to see how these modified exponents emerge in the RG picture.
To this end we follow here the approach of Bergman and Halperin [@BergmanHalperin1976],
who analyzed the effect of lattice coupling for an Ising transition, meaning they only looked at $N=1$---an unnecessary restriction
that we will not impose here---and find a new fixed point that is associated with Fisher renormalized exponents.

The approach of Bergman and Halperin differs from previous attempts to solve the problem in that they do not integrate out elastic degrees of freedom.
Indeed it there is no reason why one should not perform the RG procedure on the original action in terms of the fields $\vphi$ and $\u$.
This approach has the benefit of showing transparently the effect of the transition on the elastic constants,
as well as the change in critical behavior.
It also avoids the subtleties involved with the integration of the macroscopic mode,
since in momentum shell RG the integration domain always strays far away from the singularity at $q = 0$.
As an additional benefit, in Bergman and Halperins approach it becomes feasible to include lattice anisotropy into the model.
Hence, we write down the action
$$
    \mathcal{S} = \int d^dr \bigg(\frac12 \vphi^T(- c^2\nabla^2 + r)\vphi + \frac{u}{4!} (\phi^2)^2
		+ \lambda \tr\strain \phi^2
		+ \frac12 \strain_{ij} C_{ijkl} \strain_{kl}
    \bigg) \;,
$$ {#eq:bergman-halperin-action}
<!--TODO: mention that $\lambda$ is marginal-->
which is slightly more general than [@eq:larkin-pikin-action] in that it allows an anisotropic stiffness tensor.
Since we want to understand the effect of anisotropy,
but don't want to complicate things too much, we will assume a cubic lattice,
reducing the number of parameters of the phonon propagator to three,
conventionally called $C_{11}$, $C_{12}$ and $C_{44}$ in Voigt notation (see [@sec:voigt-and-mandel-notation]).
Readers trying to compare with [@BergmanHalperin1976] should be advised
that they are using a different definition of $C_{44}$,
which differs by a factor of 4 from conventional Voigt notation.
If the Zener index $A = 2C_{44}/(C_{11} - C_{12})$ is different from unity,
the sound velocity will be a function of the direction of momentum $c_l(\hat{q})$.
We write the action in Fourier space
$$
\begin{aligned}
    \mathcal{S} = \int_q \bigg(&\frac12 (c^2q^2 + r)\phi_q\phi_{-q}
		+ \frac{u}{4!} \int_{q',q''} (\vphi_q\cdot\vphi_{q'}) (\vphi_{q''}\cdot\vphi_{-q-q'-q''}) \\
		&+ \lambda \int_{q'} (i\q\cdot\u_q) (\vphi_{q'}\cdot\vphi_{-q-q'})
		+ \frac12 u_{q,i} D_{ij}(q) u_{-q,j}
    \bigg) \;.
\end{aligned}
$$
$D$ is the dynamical matrix as defined in [@eq:dynamical-matrix-continuum].
Even though the lattice is anisotropic as reflected in the stiffness tensor $C$,
the interaction still only depends on the trace
because in this theory $\vphi$ and $\u$ are assumed to live in different spaces with distinct symmetries.

The diagrams in the perturbation series now have phonon lines, which we represent as dashed,
in addition to the critical modes, which still have solid lines.
For example for the self-energy correction of the critical field $\vphi$ we get the additional diagram
$$
	\diagramPhiSelfEnergyLL\quad
		= \frac{\lambda^2}{2} \int_{q'} \langle \phi_{q-q'} \phi_{-q+q'} \rangle_0
			\langle \tr\strain_{q'} \tr\strain_{-q'} \rangle_0 \;.
	% TODO: add indices?
$$ {#eq:phi-self-energy-lambda2}
The strain-strain correlation function can be calculated from
$\langle u_{i,q} u_{j,-q} \rangle = D^{-1}_{ij}(q)$.
The global stain $E = \strain_{q=0}$ can be ignored here,
since the $q$ integration will only ever go over momenta close to the cutoff $b\Lambda < q < \Lambda$ with a $b$ that close to unity.
The effect of the trace is a projection onto the longitudinal mode
$$
	\diagramUBare\quad = \langle \tr\strain_q \tr\strain_{-q} \rangle_0
		= q_i D^{-1}_{ij}(q) q_j = \frac{1}{\rho c_l^2(\hat{q})} \;.
$$
Specifically, for a cubic lattice 
$$
	\rho c_l^2(\hat{q}) = C_{11} - 2(C_{11} - C_{12}) (1-A) Q(\hat{q})\;,
$$ {#eq:anisotropic-cl}
where we defined
$$
	Q(\hat{q}) = \frac{A(\hat{q}_x^2 \hat{q}_y^2 + \hat{q}_x^2 \hat{q}_z^2 + \hat{q}_y^2 \hat{q}_z^2)
			+ (1-A)(4-A) \hat{q}_x^2 \hat{q}_y^2 \hat{q}_z^2}
		{A^2 + 4A(1-A)(\hat{q}_x^2\hat{q}_y^2 + \hat{q}_y^2\hat{q}_z^2 + \hat{q}_z^2\hat{q}_x^2)
			+ 12(1-A)^2 \hat{q}_x^2 \hat{q}_y^2 \hat{q}_z^2} \;.
$$
$Q > 0$ and $C_{11} > C_{12}$, hence the sign of the anisotropic contribution to $c_l$ depends strictly on the sign of $1-A$.

Now we can go about calculating the diagram [@eq:phi-self-energy-lambda2].
First we must realize that it actually independent of the external momentum $q$.
It is convenient to shift $q'$ by $q$ so we can expand in $q$ and write
$$
	\frac{1}{\rho c_l^2(\frac{q-q'}{|q-q'|})}
		\frac{1}{\rho c_l^2(\hat{q}')} + q_i \partial_{q'_i}\frac{1}{\rho c_l^2(\hat{q}')} + \frac12 q_iq_j \partial_{q'_i}\partial_{q'_j}\frac{1}{\rho c_l^2(\hat{q}')} \,.
$$
where the linear term vanishes bc of symmetry.
Definition of angular integral
$$
	\int_{\hat{q}} = \int \frac{d\Omega}{S_d}
$$
Cubic symmetry:
$$
	\partial_{q'_i}\partial_{q'_j} \frac{1}{\rho c_l^2(\hat{q}')}
		= \frac{\delta_{ij}}{d} \nabla_{q'}^2 \frac{1}{\rho c_l^2(\hat{q}')}
$$
Thinking about the angular dependence in terms of spherical harmonics,
we can conclude that the gradients will kill the 0th harmonic,
which is the only one that would survive the angular integral.
As a consequence the diagram is independent of external momentum
and integrand separates into a purely angle dependent factor and a factor that only depends on the absolute values of momentum
$$
	\diagramPhiSelfEnergyLL\quad
		= \frac{\lambda^2}{2\rho \clav} \int_{q'} G_0(q')
		= \frac{\lambda^2}{2\rho \clav} I_1 \,,
		%= - \frac{\lambda^2}{16\pi^2 \rho \clav c^3} r\log\Lambda
$$
<!--(TODO: factor 4 from combinatorics not included, inconsistent with later notation)-->
with $I_1$ as in [@eq:integral-1] and
where we defined the averaged sound velocity
$$
	\frac{1}{\clav} \equiv \int_{\hat{q}} \frac{1}{c_l^2(\hat{q})} \;,
$$
At first glance this separation seem like a convenient technicality,
but actually this is what makes the RG treatment of anisotropy feasible in the fist place.
In fact all of the integrals we encounter in this classical theory will separate in this manner,
allowing us to treat anisotropy independently from the other couplings.
In general the above integral is still hard to solve,
but for small anisotropy we can expand around $A = 1$
and find the relatively simple expression
$$
	%\frac{1}{\rho c_l^2} = \frac{1}{C_{11}} + \frac25 \frac{C_{11} - C_{12}}{C_{11}^2} (1-A) + \O\left({(1-A)}^2\right) \;.
	\rho \clav = C_{11} - \frac25 (C_{11} - C_{12}) (1-A) + \O\left({(1-A)}^2\right) \;.
$$
<!--(TODO: actually there is the $\Lambda^2$ term, and it is different, from the quantum theory)-->
The critical modes are then renormalized according to
$$
\begin{aligned}
    \diagramPhiDressed \quad&=\quad \diagramPhiBare
		\quad+\quad \diagramPhiSelfEnergyV \quad+\quad \diagramPhiSelfEnergyLL \\
		G^{-1} &= G_0^{-1} - \Sigma \\
    = Z_\phi^{-1} (c^2 q^2 + r) &= c_0^2 q^2 + r_0
        + 4(N + 2)\,\frac{u_0}{4!} I_1
		- \frac{4\lambda^2}{2\rho \clav} I_1 \,.
\end{aligned}
$$
<!--TODO: reframe as $\Sigma = $?-->
Since in non of the integrals depend on external momentum,
only the tuning parameter $r$ is renormalized.
Inserting the result from [@eq:integral-1] leads to the RG equation
$$
    \frac{dr}{d\ell} = 2r + \frac{N + 2}{2\pi^2 c^3} \frac{u}{4!} r
        - \frac{1}{4\pi^2\rho \clav c^3} \lambda^2 r
$$
Next we consider the self-energy correction to the phonons.
In the same fashion as for the critical modes we write
$$
\begin{aligned}
    \diagramUDressed \quad&=\quad \diagramUBare \quad+\quad \diagramUSelfEnergyLL \\
    D_{ij}(q) &= D_{0,ij}(q)
		- 2N \lambda_0^2 q_iq_j \int_{q'} G^0(q-q') G^0(q') \;,
\end{aligned}
$$
where the integral is the same as [@eq:integral-1].
TODO: show that $s_u = s_\phi$.
Hence, we get for the renormalization of the elastic constants
$$
\begin{aligned}
    \frac{dC_{11}}{d\ell} &= - \frac{N}{4\pi^2} \frac{\lambda^2}{c^3} \\
    \frac{dC_{12}}{d\ell} &= - \frac{N}{4\pi^2} \frac{\lambda^2}{c^3} \\
    \frac{dC_{44}}{d\ell} &= 0 \;.
\end{aligned}
$$ {#eq:bergman-halperin-C-flow}
Note that the shear modulus $C_{44}$ is not renormalized, as would be expected,
since the interaction is only with the trace of the strain tensor,
effectively projecting out the transversal mode.
As a result, the shear modulus is not renormalized.
Since $C_{11}$ flows like $C_{12}$, neither the difference between them
and consequently the Zener index $A$ is invariant under RG transformations.
In a sense the only macroscopic elastic modulus that flows it the bulk modulus ([@eq:bulk-modulus-definition])
$$
    \frac{dK}{d\ell} = - \frac{N}{4\pi^2} \frac{\lambda^2}{c^3}
$$
The corresponding microscopic parameter is the longitudinal sound velocity,
for which the sound velocity can be derived in a similar fashion from [@eq:anisotropic-cl] and [@eq:bergman-halperin-C-flow]
$$
    \frac{dc_l^2}{d\ell} = - \frac{N}{4\pi^2} \frac{\lambda^2}{\rho c^3} \;.
$$
Interestingly, the anisotropic contribution to $c_l^2$ does not flow at all.
In fact it is only $C_{11}$ that changes with an RG transformation,
which suggests that the details of the anisotropy do not matter much for the critical behavior,
as long as it can be expressed in terms of RG invariants such as $A$.
It also follows from the fact that physically $\lambda$ has to be real valued and $c$ has to be positive,
the isotropic contribution to $c_l^2$ will decrease with RG (or remain constant if $\lambda = 0$), while the anisotropic one remains the same.
Therefore, the relative anisotropy
$$
	R = \frac{C_{11} - C_{12} - 2 C_{44}}{C_{11}} = (1-A) \frac{C_{11} - C_{12}}{C_{11}}
$$
will increase (or again remain constant if $\lambda = 0$) in magnitude under RG
$$
    \frac{dR}{d\ell} = \frac{N}{4\pi^2} \frac{\lambda^2}{C_{11} c^3} R \;.
$$
So far we have derived all RG equations at the propagator level.
However, the flow depends on the couplings and to find the RG flow we also need to know the flow equations for the couplings.
There are now two new diagrams with phonon lines in the vertex correction of $u$ including combinatorical factors
\def\DiagramScale{0.6}
$$
\begin{aligned}
    \diagramVDressed{}{}{}{} &\quad=\qquad
        \diagramVBare{}{}{}{}
        ~+~ \diagramVCorrectionVV{}{}{}{} \\
        &\qquad\qquad\qquad~+~ \diagramVCorrectionVLL{}{}{}{}
        ~+~ \diagramVCorrectionLLLL{}{}{}{}
\end{aligned}
$$
$$
    \frac{u}{4!} = \frac{u_0}{4!} - 4 (N+8) {\left(\frac{u_0}{4!}\right)}^2 I_2
        + \frac{24}{\rho \clav} \frac{u_0}{4!} \lambda_0^2 I_2
        - \frac{4}{\rho^2 \left\langle c_l^4 \right\rangle} \lambda_0^4 I_2
	% TODO: define I_mn or replace with integral expression
$$
where the definition of $I_2$ is the same as in [@eq:integral-2].
In the last term (the 'box' diagram) we have to average over the fourth power in the sound velocity,
which complicates things somewhat,
because in the presence of anisotropy this is different from $\clav^2$
$$
	\frac{1}{\rho^2 \left\langle c_l^4 \right\rangle}
		= \int_{\hat{q}} \left(\frac{1}{\rho c_l^2}\right)^2
		\neq \left(\int_{\hat{q}} \frac{1}{\rho c_l^2}\right)^2
$$
And of course the elastic coupling $\lambda$ also gets a vertex correction
$$
    \diagramLDressed{}{}{}{} \quad=\quad \diagramLBare{}{}{}{}
        ~+~ \diagramLCorrectionVL{}{}{}{}
        ~+~ \diagramLCorrectionLLL{}{}{}{}
$$
$$
    \lambda = \lambda_0 - 4 (N+2) \lambda_0 \frac{u_0}{4!} I_2
        + \frac{4}{\rho\clav} \lambda_0^3 I_2
$$
From a scaling analysis of [@eq:bergman-halperin-action] we can see that
$$
	\lambda' = b^{- 2s_\phi - s_u - 1 + d}\lambda = b^{\frac{4-d}{2}} \lambda
$$
RG Equations
$$
\begin{aligned}
    \frac{du}{d\ell} &= \epsilon u
        - \frac{N+8}{2\pi^2c^3} \frac{u^2}{4!}
        + \frac{3}{\pi^2 c^3 \rho\clav} u \lambda^2
        - \frac{2}{\pi^2 \rho^2\langle c_l^4 \rangle} \tilde{\lambda}^4 \\
    \frac{d\lambda^2}{d\ell} &= \epsilon \lambda^2
        - \frac{N+2}{\pi^2} \frac{u}{4!} \lambda^2
        + \frac{1}{\pi^2\rho\clav} \lambda^4
\end{aligned}
$$
The RG equations of $u$ and $\lambda$ are then more concisely expressed in the dimensionless variables
$$
    \tilde{u} = \frac{1}{2\pi^2c^3} \frac{u}{4!}
	\qquad \tilde{\lambda}^2 = \frac{\lambda^2}{4\pi^2\rho \clav c^3} \;.
$$
For these the equations hold
$$
\begin{aligned}
    \frac{d\tilde{u}}{d\ell} &= \epsilon\tilde{u}
        - (N+8) \tilde{u}^2
        + 12 \tilde{u} \tilde{\lambda}^2
        - 4 \frac{\clav^2}{\langle c_l^4 \rangle} \tilde{\lambda}^4 \\
    \frac{d\tilde{\lambda}^2}{d\ell} &= \epsilon \tilde{\lambda}^2
        - 2(N+2) \tilde{u} \tilde{\lambda}^2
        + (N+4) \tilde{\lambda}^4
\end{aligned}
$$
However, as [@BergmanHalperin1976] point out that TODO
$$
    \bar{u} = \tilde{u} - \tilde{\lambda}^2 \;,
$$
Note that this transformation coincides with the renormalization of the self interaction
found in [@eq:renormalized-self-interaction].

\begin{align}
    \frac{d\bar{u}}{d\ell} &= \epsilon\bar{u}
        - (N+8) \bar{u}^2
		 	+ 4 \left(1 - \frac{\clav^2}{\langle c_l^4 \rangle}\right) \tilde{\lambda}^2
			 	\label{eq:bh-rg-u-flow} \\
    \frac{d\tilde{\lambda}^2}{d\ell} &= \epsilon \tilde{\lambda}^2
        - 2(N+2) \bar{u} \tilde{\lambda}^2
        - N \tilde{\lambda}^4 \;. \label{eq:bh-rg-lambda-flow} 
\end{align}

Solving the equations for zero flow,
one finds that the RG flow in $4-\epsilon$ dimensions is characterized by four fixed points,
as seen in [@fig:bergman-halperin-flow].
[@eq:bh-rg-u-flow] has a fixed points at $\bar{u}^* = 0$ and at the Wilson-Fisher value $\bar{u}^* = TODO$.
In each case [@eq:bh-rg-lambda-flow] has 2 solutions: One corresponding to the unmodified Gaussian and WF fixed points at $\tilde{\lambda}^* = 0$
and one with a finite $\tilde{\lambda}^*$.
TODO: the Fr-WF exists only at $N < 4$

![RG flow of the classical theory coupled to the lattice for $N < 4$, as calculated by [@BergmanHalperin1976].](figures/qccl_figures/rg_flow_bergman_halperin.pdf){#fig:bergman-halperin-flow}

TODO: RG flow for $N > 4$

Additionally, Fisher-renormalized-Gaussian (Fr-G) and Fisher-renormalized Wilson-Fisher (Fr-WF)

* Fr-G: $\bar{u}^* = 0$, $(\tilde{\lambda}^*)^2 = \frac{N+4}{2\pi^2} \epsilon$

* point out difference between macroscopic and microscopic instability
	- important for next chapter

* new fixed point with Fisher renormalized exponents
* flow of elastic constants is determined by $\lambda$
	- if $\tilde{\lambda}^2 > 0$ at Fr-WF, $C_{11}$ scales like $b^{TODO\epsilon}$
		- in isotropic case $c_l^2$ vanishes asymptotically
		- in anisotropic case, for $A < 1$ $c_l^2$ vanishes at finite $\ell$, for $A > 1$ ? ($R$ diverges)
* anisotropy causes microscopic instability

In the isotropic limit we have $\rho c_l^2 = C_{11}$ and averages become trivial.

## Discussion
There are two approaches in the literature:
[@Sak1974] (also see [@BrunoSak1980]) starts from the Larkin-Pikin action
that results from integrating out strain [@eq:larkin-pikin-effective-action]
and performs an RG analysis that includes the non-local interaction $v$.
The second approach, applied by [@BergmanHalperin1976], is to simply take the original action,
including elastic degrees of freedom, as it is and performing the RG procedure on the displacement field as well.
This has several advantages: For one, it show transparently the 

* Baker-Essam paper corresponds to case $A = 0$, BergmanHalperin1976 also addresses this
