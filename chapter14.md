# Ferroelectric quantum criticality in an elastic medium {#sec:ferroelectric-qc}

<!--
   -In previous chapters we have seen how a $O(N)$ model behaves under perturbations with different kinds of couplings,
   -such as a cubic anisotropy of the self-interaction and of course the coupling to an elastic medium.
   -However, the $O(N)$ model is limited in its applicability, not just by its high symmetry,
   -by the fact that is only includes short range interactions.
   -It is relevant for example in the case of an antiferromagnetic transition,
   -where there is no net dipole moment after sufficient coarse-graining,
   -but in the case of ferromagnets or ferroelectrics long range dipolar interaction has a significant
   -effect on the critical behaviour, as we will see over the course of this chapter,
   -->

TODO: $\phi$ describes an optical phonon here, mentions that this can be derived
	by continuing the expansion in [@eq:potential-expansion-quadratic] to the quatric level, keeping only terms
	that are relevant in the RG sense.
	The anharmonic coupling then represents the acoustic-optical phonon interaction at the cubic level
	as well as the optical phonon self-interaction at the quartic level.

TODO: maybe mentions flexoelectricity, also the absence of pietzoelecticity at the linear coupling level

An important ingredient for describing ferroelectric quantum criticality is the inclusion of long range dipolar interaction.
[@sec:phi4-dipolar] covers the work of Khmel'nitskii and Shneerson [@KhmelnitskiiShneerson1973]
who first did this calculation for an $O(3)$ model in $3+1$ dimensions.
They show that dipolar interaction changes the universality class of the transition
from Wilson-Fisher (WF) to what we will refer to as Khmel'nitskii-Shneerson (KS) universality class.
They also show that with the inclusion of dipolar interaction, cubic anisotropy becomes relevant,
indicating an instability, where the effective quartic self-interaction vanishes, leading to a fist order transition.

We then move on to discuss the main topic of the chapter, the RG flow of the isotropic ferroelectric with lattice coupling, in [@sec:rg-isotropic-ferroelectric],
which combines the well known KS physics with the insights we gained in [@sec:quantum-criticality-on-a-compressible-lattice].
Besides dipolar interaction, there is the important difference that for the ferroelectric the order parameter field, 
representing an optical phonon, is now a real space vector
and is therefore bound to transform the same as the displacement field $u$ under rotations.
This reduces the symmetry and allows for a coupling to shear strain,
which leads to a renormalization of the transversal acoustic phonon velocity, not just the longitudinal one.
This has important consequences for the critical behaviour.
TODO: describe main results once they are clear


## Quantum criticality in the presence of dipolar interaction {#sec:phi4-dipolar}
If the order parameter field $\vphi$ represents a degree of freedom that is associated with a dipole moment,
such is the case in ferroelectrics or ferromagnets,
there will be a long range dipole-dipole interaction between $\vphi$ at different points.
This gives a contribution to the action that is in the isotropic case of the form
$$
    \mathcal{S}_\mathrm{dipolar} = s \int_q \frac{(q\cdot\vphi)^2}{q^2} \;.
$$
The above expression contains the scalar product between the reciprocal space vector $\q$ and the order parameter $\vphi$,
which is of course only meaningful if the flavor number is equal to the dimension $N = d$.

As a first step we want to know what is the effect of this term in isolation on the critical behaviour,
postponing the discussion of elastic coupling to [@sec:rg-isotropic-ferroelectric].
We will also discuss how it changes the effect of cubic anisotropy.
In order to not repeat ourselves too much we will include cubic anisotropy from the start,
see [@sec:cubic-fixed-point], obtaining the action
$$\begin{aligned}
    \mathcal{S} = \int_{\q,\omega} &\frac12 \vphi_{\q}^T(\omega^2 + c^2q^2 + r + s \hat{q}\hat{q}^T)\vphi_{-\q} \\
		&+ \int_{\q,\omega} \int_{\q',\omega'} \int_{\q'',\omega''} V_{ijkl} \phi_{i,\q} \phi_{j,\q'} \phi_{k,\q''} \phi_{l,-\q-\q'-\q''} \;,
\end{aligned}$$
where $\hat{q}_i = q_i/q$ and
$$
    V_{ijkl} = u \delta_{ij}\delta_{kl} + v g_{ijkl} \;.
$$
At the Gaussian level the dipolar term splits the $d$ modes of $\vphi$
into $d-1$ transversal modes ($q\cdot\vphi = 0$) and one longitudinal mode ($q\cdot\vphi \neq 0$).
The longitudinal mode is the only one that sees the dipolar interaction
in the form of a shifted gap $r_l = r + s$, while the transversal modes remain unchanged $r_t = r$.
Assuming $s$ is positive this means the longitudinal mode will be stabilized by dipolar interaction,
so only the transversal modes become critical as $r$ vanishes.
In general the bare propagator is given by [@Roussev_2003]
$$
    G_{ij}^{(0)}(q, \omega)
        = \frac{1}{\omega^2 + c^2q^2 + r} \left(
            \delta_{ij} - \hat{q}_i \hat{q}_j \frac{s}{\omega^2 + c^2q^2 + r + s}
        \right)
$$
However, as the system approaches criticality the contribution of $\omega$, $q$ and $r$
will become negligible compared to the strength of dipolar interaction $s$.
Therefore we consider the Green's function only for the transversal modes
$$
    G^{(0)}_{ij}(q, \omega) \approx \frac{\delta_{ij} - \hat{q}_i \hat{q}_j}{\omega^2 + c^2q^2 + r} \;.
$$ {#eq:transversal-propagator}
We can now essentially repeat the RG analysis we made in [@sec:cubic-fixed-point] with this modified propagator.
We will need the following identities to solve the new angle dependent integrals
$$\begin{aligned}
    \int_{\hat{q}} \hat{q}_i \hat{q}_j &= \frac{S_d}{d} \delta_{ij} \\
    \int_{\hat{q}} \hat{q}_i \hat{q}_j \hat{q}_k \hat{q}_l
		&= \frac{S_d}{d(d+2)} (\delta_{ij}\delta_{kl} + \delta_{ik}\delta_{jl} + \delta_{il}\delta_{jk}) \;.
\end{aligned}$$ {#eq:angle-integrals}
Using these relations we calculate the one-loop self-energy correction.
We obtain
$$\begin{aligned}
    \diagramPhiSelfEnergyV
        \quad&=\quad \diagramPhiSelfEnergyVTadpole
        \quad+\quad \diagramPhiSelfEnergyVRainbow \\
    %\Sigma^{(1L)}_{ij} &= 2 \frac{u}{4!} \left(2\delta_{ij} \int_q G_{mm} + 4\int_q G_{ij}\right) \\
    %    &= 2 \frac{u}{4!} \left(2(d - 1) \delta_{ij} + 4\left(1-\frac{1}{d}\right) \delta_{ij}\right) I_1 \\
    \Sigma^{(1L)}_{ij} &= 2 (2V_{ijkl} + 4V_{iklj})\int_q G_{kl} \\
        &= 4 I_1 \int_{\hat{q}}\big[(u\delta_{ij}\delta_{kl} + vg_{ijkl})(\delta_{kl} - \hat{q}_k \hat{q}_l) + 2(u\delta_{ik}\delta_{jl} + vg_{iklj})(\delta_{kl} - \hat{q}_k \hat{q}_l) \big] \\
        %&= 4\int_{\hat{q}}\big[(u\delta_{ij}(d-1) + v\delta_{ij} - vg_{ijkl}n_kn_l) + 2(u\delta_{ij} + v\delta_{ij} - un_in_j - vg_{iklj}n_kn_l)\big] I_1 \\
        &= 4 I_1 \Big[u\delta_{ij}(d-1) + v\delta_{ij} - \frac1d vg_{ijkl}\delta_{kl} \\
			&\qquad\quad+ 2\left(u\delta_{ij} + v\delta_{ij} - \frac1d u\delta_{ij} - \frac1d vg_{iklj}\delta_{kl}\right)\Big] \\
        &= 4 I_1 \left[\left(d+1-\frac2d\right)u + \left(3 - \frac3d\right)v\right] \delta_{ij} \;,
\end{aligned}$$
where the integral $I_n$ is the as in [@eq:eq:integral-n-definition].
Similarly, we proceed with the vertex correction, which can be written as
$$\begin{aligned}
    \diagramVCorrectionVV{i}{j}{k}{l}
        &= \diagramVCorrectionVVLoop{i}{j}{k}{l}{i'}{j'}{k'}{l'}
            \!\!\!+\!\!\! \diagramVCorrectionVVPenguin{i}{j}{k}{l}{i'}{j'}{k'}{l'}
            \!\!\!+\!\!\! \diagramVCorrectionVVBox{i}{j}{k}{l}{i'}{j'}{k'}{l'} \\
    \Gamma^{(1L)}_{ijkl} &= \left[2^2 V_{iji'k'} V_{j'l'kl}
		+ 2^4 V_{iji'k'}V_{j'kl'l} + 2^4 V_{ii'jk'}V_{j'kl'l}\right]\int_q G_{i'j'}G_{k'l'}
\end{aligned}$$
The calculation is somewhat involved,
but conceptually completely analogous to the previous calculation of the self-energy correction.
We skip the algebra and give the result for the overall vertex correction
$$\begin{aligned}
    \Gamma^{(2)}_{ijkl} &= 4\Bigg[\left(d + 2 - \frac8d + \frac{12}{d(d+2)} + \frac{d}{d+2} + \frac{6}{d+2}\right) u^2 \\
            &\qquad+ 6\left(1 - \frac2d + \frac{1}{d+2} + \frac{4}{d(d+2)}\right) u v + \frac{9}{d(d+2)} v^2\Bigg] \delta_{ij}\delta_{kl} \\
            &\qquad+ 16\left(1 - \frac2d + \frac{1}{d(d+2)}\right) u^2 \delta_{ik}\delta_{jl} \\
            &\qquad+ \frac{16}{d(d+2)} u^2 \delta_{il}\delta_{jk} \\
        &\qquad+ \Bigg[36\left(1 - \frac{2}{d} + \frac{2}{d(d+2)}\right) v^2 + 48\left(1 - \frac{2}{d} + \frac{2}{d(d+2)}\right) uv\Bigg] g_{ijkl} \;.
\end{aligned}$$
We have left the dimension $d$ arbitary for book keeping reasons.
But since the expressions get unwieldy at a certain point we contine with $d = 3$.
As in [@sec:cubic-fixed-point] we put the RG equations in terms of the dimensionless couplings
$$
	\tilde{u} = \frac{1}{2\pi^2c^3} u \;,\qquad
	\tilde{v} = \frac{1}{2\pi^2c^3} v \;,
$$
such that
$$\begin{aligned}
	\frac{dr}{dl} &= 2 r
		- \frac{10}{3} \tilde{u}r - 2 \tilde{v}r \\
	\frac{d\tilde{u}}{dl} &= \epsilon \tilde{u}
		- \frac{34}{5}\, \tilde{u}^2
		- \frac{24}{5}\, \tilde{u}\tilde{v}
		- \frac{3}{5} \tilde{v}^2 \\
	\frac{d\tilde{v}}{dl} &= \epsilon \tilde{v}
		- \frac{21}{5} \tilde{v}^{2} - \frac{28}{5} \tilde{u}\tilde{v}
\end{aligned}$$
Comparing these equations to [@eq:rg-flow-cubic-anisotropy],
there is an additional $\tilde{v}^2$ term in $d\tilde{u}/d\ell$.
TODO: show consequences for stability

TODO: fixed points

### Isotropic case $v = 0$
$$\begin{aligned}
    \frac{dr}{dl} &= 2 r - \frac{10}{3} r \tilde{u} \\
    \frac{d\tilde{u}}{dl} &= \epsilon \tilde{u} - \frac{34}{5} \tilde{u}^2
\end{aligned}$$
The ladder has the Khmel'nitskii-Shneerson fixed point $\tilde{u}^* = \frac{5\epsilon}{34}$,
with the critical exponent
$$
	\nu_\mathrm{KS}^{-1} = \left(\frac{d\log r}{d\log b}\right)_{\tilde{u}=\tilde{u}^*_\mathrm{KS}}
		= 2-\frac{25}{51}\epsilon
$$
and consequently
$$
	\alpha_\mathrm{KS} = 2 - (d+z)\nu_\mathrm{KS} = \frac{1}{102} \epsilon + \O(\epsilon^2)
$$
Note that this is different from the Wilson-Fisher universality class,
where we would expect for $N = 3$ a correlation length exponent of
$\nu_\mathrm{WF}^{-1} = 2 - \frac{5}{11}\epsilon$.

At the upper critical dimension $\epsilon = 0$,
we have logarithmic corrections.
The RG flow equation for $r$ can then be solved exactly and we obtain
$$
    r \propto b^{-2} (-\log b)^{-\frac{25}{51}}
$$
cmp. [@KhmelnitskiiShneerson1973].

### Relevance of cubic anisotropy in the presence of dipolar interaction {#sec:cubic-anisotropy-dipolar}

The above equations have only two fix points corresponding to the Gaussian and Khmelnitskii-Shneerson.
Both of them are unstable.
This indicates that if the system starts with a non-vanishing cubic anisotropy,
it will always eventually end up with runaway flow.

![RG flow in the presence of cubic anisotropy and dipolar interaction](figures/cubic_anisotropy_figures/cubic_anisotopy_with_dipolar_rg_flow_3d_mod_sw.pdf)

Without dipolar interaction, we saw that above a certain flavor number ($N_c = 4$ at one-loop order),
the cubic fixed point becomes stable.
It turns out that, if we add dipolar interaction, there is also a critical flavor number
above which the RG flow is qualitatively different.
However, the flavor number is now tied to the spatial dimension.
At one-loop order, it appears that above $d = 8$ dimensions an equivalent of the cubic fixed point appears.
Of course this puts $d+z$ far above the upper critical dimension and the Gaussian fixed point is stable.

It stands to reason that the critical flavor number will be modified,
if one includes higher order corrections into the RG procedure.
This calculation has so far not been done and,
if higher loop corrections would lower the critical flavor number below the upper critical dimension
is at this point an open question.


## RG analysis of an isotropic ferroelectric {#sec:rg-isotropic-ferroelectric}
We now seek to describe a ferroelectric transition coupled to an elastic medium.
As usual we use the minimal, most symmetric model that captures most of the physics.
In this case the minimal model is an isotropic elastic medium coupled to a Lorentz invariant $\phi^4$ theory
It couples to the strain tensor $\strain$, representing the acoustic branch, with a generic quadratic term.
$$\begin{aligned}
    \mathcal{S} = \int d^3r d\tau \Bigg[&
        \frac12 \vphi^T {G^{(0)}}^{-1} \vphi + \frac{u}{4!} (\phi^2)^2 \\
        &+ \lambda_1 \tr\{\strain\} \phi^2 + 2\lambda_2 \vphi^T\strain\vphi \\
        &+ \frac\rho2 \left((\partial_\tau \mathbf{u})^2 + c_l^2\tr\{\strain\}^2 + 2c_t^2 \tr\{\tilde\strain\tilde\strain\}\right) \Bigg]
\end{aligned}$$
where $c_l$ and $c_t$ are the longitudinal and transversal sound velocity (see [@eq:isotropic-sound-velocities]).

As described in the previous section consider the limit of strong dipolar interaction,
leading to a transversal projector in the bare propagator of the critical modes
$$
   G^0_{ij}(q, \omega) = \frac{\delta_{ij} - \hat{q}_i \hat{q}_j}{\omega^2 + c^2q^2 + r} \;.
$$
For the acoustic phonons on the other hand we have both longitudinal and transversal mode
$$
   D^0_{ij}(q, \omega) = \frac{\hat{q}_i \hat{q}_j}{\omega^2 + c_l^2q^2} + \frac{\delta_{ij} - \hat{q}_i \hat{q}_j}{\omega^2 + c_t^2q^2} \;.
$$
Previously, we ignored the second term,
but because of the additional coupling term $\lambda_2$ that couples shear strain to the order parameter
we must include it now and we expect a non-trivial RG flow for $c_t$.
For convenience we define a rank 4 tensor for the coupling
$$
    \lambda_{ijkl} = \lambda_1 \delta_{ij} \delta_{kl}
        + \lambda_2 (\delta_{ik} \delta_{jl} + \delta_{il} \delta_{jk})
$$
analogous to the isotropic stiffness tensor [@eq:isotropic-stiffness-tensor].
The interaction can then be written as
$$
    \lambda_1 \phi^2 + 2\lambda_2 \vphi^T\strain\vphi =
        \lambda_{ijkl} \strain_{ij} \phi_k \phi_l \;.
$$
Importantly, the second term ($\lambda_2$) is not purely determined by shear strain.
We can see this by writing
$$
    \strain = \frac1d \tr\{\strain\} \one + \tilde{\strain}
$$
where is $\tilde{\strain}$ is the traceless strain tensor,
given by inverting the above equation.
Clearly,  $\tr\{\tilde{\epsilon}\} = 0$.
We can then rewrite the elastic coupling as
$$\begin{aligned}
	\lambda_1\tr\{\strain\} \phi^2 + 2\lambda_2 \vphi^T\tilde{\strain}\vphi
        &= \left(\lambda_1 + \frac2d\lambda_2\right)\tr\{\strain\} \phi^2
			+ 2\lambda_2 \vphi^T\tilde{\strain}\vphi \\
        &\equiv \lambda_l \phi^2 + 2\lambda_t \vphi^T\tilde{\strain}\vphi \;.
\end{aligned}$$
So $\lambda_l = \lambda_1 + 2\lambda_2/d$ is the "longitudinal coupling".

### Calculation of corrections
The complete self energy correction to one-loop order for the critical field amounts to
$$\begin{aligned}
    TODO \quad&=\quad \diagramPhiSelfEnergyV \quad+\quad \diagramPhiSelfEnergyLL \\
    \Sigma^{(1L)}_{ll'}(\q, \omega) &= 2\,\frac{u_0}{4!} \int_{q',\omega'}
		\left(2\delta_{ll'}G^{(0)}_{kk} + 4G^{(0)}_{ll'}\right) \\
        &- \frac{4}{\rho} \lambda_{0,ijkl} \lambda_{0,i'j'k'l'} \int_{q',\omega'} G^{(0)}_{kk'}(q'-q,\omega'-\omega) q'_i D^{(0)}_{jj'}(q',\omega) q'_{i'} \;.
\end{aligned}$$
Here a factor of 2 was included to compensate for the $1/2$ prefactor in the Gaussian term of the action
and the last term includes an extra factor of $1/2$ because it is second order in the coupling.
The first of the two corrections was already calculated in [@sec:phi4-dipolar]
$$
    2\,\frac{u_0}{4!} \int_{q,\omega} \left(2\delta_{ij}G^{(0)}_{kk} + 4G^{(0)}_{ij}\right) =
        - \frac{1}{48\pi^2c_0^3} \left(d + 1 - \frac2d\right) u_0 r_0 \log\Lambda
$$
The second correction to the self-energy is more involved,
since it depends on an external momentum and therefore contains projectors of the form
$$
    G^{(0)}_{ij}(q'-q,\omega'-\omega)
		\propto \left(\delta_{ij} - \frac{(q'-q)_i (q'-q)_j}{(q'-q)^2}\right) \;.
$$
Together with the momentum dependence of the phonon propagator
it follows that the integrand has a complicated external momentum dependence.
To keep the discussion here at least somewhat compact,
the full calculation is done in [@sec:appendix-self-energy-diagram],
while here we simply give the result for the corrections to the wave function renormalization,
the tuning parameter and the velocity of the critical mode
$$\begin{aligned}
    Z_\phi^{-1} &= Z_{\phi,0} \left(1 + \frac{2}{3\pi^2\rho} \left[
        \frac{3d-5}{2(d-1)} \frac{\lambda_1^2}{cc_l(c+c_l)^3}
            + \frac{\lambda_2^2}{cc_t(c+c_t)^3}
    \right]_0 \log\Lambda \right) \\
    r &= r_0
        - \frac{1}{48\pi^2c_0^3} \left(d + 1 - \frac2d\right) u_0 r_0 \log\Lambda \\
        &\qquad+ \frac{1}{3\pi^2\rho} \left[
        \frac{3d-5}{2(d-1)} \frac{2c + c_l}{c^3c_l(c+c_l)^2}\lambda_1^2
            + \frac{2c + c_t}{c^3c_t(c+c_t)^2}\lambda_2^2
    \right]_0 r_0 \log\Lambda \\
    c^2 &= c_0^2 + \frac{1}{15\pi^2\rho(d-1)}
        \bigg[
            \frac{(5d-9) c - 2c_l}{c_l(c+c_l)^3}\lambda_1^2 \\
            &\qquad\qquad+
                %\frac{24}{cc_l(c+c_l)}
                %    \left(\lambda_1 + \frac43 \lambda_2\right)\lambda_2 \\
                \frac{8}{cc_l(c+c_l)^2} \left((c+2c_l)\lambda_1 + 4 (c+c_l)\lambda_2\right)\lambda_2 \\
            &\qquad\qquad+
                %10\frac{(4d - 6)c^2 + (5d-9) cc_t + 2(d-2)c_t^2}{cc_t(c+c_t)^3}\lambda_2^2
                2\frac{(4d - 18)c^2 + (7d-33) cc_t + (4d-16)c_t^2}{cc_t(c+c_t)^3}\lambda_2^2
        \bigg]_0 \log\Lambda \;,
\end{aligned}
$$
where for conciseness of notation we just put the index 0 behind the brackets
to mean that all parameters enclosed are bare parameters.

On the other hand, the acoustic phonon self-energy correction is given by
$$\begin{aligned}
    TODO \quad&=\quad \diagramUSelfEnergyLL \\
    \Pi_{ii'} &= \rho_0 \omega^2 \delta_{ii'} + C_{0,ijki'} q_j q_k \\
        &\qquad- 2q_j q_{j'} \lambda_{0,ijkl} \lambda_{0,i'j'k'l'} \int_{q',\omega'} G^0_{kk'} G^0_{ll'} \;,
\end{aligned}$$
where we express the propagator in terms of the elastic tensor $C$, which in an isotropic solid is of the form
[@eq:isotropic-stiffness-tensor].
Using the relations [@eq:angle-integrals] as well as the integral $I_2$ we obtain
$$\begin{aligned}
    Z_u\big(\rho\omega^2 \delta_{il} &+ C_{ijkl} q_j q_k\big) = Z_{u,0} \big(\rho_0 \omega^2 \delta_{il} + C_{0,ijkl} q_j q_k\big) \\
        &- \frac{1}{8\pi^2c_0^3} \Bigg[ \left(2(d - 1) \lambda_{0,1}^2
            + 8\,\frac{d-1}{d} \lambda_{0,1}\lambda_{0,2}
            + \frac{8}{d(d+2)} \lambda_{0,2}^2\right) \delta_{ij}\delta_{kl} \\
        &\qquad\qquad\qquad+ 4\frac{d^2 - 2}{d(d+2)} \lambda_{0,2}^2 (\delta_{ik}\delta_{jl}
            + \delta_{il}\delta_{jk}) \Bigg] q_j q_k \log\Lambda \;.
\end{aligned}
$$
In terms of the parameters $\kappa$ and $\mu$ used in the initial action this amounts to
$$
\begin{aligned}
    \kappa &= \kappa_0 - \frac{1}{4\pi^2c_0^3} \left((d - 1) \lambda_{0,1}^2
        + 4\,\frac{d-1}{d} \lambda_{0,1}\lambda_{0,2}
        + \frac{4}{d(d+2)} \lambda_{0,2}^2\right) \log\Lambda \\
    \mu &= \mu_0 - \frac{1}{4\pi^2c_0^3} \frac{d^2 - 2}{d(d+2)} \lambda_{0,2}^2 \log\Lambda
\end{aligned}
$$
while $\rho = \rho_0$ is not renormalized (there is no nontrivial $Z$ factor as there was for $\vphi$).

A similar procedure is used for the corrections on $u$, $\lambda_1$ and $\lambda_2$.
Here we have to be careful with the different contractions again,
for example
\def\DiagramScale{0.45}
$$
\begin{aligned}
    &\diagramVCorrectionVV{i}{j}{k}{l}
        = \diagramVCorrectionVVLoop{i}{j}{k}{l}{i'}{j'}{k'}{l'}
            + \diagramVCorrectionVVPenguin{i}{j}{k}{l}{i'}{j'}{k'}{l'}
            + \diagramVCorrectionVVBox{i}{j}{k}{l}{i'}{j'}{k'}{l'} \\
        &\quad= \frac12 \left(\frac{u_0}{4!}\right)^2 \left(
            2^3 \delta_{ij}\delta_{kl} \int_{q,\omega} G^0_{mn} G^0_{mn}
            + 2^5 \delta_{ij} \int_{q,\omega} G^0_{km} G^0_{ml}
            + 2^5 \int_{q,\omega} G^0_{ij} G^0_{kl}
        \right)
\end{aligned}
$$
and
$$
\begin{aligned}
    \diagramVCorrectionVLL{i}{j}{k}{l}
        &= \diagramVCorrectionVLLPenguin{i}{j}{k}{l}{i'}{k'}{j'}{l'}
        + \diagramVCorrectionVLLBox{i}{j}{k}{l}{i'}{k'}{j'}{l'} \\
        &= \frac{1}{2\rho} \frac{u_0}{4!} \lambda_{kk'mm'} \lambda_{ll'nn'}
            \!\int_{q,\omega}\!\! \left(2^4 \delta_{ij} G^0_{i'k'} G^0_{i'l'}
                + 2^5 G^0_{ik'} G^0_{jl'}\right) q_m D^0_{m'n} q_{n'} % \,.
\end{aligned}
$$
So in total for the dressed $u$-vertex we get
\def\DiagramScale{0.6}
$$
\begin{aligned}
    \diagramVDressed{}{}{}{} &\quad=\qquad
        \diagramVBare{}{}{}{}
        ~+~ \diagramVCorrectionVV{}{}{}{} \\
        &\quad~+~ \diagramVCorrectionVLL{}{}{}{}
        ~+~ \diagramVCorrectionLLLL{}{}{}{}
\end{aligned}
$$
$$
\begin{aligned}
    u &= u_0 - \frac{4}{4!} \left(d + 7 - \frac{12}{d} + \frac{12}{d(d+2)}\right) u_0^2 I_{20} \\
        &+ \frac8\rho \left[ \frac{3d^2 + d - 4}{d(d + 2)} \lambda_1^2 I_{21}(c_l)
        + \frac{d^2 + 3d - 4}{d (d + 2)} \lambda_2^2 I_{21}(c_t) \right]_0 u_0 \\
        &- \frac{4! \cdot 96}{4! \rho^2} \frac{d - 1}{d(d+2)} \bigg[(d + 1) \lambda_1^4 I_{22}(c_l, c_l)
            + 2 \lambda_1^2 \lambda_2^2 I_{22}(c_l, c_t) + 3 \lambda_2^4 I_{22}(c_t, c_t)\bigg]_0
\end{aligned}
$$
where we multiplied both sides by $4!$.
We also defined
$$
    I_{mn}(\{c_i\}) =
        \int_{q,\omega} \left(\frac{1}{\omega^2 + c^2q^2 + r}\right)^m
            \prod_i^n \left(\frac{q^2}{\omega^2 + c_i^2 q^2}\right)
$$
where $c_i$ can be $c_l$ or $c_t$.
The expression for these integrals are given later.
Now for the corrections to the $\lambda$-vertex we need
\def\DiagramScale{0.45}
$$
\begin{aligned}
    \diagramLCorrectionVL{i}{j}{k}{l}
        &= \diagramLCorrectionVLLoop{i}{j}{k}{l}{}{}{}{}
            + \diagramLCorrectionVLPenguin{i}{j}{k}{l}{}{}{}{} \\
        &= \frac{2}{2!} \frac{u}{4!} \lambda_{iji'j'}
            \int_{q,\omega} \left(4 \delta_{kl} G^0_{i'k'} G^0_{j'k'} + 8 G^0_{i'k} G^0_{j'l}\right) \;.
\end{aligned}
$$
The full dressed vertex is then given by
\def\DiagramScale{0.6}
$$
    \diagramLDressed{}{}{}{} \quad=\quad \diagramLBare{}{}{}{}
        ~+~ \diagramLCorrectionVL{}{}{}{}
        ~+~ \diagramLCorrectionLLL{}{}{}{}
$$
$$
\begin{aligned}
    \lambda_1 \delta_{ij}\delta_{kl}& + \lambda_2 (\delta_{ik}\delta_{jl} + \delta_{il}\delta_{jk}) \\
        =~ &\lambda_{0,1} \delta_{ij}\delta_{kl}
            + \lambda_{0,2} (\delta_{ik}\delta_{jl} + \delta_{il}\delta_{jk}) \\
        &- \frac{u_0}{4!} \lambda_{iji'j'}
            \int_{q,\omega} \left(4 \delta_{kl} G^0_{i'k'} G^0_{j'k'} + 8 G^0_{i'k} G^0_{j'l}\right) \\
        &+ \frac4\rho \lambda_{iji'j'} \lambda_{kk'mm'} \lambda_{ll'nn'}
            \int_{q,\omega} G^0_{i'k'} G^0_{j'l'} q_m D^0_{m'n} q_{n'}
\end{aligned}
$$
from which we obtain
$$\begin{aligned}
    \lambda_1 =~ \lambda_{0,1}
        &- \frac{4}{4!} \bigg[\frac{d^2 + d - 2}{d} \lambda_{0,1}
            + 2 \frac{d-1}{d+2} \lambda_{0,2}\bigg] u_0 I_{20} \\
        &+ \frac4\rho \bigg[\frac{d-1}{d} \lambda_1^3 I_{21}(c_l)
            + \frac{2}{d(d+2)} \lambda_1^2 \lambda_2 I_{21}(c_l) \\
            &\quad\qquad+ \frac{d-1}{d} \lambda_1 \lambda_2^2 I_{21}(c_t)
            + 2 \frac{d+1}{d(d+2)} \lambda_2^3 I_{21}(c_t) \bigg]_0
\end{aligned}$$
$$\begin{aligned}
    \lambda_2 = \lambda_{0,2}
        &- \frac{8}{4!} \frac{d^2 - 2}{d(d+2)}\lambda_{0,2} u_0 I_{20} \\
        &+ \frac4\rho \left[\frac{d^2 - 2}{d(d+2)} \lambda_1^2 \lambda_2 I_{21}(c_l)
            - \frac{2}{d(d+2)}\lambda_2^3 I_{21}(c_t)\right]_0
\end{aligned}$$
To arrive at the RG equations we need to evaluate the integrals appearing in the above expressions.
Most of them can be obtained from previous integrals as a derivative with respect to some parameter
like
$$
    I_{21}(c_i) = - \frac{\partial}{\partial r} I_{11}(0, 0; c_i)
        = \frac{1}{8\pi^2} \frac{2 c + c_i}{c^3 c_i (c+c_i)^2}\log\Lambda
$$
and
$$
    I_{22}(c_i, c_i) = - \frac{1}{2c_i}\frac{\partial}{\partial c_i} I_{21}(c_i)
        = \frac{1}{8\pi^2} \frac{c^2 + 3 cc_i + c_i^2}{c^3c_i^3(c + c_i)^3} \log\Lambda \;.
$$
The integral $I_{22}(c_l, c_t)$ can not be derived in this way.
However, extracting the logarithmic divergence is still possible by expanding in $r$ (only 0th term contributes)
$$
\begin{aligned}
    I_{22}(c_l, c_t) &= \int_{q,\omega} \frac{q^4}{(\omega^2 + c^2 q^2)^2 (\omega^2 + c_l^2 q^2) (\omega^2 + c_t^2 q^2)} + \mathcal{O}(r) \\
        &= \frac{1}{8\pi^2} \frac{2c^3 + (2c + c_l) (2c + c_t) (c_l + c_t)}{c^3 c_l c_t (c+c_l)^2 (c+c_t)^2 (c_l+c_t)} \log\Lambda
\end{aligned}
$$
Note that in the case $c_l = c_t$ this is the same as $I_{22}(c_i, c_i)$.

### RG flow equations
In Kardanoff RG the integrals are not from $0$ to $\Lambda$, but from $\Lambda/b$ to $\Lambda$ for some $b > 1$.
Rescaling then with $k = k'/b$, $\omega = \omega'/b$ and $\phi_k = b^{\frac{d-3z}{2}} \sqrt{Z_\phi} \phi'_{k'}$ gives
$$
\begin{aligned}
    c^2 &= c'^2/ (Z_\phi b^{2z-2}), \qquad
		&u &= u' / (Z_\phi^2 b^{3z-d}), \\
    c_l^2 &= c_l'^2 / (Z_u b^{2z-2}), \qquad
		&\lambda_1 &= \lambda_1' / (\sqrt{Z_u} Z_\phi b^{\frac{5z-d-2}{2}}), \\
    c_t^2 &= c_t'^2 / (Z_u b^{2z-2}), \qquad
		&\lambda_2 &= \lambda_2' / (\sqrt{Z_u} Z_\phi b^{\frac{5z-d-2}{2}}), \\
	r &= r'/ (Z_\phi b^{-2z}). \\
\end{aligned}$$
With $z = 1$ and $d = 3 - \epsilon$ we arrive at
$$\begin{aligned}
    \frac{dZ_\phi}{d\ell} &= -\frac{2}{3\pi^2\rho} \left[
        \frac{\lambda_1^2}{cc_l(c+c_l)^3} + \frac{\lambda_2^2}{cc_t(c+c_t)^3}
    \right] \\
    \frac{dc^2}{d\ell}
        &= - \frac{1}{15\pi^2\rho} \bigg[\frac{13 c - c_l}{c_l(c+c_l)^3}\lambda_1^2
            + \frac{4}{cc_l(c+c_l)^2} \big((c+2c_l)\lambda_1 + 4 (c+c_l)\lambda_2\big)\lambda_2 \\
		&\qquad\qquad\qquad+ \frac{4(c^2 - 3 c c_t - c_t^2)}{cc_t(c+c_t)^3}\lambda_2^2\bigg] \\
    \frac{dr}{d\ell} &= \bigg(2
        - \frac{5}{3\pi^2c^3} \frac{u}{4!}
        + \frac{1}{3\pi^2\rho c^3} \left[\frac{3c+c_l}{(c+c_l)^3} \lambda_1^2
            + \frac{3c+c_t}{(c+c_t)^3} \lambda_2^2\right]\bigg) r \\
\end{aligned}$$
as well as the elastic constants
$$\begin{aligned}
    \frac{dc_l^2}{d\ell} &= - \frac{1}{2\pi^2\rho c^3} \left(\lambda_1^2
            + \frac43 \lambda_1\lambda_2 + \frac{16}{15}\lambda_2^2\right)
        = - \frac{1}{2\pi^2\rho c^3} \left(\lambda_l^2 + \frac{28}{45}\lambda_t^2\right) \\
    \frac{dc_t^2}{d\ell} &= - \frac{7}{30\pi^2\rho c^3} \lambda_2^2
\end{aligned}$$
The couplings
$$\begin{aligned}
    \frac{d\lambda_1}{d\ell} &= \frac\epsilon2 \lambda_1
        - \frac{1}{\pi^2c^3} \bigg(\frac{5}{3} \lambda_1
            + \frac{2}{5} \lambda_2\bigg) \frac{u}{4!}
        + \frac{1}{3\pi^2\rho c^3} \left(
                \frac{3c + c_l}{(c+c_l)^3} \lambda_1^2
                + \frac{3c + c_t}{(c+c_t)^3} \lambda_2^2
            \right) \lambda_1 \\
        &\qquad+ \frac{1}{15\pi^2\rho c^3} \left(
                \frac{2 c + c_l}{c_l (c+c_l)^2} \lambda_1^2
                + 2 \frac{2 c + c_t}{c_t (c+c_t)^2} \lambda_2^2
            \right) \lambda_2 \\
    \frac{d\lambda_2}{d\ell} &= \frac\epsilon2 \lambda_2
        - \frac{7}{15\pi^2c^3} \lambda_2 \frac{u}{4!} \\
        &\qquad+ \frac{1}{30\pi^2\rho c^3} \bigg(
            \frac{7 c_l^2 + 21 c c_l - 6 c^2}{c_l (c+c_l)^3} \lambda_1^2
            - 2\, \frac{c_t^2 + 3 c c_t + 12 c^2}{c_t (c+c_t)^3} \lambda_2^2
        \bigg) \lambda_2
\end{aligned}$$
and
$$
\begin{aligned}
    \frac{du}{d\ell} = \epsilon u &- \frac{17}{5\pi^2c^3} \frac{u^2}{4!} \\
        &+ \frac{2}{15\pi^2\rho c^3} \bigg( \frac{21c^2 + 39 cc_l + 13 c_l^2}{c_l(c + c_l)^3} \lambda_1^2
			+ \frac{27 c^2 + 48 cc_t + 16 c_t^2}{c_t(c + c_t)^3} \lambda_2^2 \bigg) u \\
        &- \frac{8}{5\pi^2\rho c^3} \bigg(
            4 \frac{c^2 + 3 cc_l + c_l^2}{c_l^3(c + c_l)^3} \lambda_1^4
            + 3\, \frac{c^2 + 3 cc_t + c_t^2}{c_t^3(c + c_t)^3} \lambda_2^4 \\
            &\quad\qquad+ 2\,\frac{2 c^3+(c_l+2 c) (c_t+2 c) (c_l+c_t)}{
                c_l c_t (c_l+c)^2 (c_t+c)^2 (c_l+c_t)} \lambda_1^2 \lambda_2^2\bigg)
\end{aligned}
$$
<!--As the system of equations has many similarities with the one encountered in [@sec:quantum-criticality-compressible-lattice],-->

We would like to identify any instabilities that may occur,
therefore it helps to write down stability requirements.
The system is microscopically stable as long as
$$
    c_l^2 > 0 \quad \text{and} \quad c_t^2 > 0 \;.
$$
If one of the phonon velocities vanishes the system becomes unstable
with respect to the corresponding phonon.
On ther other hand, for macroscopic stability it must hold that
$$
    K = \rho\left(c_l^2 \kappa - \frac43 c_t^2\right) > 0
    \qquad \text{and} \qquad
    \mu = \rho c_t^2 > 0 \;.
$$
If the bulk modulus $K$ vanishes the system becomes unstable with respect to volume changes
which results in an isostructural transition.
If however the shear modulus $\mu$ vanishes the system is unstable with respect to changes in shape.
This indicates a solid-liquid transition, i.e. melting.

To identify these instabilities it is convenient to look at the flow
of the dimensionless parameters $\vartheta_l = c_l/c$ and $\vartheta_t = c_t/c$.
If $\mu$ vanishes, so does $c_t$ and so does $\vartheta_t$.
For them the global stability condition reads
$$
    K > 0 \iff c_l^2 > \frac43 c_t^2 \iff \vartheta_l > \frac{2}{\sqrt{3}} \vartheta_t \;.
$$
Flow of bulk modulus $K = \rho (c_l^2 - \frac43 c_t^2)$ and shear modulus $\mu = \rho c_t^2$
$$
\begin{aligned}
    \frac{dK}{d\ell} &= - \frac{1}{2\pi^2 c^3} \lambda_l^2 \\
    \frac{d\mu}{d\ell} &= - \frac{7}{30\pi^2 c^3} \lambda_t^2
\end{aligned}
$$
