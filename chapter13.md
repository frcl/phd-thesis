# Quantum criticality on a compressible lattice

After developing a thorough understanding of how a classical phase transition changes trough lattice coupling,
we can turn our attention to the quantum phase transition and ask if the story will bi similar.
An obvious question is if at a quantum critical point the critical exponents are eventually Fisher-renormalized.
In will turn out that this is not the case and we should not necessarily expect Fisher-renormalization,
as will be explained in [@sec:larkin-pikin-quantum-breakdown].

A natural extension of the theory investigated by [@LarkinPikin1969] and [@BergmanHalperin1976]
is to couple the Lorentz invariant model that was already analyzed in [@sec:example-phi4]
in the same manner as the in the classical theory, with a quadratic coupling.
This means we restrict ourselves to the case $z=1$
and for other dynamical exponents $z$ the results may differ.
An important feature of the elastic coupling is that it explicitly breaks Lorentz invariance.
This must necessarily be the case since elastic medium makes this effectively an 'aether' theory.

The breaking of a symmetry, such as Lorentz invariance, is often associated with a change in universality class.
We therefore expect a significant change in critical behavior, provided that the elastic coupling is relevant.
The Larkin-Pikin criterion for non-perturbative elastic coupling has a strait-forward generalization to quantum criticality.
If specific heat exponent of the quantum transition
$$
	\alpha_q = 2 - \nu_q d_\mathrm{eff} = 2 - \nu_q (d+z) \;,
$$ {#eq:quantum-criterion}
is positive, then non-perturbative elastic coupling is expected.

TODO: talk about linear vs. quadratic coupling again, cite works on linear coupling/critical elasticity

* relevant for antiferromagnetic quantum phase transition in pressure tuned TlCuCl3 [@Ruegg_2008]
	- maybe move to end when results are known
* motivate approach from classical criticality
    - Bergman-Halperin approach is most transparent and most general (no need to chose boundary conditions)

TODO: change $c_L$ to $c_l$ in figures, maybe also change figures to fit width

## Breakdown of the Larkin-Pikin procedure at quantum criticality {#sec:larkin-pikin-quantum-breakdown}

TODO: quantum annealed criticality? [@Chandra2020]

In analogy to the Larkin-Pikin action, just adding the imaginary time derivatives
$$\begin{aligned}
    \mathcal{S} = \int d&\tau d^dx \Bigg(\frac12 \vphi^T \left(- \partial_\tau^2 - c^2\nabla^2 + r\right) \vphi
			+ \frac{u}{4!} (\phi^2)^2 + \lambda \phi^2 (\nabla \cdot u) \\
        & + \frac{K}{2} \tr\{E\}^2 + \frac\rho2 \left[
			(\partial_\tau\u)^2
			+ c_l^2 (\nabla\cdot\u)^2
			+ 2c_t^2 \tr\{\tilde\strain\tilde\strain\}
		\right]\Bigg) \;.
\end{aligned}$$
We have added dynamic terms for the $\vphi$ and $\u$ fields,
however one might ask why this action does not contain time derivatives of the global strain $E$.
It turns out that for an infinite system $E$ is not an eigenmode in the elastic dynamics.
Rather it is a purely static degree of freedom that only enters via the boundary conditions.
In a finite system, where boundary effects become relevant, the situation can be far more complicated.
However, throughout this chapter we assume that boundaries are unimportant.

After integrating out phonons with the same procedure as in [@sec:larkin-pikin-procedure] we obtain
$$\begin{aligned}
    \mathcal{S} = \int d\tau &d^dx \frac12 \vphi^T \left(- \partial_\tau^2 - c^2\nabla^2 + r\right) \vphi
			+ v V^{-1} \left(\int d\tau d^dx \phi^2\right)^2 \\
        &+ \int d\tau d^dx \int d\tau' d^dx' \phi(x,\tau)^2 \Gamma(x-x',\tau-\tau') \phi(x',\tau')^2 \\
\end{aligned}$$ {#eq:quantum-action-inegrated-phonons}
where $v$ is the same as in [@eq:non-local-interaction-constant] and there is another non-local interaction with the vertex function
$$
	\Gamma(q,\omega) = \frac{u}{4!} + \frac{\lambda^2}{\rho} \frac{q^2}{\omega^2 + c_l^2q^2} \;.
$$
Note that $\Gamma$ is non-analytic around $(q, \omega) = (0, 0)$.
This can be most easily seen by taking the non-commuting limits $\lim_{q\to 0} \Gamma = u/4!$ and $\lim_{\omega\to 0} \Gamma = u/4! + \lambda^2/\rho c_l^2$.
The latter corresponds to the renormalization of the self-interaction that we encountered in the classical theory.
In fact, Larkin-Pikin physics could be recovered by taking the classical limit for phonons only
by taking the mass density to vanish $\rho \to 0$.

If there was a manifestly Lorentz invariant coupling to a four-displacement $\phi^2 \partial_\mu u^\mu$,
this would be a truly quantum version of Larkin-Pikin.
However, there is no time component of the displacement field and it is not clear what such a field would represent.

We conclude that Larkin-Pikin procedure does not work for quantum criticality.
Neither does Fishers hidden variable approach,
since the non-analytic coupling can not be gotten rid of using the Hubbard-Stratonovich transformation to a macroscopic strain like variable.
Instead the Hubbard-Stratonovich field would have to depend on space and time, effectively reintroducing the strain field.
Hence, there is no hidden thermodynamic variable, such as was assumed in [@sec:thermodynamics-compressible-lattice].

To learn about the critical behavior of the quantum system we must resort to RG.
As in the classical theory one could perform an RG procedure on the action with phonons already integrated out [@eq:quantum-action-inegrated-phonons],
but this is complicated considerably by the non-analytic vertex function.
Nonetheless this was done in [@Samanta2022], although the infinite range interaction was neglected in that work ($v=0$), restricting the applicability of the results.
The other option if to perform the RG procedure on the original action, including phonons, which is what is done in the following.


## RG analysis of a quantum phase transition in an isotropic elastic medium {#sec:quantum-criticality-compressible-lattice}
From the argument in the previous section, we can already see that the quantum phase transition
responds very differently to lattice coupling than the classical transition.
This section closely follows the contents of [@Sarkar2023].

We take the model already introduced in the previous section for an $O(N)$ quantum phase transition,
coupled to an isotropic elastic medium.
The only difference is that, since we do not integrate out strain,
we can ignore the $q = 0$ strain components,
i.e. we drop all terms depending on $E$ and write
$$\begin{aligned}
    \mathcal{S} = \int d\tau d^dx \Bigg(\frac12 &\vphi^T \left(- \partial_\tau^2 - c^2\nabla^2 + r\right) \vphi
        + \frac{u}{4!} (\phi^2)^2 + \lambda \phi^2 (\nabla \cdot u) \\
        &+ \frac\rho2 \left[
			(\partial_\tau\u)^2
			+ c_l^2 (\nabla\cdot\u)^2
			+ 2c_t^2 \tr\{\tilde\strain^2\}
		\right]\Bigg) \;.
\end{aligned}$$
In $3+1$ dimensions both the $u$ and the $\lambda$ coupling terms are marginal.
Following the same procedure that was applied in [@BergmanHalperin1976] and outlined in [@sec:rg-anisotropic],
we perform a Wilsonian RG by calculating the self-energy and vertex corrections to one-loop,
followed a rescaling of coordinates to derive RG flow around the Gaussian fixed point.

### Calculation of the corrections

The fluctuations of the order parameter field $0$th order are characterized by the diagonal Green function
$$
	G^{(0)}_{\alpha \beta}(\q, \omega) = \frac{\delta_{\alpha \beta}}{\omega^2 + c^2 q^2 + r}\;.
$$
The fluctuations of the displacement field with $\q \neq 0$ on the other hand are split into a longitudinal and a transversal contribution
$$
	F^{(0)}_{ij}(\q, \omega) = ((\rho \omega^2 + D(\q))^{-1})_{ij} %\\
		= \frac{1}{\rho} \Big[\frac{\hat q_i \hat q_j}{\omega^2 + c_l^2 q^2} + \frac{\delta_{ij} - \hat q_i \hat q_j}{\omega^2 + c_t^2 q^2} \Big] \;.
$$
As the strain enters the elastic coupling only via its trace,
analogous to [@sec:rg-anisotropic] this will project on the longitudinal modes.
The bare and dressed Greens functions are connected by
$$\begin{aligned}
	G^{-1}(\q,\omega) &= {G^{(0)}}^{-1}(\q,\omega) - \Sigma(\q,\omega) \;, \\
	F^{-1}(\q,\omega) &= {F^{(0)}}^{-1}(\q,\omega) - \Pi(\q,\omega) \;.
\end{aligned}$$
For notational convenience we define the integrals
$$\begin{aligned}
	I_{mn}(\q,\omega) &= \int_{q',\omega'} (G^{(0)}(\q - \q',\omega - \omega'))^m
			(\q'^TF^{(0)}(\q',\omega')\q')^n \\
		&= \int_{q',\omega} \left(\frac{1}{(\omega-\omega')^2 + c^2(\q-\q')^2 + r}\right)^m
			\left(\frac1\rho \frac{q'^2}{\omega'^2 + c_l^2q'^2}\right)^n
\end{aligned}$$
such that the integrals from [@sec:example-phi4] are $I_1 = I_{10}$ and $I_2 = I_{20}$.
These turn out to be independent of external momentum, as can be seen by a simple shift of $\q'$.
However, $I_{11}$ for example does depends on external momentum.

We obtain to one-loop order the self-energy corrections
$$\begin{aligned}
	% TODO: self energy diagram
	\diagramPhiDressed \quad&=\quad
		\diagramPhiSelfEnergyV \quad+\quad \diagramPhiSelfEnergyLL \\
	\Sigma(\q, \omega) &= - 2 (2N + 4) \frac{u}{4!} I_{10} + 4 \lambda^2 I_{11}(\q, \omega),\\
    \diagramUDressed \quad&=\quad \diagramUSelfEnergyLL \\
	\Pi_{ij}(\q,\omega) &= 2N \lambda^2 I_{20} q_i q_j ,
\end{aligned}$$
and the vertex corrections
$$\begin{aligned}
	\frac{u}{4!} &= \frac{u_0}{4!} - (4N + 32) \left(\frac{u_0}{4!}\right)^2 I_{20} + 24\lambda_0^2 \frac{u_0}{4!} I_{21} - 4 \lambda_0^{4} I_{22},\\
	\lambda &= \lambda_0 - (4N + 8) \frac{u \lambda}{4!} I_{20} + 4\lambda^3 I_{21} \;.
\end{aligned}$$
The next step is to calculate the loop integrals.
Two of the integrals ($I_{10}$ and $I_{20}$) are already known from [@sec:example-phi4].
Similar to the realization that $I_{20}$ can be written as an $r$-derivative of $I_{20}$,
we can make our lives easier by writing $I_{21}$ as an $r$-derivative of $I_{11}$
and $I_{22}$ as an $c_l$-derivative of $I_{21}$.
In fact we only need to calculate one additional integral, which is
$$\begin{aligned}
    I_{11}(\q, \omega) &= \frac{1}{\rho} \int \frac{d^3q'd\omega'}{(2\pi)^4} \;
            \frac{1}{(\omega - \omega')^2 + c^2(q - q')^2 + r}
            \frac{q'^2}{\omega^2 + c_l^2 q^2} \;.
\end{aligned}$$
We may solve this integral introducing a Feynman parameter, using
$$
	\frac{1}{AB} = \int_0^1 dx \frac{1}{xA + (1-x) B} \;.
$$
The $\omega'$ integral may be simplified by completing the square in the denominator
and shifting $\omega' \to \omega' + (1-x)\omega$
$$\begin{aligned}
    \int_{-\infty}^\infty &d\omega' \; \frac{1}{(\omega - \omega')^2 + c^2(\q - \q')^2 + r} \; \frac{1}{\omega'^2 + c_l^2 q'^2} \\
        &= \int_{-\infty}^\infty d\omega' \int_0^1 dx \; \frac{1}{(x[\omega'^2 + c_l^2 q'^2] + (1-x)[(\omega - \omega')^2 + c^2(\q - \q')^2 + r])^2} \\
        &= \int_{-\infty}^\infty d\omega' \int_0^1 dx \; \frac{1}{(\omega'^2 + \Delta)^2} \;, \\
\end{aligned}$$
where we have defined
$$
    \Delta = x(1-x)\omega^2 + x c_l^2 q'^2 + (1-x)[c^2(q-q')^2 + r]
$$
which is of course independent of $\omega'$.
Now we can use
$$
    \int_{-\infty}^\infty d\omega' \; \frac{1}{(\omega'^2 + \Delta)^2} = \frac\pi2 \frac{1}{\Delta^\frac32} \;.
$$
For the $q'$ integral, we first substitute $q' = k/\alpha(x)$,
where $\alpha(x) = \sqrt{x c_l^2 + (1-x)c^2}$,
and obtain
$$\begin{aligned}
    I_{11} = \frac{\pi}{2\rho} \int_0^1 dx \frac{1}{\alpha(x)^{d+2}}
		\int d^dk \; \frac{k^2}{[k^2 - 2\beta(x)\k\cdot\q + (1-x)(x\omega^2 + c^2q^2+r)]^\frac32} \\
\end{aligned}$$
with $\beta(x) = (1-x) c^2/\alpha(x)$.
Now we can again complete the square in the denominator
and shift $\k \to \k + \beta\q$. 
This creates a $\k\cdot\q$ term in the enumerator which is an odd function in $\k$,
thus giving no contribution to the integral
$$\begin{aligned}
	\int d^dk \; \frac{k^2}{[(k-\beta(x)q)^2 + \Theta]^\frac32}
        &\overset{k\to k + \beta q}{=} \int d^dk \; \frac{(k+\beta(x)q)^2}{[k^2 + \Theta]^\frac32} \\
        &\quad=\quad \int d^dk \; \frac{k^2 + \beta(x)^2q^2}{[k^2 + \Theta]^\frac32} \;,
\end{aligned}$$
where we defined
$$
    \Theta = - \beta(x)^2 q^2 + (1-x)(x\Omega^2 + c^2q^2+r) \;.
$$
We now have two integrals with trivial angle dependence.
Since we work in $d = 3$ spatial dimensions the angular integral thus resolves to $S_3 = 4\pi$
and the radial integrals have the cutoff dependence
$$\begin{aligned}
    \int_0^{\alpha\Lambda} dk \; \frac{k^2}{(k^2 + \Theta)^\frac32}
        &= \asinh\left(\frac{\alpha\Lambda}{\sqrt{\Theta}}\right)
			- \frac{\alpha\Lambda}{\sqrt{\Theta + \alpha^2\Lambda^2}}
        \approx \log\left(\frac{2\alpha\Lambda}{\sqrt{\Theta}}\right) - 1
\end{aligned}$$
and
$$\begin{aligned}
    \int_0^{\alpha\Lambda} dk \; \frac{k^4}{(k^2 + \Theta)^\frac32}
        &= \frac32 \left(\alpha\Lambda \sqrt{\Theta + \alpha^2\Lambda^2} - \Theta \asinh\left(\frac{\alpha\Lambda}{\sqrt{\Theta}}\right)\right) \\
        &\approx \frac32 \left(\alpha\Lambda^2 - \Theta \log\left(\frac{2\alpha\Lambda}{\sqrt{\Theta}}\right)\right)
\end{aligned}$$
So we end up with a rather complicated $x$-integral:
$$
    I_{11} \sim \frac{K_d}{4\rho} \int_0^1 dx \; \frac{1}{\alpha^{5}}
        \left[\left( \beta^2 q^2 - \frac{3\Theta}{2}\right) \log\left(\frac{2\alpha\Lambda}{\sqrt\Theta}\right)
        + \frac32 \alpha^2 \Lambda^2\right]
$$
We use the following integrals TODO: shorten
$$
    \int_0^1 dx \; \frac{1}{\alpha^3} = \frac{2}{cc_l(c+c_l)}
$$
$$
    \int_0^1 dx \; \frac{5 \beta - 3(1-x)c^2}{2\alpha^5} = \frac{2c}{3c_l(c+c_l)^3}
$$
$$
    \int_0^1 dx \; \frac{(1-x)}{\alpha^5} = -\frac{2c + c_l}{c^3c_l(c+c_l)^3}
$$
$$
    \int_0^1 dx \; \frac{(1-x)x}{\alpha^5} = -\frac{2c}{c_l(c+c_l)^3}
$$
<!--
   -and finally arrive at
   -$$\begin{aligned}
   -    I_{11} \sim \frac{K_d}{4\rho} \bigg[& \frac{3}{cc_l(c+c_l)} \Lambda^2 \\
   -        &- \left(\frac{2}{cc_l(c+c_l)^3}\Omega^2 - \frac{2}{3 c c_l (c+c_l)^3} c^2q^2 + \frac{2 c + c_l}{c^3 c_l (c+c_l)^2} r\right)\log(\Lambda)\bigg]
   -\end{aligned}$$
   -->
As discussed before we can extract the cutoff dependence of the remaining integrals by derivative of $I_{11}$
$$\begin{aligned}
	I_{10} &\simeq \frac{1}{8\pi^2 c^3} \left[c^2 \Lambda^2 - r \log\Lambda \right], \\
	I_{20} &= -\frac{\partial I_{10}}{\partial r} \simeq \frac{1}{8\pi^2 c^3} \log\Lambda, \\
	I_{11} &\simeq \frac{1}{8\pi^2\rho} \frac{1}{c^3 c_l (c + c_l)}
		\left[c^2 \Lambda^2 - \frac{2c + c_l}{c + c_l} r \log\Lambda \right] \nonumber \\
			&\qquad- \frac{1}{4 \pi^2 \rho} \frac{1}{c c_l(c+ c_l)^3} \left( \omega^2 - \frac{1}{3} c^2 q^2 \right)  \log\Lambda \\
	I_{21} &= - \frac{\partial I_{11}}{\partial r} \simeq \frac{1}{8\pi^2\rho} \frac{2c + c_l}{c^3 c_l (c + c_l)^2} \log\Lambda, \\
	I_{22} &= - \frac{1}{\rho} \frac{\partial I_{21}}{\partial c_l^2} \simeq \frac{1}{8\pi^2\rho^2} \frac{c^2 + 3 c c_l + c_l}{c^3 c_l^3 (c + c_l)^2} \log\Lambda \;.
\end{aligned}$$


### RG flow equations

These results can be used to integrate out a momentum shell $q \in [\Lambda/b,\Lambda]$
with $b > 1$ and $\ell = \log b \ll 1$.
We then rescale momentum and frequencies according to
$$
	\q = \q'/b, \quad \omega = \omega'/ b^z\;.
$$
For now we let the dynamical exponent $z$ be arbitrary,
keeping in the back of our heads that physically it will have the bare value $z = 1$.
The fields scale like
$$\begin{aligned}
	\vphi(\q,\omega) &= \sqrt{Z_\phi} b^{\frac{d + 3 z}{2}} \vphi'(\q',\omega'),\\
	\u(\q,\omega) &= \sqrt{Z_u} b^{\frac{d + 3 z}{2}} \u'(\q',\omega'),
\end{aligned}$$
where we introduced two wave function renormalizations, $Z_\phi$ and $Z_u$.
Finally, the parameters of the action are rescaled according to
$$\begin{aligned}
	c^2 & = c'^2/(Z_\phi b^{(2 z -2)}),\quad
	c_l^2 = c'^2_l/(Z_u b^{(2 z -2)}),\nonumber\\
	u &= u'/(Z_\phi^2 b^{(3 z-d)}),\quad
	\lambda = \lambda'/(Z_\phi \sqrt{Z_u} b^{(5 z-d-2)/2}),\nonumber \\
	r&= r'/(Z_\phi b^{2 z}).
\end{aligned}$$
Imposing the RG conditions
$$\begin{aligned}
	G^{-1}(0,\omega)|_{r = 0} = \omega^2, \quad
	D^{-1}(\q,\omega)|_{r = 0} \overset{\q \to 0}{\to} \rho \omega^2,
\end{aligned}$$
we find the flow of the wave function renormalizations
$$\begin{aligned}
	\frac{d \log Z_\phi}{d \ell}
		&=- \frac{1}{\pi^2 \rho} \frac{\lambda^2}{c c_l (c + c_l)^3} \\
	\frac{d \log Z_u}{d \ell} &= 0 \;. \\
\end{aligned}$$
The full set of RG flow equations up to one-loop order is then complete with
$$\begin{aligned}
    \frac{d r}{d\ell}
        &= \bigg(2z
        + \frac{1}{2\pi^2\rho} \frac{3c + c_l}{c^3(c+c_l)^3} \lambda^2
        - \frac{N+2}{48\pi^2} \frac{u}{c^3}\bigg) r  ,\\
    \frac{d c^2}{d \ell} 
        &= \left(2z - 2 - \frac{4}{3\pi^2\rho} \frac{\lambda^2}{cc_l(c+c_l)^3} \right) c^2 ,\\
    \frac{d c_l^2}{d \ell}
        &= \left(2z - 2 - \frac{N}{4\pi^2\rho} \frac{\lambda^2}{c_l^2c^3}\right)c_l^2 ,\\
    \frac{d u}{d \ell}
        &= (3z - d)u
        - \frac{(N+8)}{48\pi^2} \frac{u^2}{c^3} \\
        &\qquad\qquad+ \frac{1}{\pi^2\rho} \frac{4 c^2 + 9 c c_l + 3 c_l^2}{c^3 c_l(c+c_l)^3} u \lambda^2
        - \frac{12}{\pi^2\rho^2} \frac{c^2 + 3cc_l + c_l^2}{c^3c_l^3(c+c_l)^3} \lambda^4 ,\\
    \frac{d\lambda}{d\ell}
        &= \left(\frac{5z - d - 2}{2}
        + \frac{1}{2\pi^2\rho} \frac{3 c + c_l}{c^3 (c+c_l)^3} \lambda^2
        - \frac{N+2}{48\pi^2} \frac{u}{c^3} \right) \lambda \;.
\end{aligned}$$
The elastic coupling $\lambda$ explicitly breaks Lorentz invariance causing the flow of $c$.
This can be also interpreted as a correction to the dynamical exponent $z$.
If we demand that $z$ always takes the value that makes $dc^2/d\ell$ vanish,
we can derive an RG flow equation for $z$.
The bare value would of cause be unity, but with a correction of order $\lambda^2$.
This would be equivalent to changing the RG condition to preserving the $c^2q^2$ term in the action.
We will however stick to our original RG condition.

Analogous to [@sec:rg-anisotropic] the above equations can be simplified by introducing dimensionless parameters
$$\begin{aligned}
    \vr &= \frac{c_l}{c},\qquad 
    \tilde{u} = \frac{1}{2\pi^2} \frac{u}{4!\, c^3},\qquad 
    \tilde{\lambda}^2 = \frac{\lambda^2}{4\pi^2\rho c_l^2 c^3},
\end{aligned}$$
In this representation the RG flow is governed by only three equations
$$\begin{aligned}
    \frac{d\vr}{d\ell}
		&= \frac12 \left(\frac{16}{3} \frac{\vr^2}{(1+\vr)^3} - N\vr \right) \tilde{\lambda}^2, \\
    \frac{d\tilde{u}}{d\ell}
		&= \epsilon \tilde{u}
        - (N+8) \tilde{u}^2
        + \frac{12 \vr(2 + \vr)}{(1+\vr)^2} \tilde{u} \tilde{\lambda}^2 
        - \frac{4\vr(1 + 3\vr + \vr^2)}{(1+\vr)^3} \tilde{\lambda}^4, \\
    \frac{d\tilde{\lambda}^2}{d\ell}
		&= \epsilon \tilde{\lambda}^2
        - 2(N+2) \tilde{u} \tilde{\lambda}^2
		+ \left(N + 4 - \frac{4}{(1+\vr)^2}\right) \tilde{\lambda}^4,
\end{aligned}$$ {#eq:rg-flow-dimensionless}
where we have set $d = 3-\epsilon$.
Remarkably, the so far arbitrary dynamical exponent $z$ drops out of the equations with the above choice of variables.
This is remarkable, because this means that these equations are independent
of the above mentioned change in the RG condition.
In a sense that makes the dimensionless representation the most natural representation of the RG flow.

The RG flow equations for $Z_\phi$ and the tuning parameter $r$ in terms of theses dimensionless variables are
$$\begin{aligned}
	\frac{d \log Z_\phi}{d \ell} & =- \frac{4\vr}{(1 + \vr)^3} \tilde{\lambda}^2 \\
	\frac{d\log r}{d\ell} &= 2z
        - (N+2) \tilde{u}
        + \frac{2\vr^2(3 + \vr)}{(1+\vr)^3} \tilde{\lambda}^2 \,. \\
\end{aligned}$$


### Fixed points

We continue with the determination of the fixed points.

For a vanishing elastic coupling $\lambda = 0$,
the equations reduce to the RG equations of the $\phi^4$-theory already known from [@sec:example-phi4]
with an unstable Gaussian (G) and a stable Wilson-Fisher (WF) fixed point.
We may then ask, if the $\tilde{\lambda}^2 = 0$ line is stable or unstable
with respect to an infinitesimal elastic coupling $\tilde{\lambda}$.
This can be checked by calculating its scaling dimension around $\tilde{\lambda}^2 = 0$ from [@eq:rg-flow-dimensionless],
$$
	\frac{d\log \tilde{\lambda}^2}{d\ell}\Big|_{\tilde{\lambda} = 0}
		= \frac{1}{\tilde{\lambda}^2} \frac{d\tilde{\lambda}^2}{d\ell}\Big|_{\tilde{\lambda} = 0}
		= \epsilon - 2(N+2) \tilde{u}.
$$
The elastic coupling is marginally relevant (positive scaling dimension of order $\epsilon$)
with respect to both G and WF, if the right-hand side is larger than zero.
This coincides with the criterion given in [@eq:quantum-criterion], $\alpha_q > 0$.
At $\tilde{\lambda}^2 = 0$ the correlation length exponent reads
$$
	1/\nu = \frac{d\log r}{d\ell}|_{\tilde{\lambda} = 0}  = 2 -\frac{N+2}{2\pi^2} \tilde{u}
$$
and $z=1$. So we get up to corrections of order
$\mathcal{O}(\tilde{u}^2, \tilde{u} \epsilon)$, $$\begin{aligned}
\alpha_q|_{\tilde{\lambda}^2 = 0} = \frac{\epsilon}{2} - \frac{N+2}{2\pi^2} \tilde{u}.
\end{aligned}$$ At the Gaussian fixed point, $\tilde{u} = 0$, and the elastic
coupling is relevant for $\epsilon > 0$. At the WF fixed point
$\tilde u = \frac{2\pi^2}{N+8} \epsilon$ and the elastic coupling is
relevant for $N < 4$.

For finite elastic coupling $\tilde{\lambda} \neq 0$ we fist consider the flow of the velocity ratio $\vr$.
Physically, its bare value $\vr_0$ should always be positive.
Then for $N \geq 1$ the RG flow of $\vr$ [@eq:rg-flow-dimensionless] is always negative, only stopping for $\vr = 0$.
It follows that $c_l$ decreases faster than $c$ under RG.

Interestingly, if we allow for a moment for $N$ to be something else than a positive integer
[@eq:rg-flow-dimensionless] has a fixed point at a finite $\vr$ if $N \leq \frac{64}{81} \approx 0.79$.
This is significant in that the absence of a fixed point where both $\vr$ and $\lambda$ remain finite
seems to be in some sense incidental.
There is no obvious reason why in a theory that has analogs to the velocities $c$ and $c_l$,
but with slightly different combinatorics or differently formed integrals,
there should not be an number of non-trivial fixed points.
However, in the case of the $O(N)$ model<!-- as well as in the theory considered in [@sec:ferroelectric-qc],
which includes long range dipolar interactions,--> only the trivial fixed point exists.

We conclude that a true fixed point at finite elastic coupling can only exist in the limit $\vr \to 0$.
Formally, the RG flow equations have two additional solutions
with a finite value for the dimensionless elastic coupling $\tilde{\lambda}^2$,
corresponding to fixed points G${}^*$ and WF${}^*$, see [@tbl:qccl-fixed-points].
However, only one of them is positioned at a real value of $\tilde{\lambda}$,
which one depends on the sign of $\epsilon$.
Above the critical dimension ($\epsilon < 0$) there is a modified Gaussian fixed point G${}^*$ on the $\tilde{u} = 0$ axis.
Below the critical dimension ($\epsilon > 0$) there is a modified Wilson-Fisher fixed point WF${}^*$,
with finite values for $\tilde{u}$ and $\tilde{\lambda}^2$.

The new fixed points can be characterized by a renormalized dynamical exponent for the phonon degrees of freedom,
$z_u \neq 1$, by demanding that the scaling dimension of the longitudinal phonon velocity vanishes.

  Fixed point                        $\vr$                     $\tilde{u}$               $\tilde{\lambda}^2$                           $z_u$                   $\frac{d\log\tilde{\lambda}^2}{d\ell}|_{\tilde{\lambda} = 0}$
  --------------------------- ------------------- ------------------------------- ---------------------------------------- ---------------------------------- ----------------------------------------------------------------
  G                            $\mathbb{R}_{>0}$                 0                                   0                                     1                             $\epsilon$
  WF $(\epsilon >0)$           $\mathbb{R}_{>0}$   $\frac{\epsilon}{N+8}$                            0                                    $1$                    $\frac{4-N}{N+8}\epsilon$
  G$^* (\epsilon < 0)$                 0                         0                     $-\frac{\epsilon}{N}$                   $1 - \frac{1}{2}\epsilon$                         -
  WF$^* (\epsilon >0, N>4)$            0           $\frac{\epsilon}{N+8}$          ${\frac{N-4}{N(N+8)}}\epsilon$           $1 - \frac{4-N}{2(N+8)}\epsilon$                     -

  Table: Fixed points of the $O(N)$ model coupled to an isotropic elastic medium, with values of the dimensionless couplings,
  the renormalized dynamical exponent of the phonons $z_u$ and the scaling dimension of the dimensionless elastic coupling $\tilde{\lambda}$. {#tbl:qccl-fixed-points}

TODO: calculate scaling dimension $\frac{d\log |\tilde{\lambda}|}{d\ell}|_{\tilde{\lambda} = \tilde{\lambda}^*}$ at G* and WF*

TODO: diagonalization around each fixed point

The direction of the RG flow within the $(\tilde{u}, \tilde{\lambda}^2)$ plane is determined by the matrix
$$
	\frac{d}{d\ell} \begin{pmatrix}
		\delta\tilde{u} \\
		\delta\tilde{\lambda}^2
	\end{pmatrix}_{\vr \to 0} = \begin{pmatrix}
		\epsilon - 2(N+8) \tilde{u}^* & 0  \\
	    -2(N+2) \tilde{\lambda}^*{}^2 & \epsilon + 2(N \tilde{\lambda}^2 - (N+2) \tilde{u}^*)
	\end{pmatrix} \begin{pmatrix}
		\delta\tilde{u} \\
		\delta\tilde{\lambda}^2
	\end{pmatrix}.
$$

![RG flow for $d=3.01$ dimensions ($\epsilon = -0.01$) where the
self-interaction $\tilde{u}$ is irrelevant. The flow is shown for $\vr \ll 1$,
and it is representative for all values $N \geq 1$. The fixed point G is
stable with respect to $\tilde{\lambda}^2$ whereas G$^*$ is unstable along the
$\tilde{\lambda}^2$ axis. Even though G is stable, the flow is towards strong
coupling if the elastic coupling $\tilde{\lambda}^2$ exceeds a threshold value.
](figures/qccl_figures/rg_flow_above_crit_dim.pdf){#fig:rg_flow_N_below_4_3p01 width=80%}

TODO: describe flow in diagrams, $\epsilon < 0$, $\epsilon < 0$, $N < 4$, $N > 4$

![RG flow for $d = 2.99$ dimensions $(\epsilon = 0.01)$, $\vr \ll 1$ and
$1\leq N < 4$. Both, the G and WF fixed point are unstable with respect
to $\tilde{\lambda}^2$ resulting in runaway
flow.](figures/qccl_figures/rg_flow_below_crit_dim_pos_alpha.pdf){#fig:rg_flow_N_below_4 width=80%}

![RG flow for $d = 2.99$ dimensions $(\epsilon = 0.01)$, $\vr \ll 1$ and
$N > 4$. The G and WF fixed point is unstable and stable. respectively.
The additional fixed point WF$^*$ is repulsive with respect to $\tilde{\lambda}^2$
leading to runaway flow for sufficiently large elastic
coupling.](figures/qccl_figures/rg_flow_below_crit_dim_neg_alpha.pdf){#fig:rg_flow_N_above_4 width=80%}

In the following, we will further elaborate on the RG flow for $N>4$ where both fixed points, WF and WF$^*$, are present.
In order to elucidate the physics of the runaway flow,
we present in [@fig:numerical_example] the flow of the dimensionful variables $c_l$ and $\lambda$
as well as the dimensionless coupling $\tilde{\lambda}^2$ for starting values below and above the separatrix.
The flow of the longitudinal phonon velocity $c_l$ is always towards smaller values.
For starting values above the separatrix, the flow of $\tilde{\lambda}^2$ is towards strong coupling
and the phonon velocity $c_l$ vanishes at some finite RG scale $\ell$,see panel (b).
For starting values within the basin of attraction of the WF fixed point, see panel (c),
the dimensionless elastic coupling $\tilde{\lambda}^2$ flows sufficiently fast to zero
such that the flow for $c_l$ stops and the phonon velocity remains finite at lowest energies.

![RG flow for $d=2.99$ and $N=5$ of the phonon velocity $c_l$, the
elastic coupling $\lambda$, and the dimensionless elastic coupling
$\tilde{\lambda}^2$ using starting values $c_0=1$, $c_{0,l}=0.1$ and $u_0 = 0.5$.
The starting value for panel (b) $\lambda_0 = 0.015$ is located above
the separatrix as indicated in (a), leading to runaway flow: the phonon
velocity vanishes at some finite RG scale $\ell$. The starting value for
panel (c) $\lambda_0 = 0.009$ is located below the separatrix, and the
flow is towards the WF fixed point. Here, the velocity $c_l$ saturates
at a finite value.](figures/qccl_figures/rg_numerics_instability.pdf){#fig:numerical_example height=40%}

The influence of the WF$^*$ fixed point materializes if the initial values are located on or very close to the separatrix.
In this case, the phonon velocity eventually decreases as $c_l(\ell) \sim e^{-\delta z\,  \ell}$ that can be interpreted as a
correction to the dynamical exponent of the phonons $z_u = 1 + \delta z$, see [@tbl:qccl-fixed-points],
where $\delta z = \frac12 \frac{N-4}{N+8} \epsilon$.
This is illustrated in [@fig:regimes].

![RG flow for $d=2.99$ and $N=5$ illustrating the influence of the
WF$^*$ fixed point. Starting values just below the separatrix are
chosen, $c_0=1$, $c_{l,0}=0.3$, $u_0 = 0.1$ and
$\lambda_0 \approx0.0163$. The resulting flow within the $(\tilde{u}, \tilde{\lambda}^2)$
plane is shown in panel (a). Panels (c)-(f) display the phonon velocity
$c_l$, the dimensionful interactions $u$ and $\lambda$, as well as the
dimensionless elastic coupling $\tilde{\lambda}^2$ as a function of the RG scale
$\ell$, respectively. The shaded region indicate the range of $\ell$
governed by a specific fixed point whereas the white regions correspond
to crossovers. Initially, the flow is still influenced by the Gaussian
fixed point G. There is an extended range of $\ell$ where the flow is
dominated by WF$^*$ leading to a power-law dependence of the phonon
velocity with the expected exponent
$\delta z = \frac12 \frac{N-4}{N+8} \epsilon \approx 3.846 \times10^{-4}$,
as shown in more detail in panel (b). Eventually, the flow is governed
by the WF fixed point when $\tilde{\lambda}^2$ drops quickly to zero.
](figures/qccl_figures/rg_numerics_three_fixed_points.pdf){#fig:regimes height=60%}

### RG flow at the upper critical dimension

![RG flow at the upper critical dimension $d = 3$ for $\vr \ll 1$ and
$N>4$. There exists a separatrix below which the flow is towards the
Gaussian fixed point. Above the separatrix the flow is towards strong
coupling.](figures/qccl_figures/rg_flow_at_crit_dim.pdf){#fig:rg_flow_N_above_4_at_critical_dimension width=80%}

![RG flow for $d=3$ and $N=5$ of the phonon velocity $c_l$, the elastic
coupling $\lambda$, and the dimensionless elastic coupling $\tilde{\lambda}^2$ using
starting values $c_0=1$, $c_{l,0}=0.1$, $u_0 = 0.1$. Panel (a)
illustrates the choice for the starting value of $\lambda$. For panel
(c)-(d) the starting values $\lambda_0 = 0.006$,
$\lambda_0 \approx 0.0045$, and $\lambda_0 = 0.003$ are chosen that are
located above, on, and below the separatrix, respectively. Above the
separatrix (c) the phonon velocity vanishes at a finite RG scale. On the
separatrix (d) the phonon velocity $c_l \sim \ell^{-\#}$ vanishes
logarithmically with exponent $\# = \frac{N-4}{2(N+8)} = 0.03846..$, as
illustrated in panel (b). Below the separatrix (e) $c_l$ saturates in
the large $\ell$ limit.](figures/qccl_figures/rg_numerics_at_crit_dim.pdf){#fig:rg_numerics_at_crit_dim height=60%}

At the upper critical dimension $d=3$ both couplings $\tilde{u}$ and $\tilde{\lambda}^2$ are marginal
and the couplings flows only logarithmically.
In general, the solution of the three coupled equations [@eq:rg-flow-dimensionless] is only numerically feasible,
but it simplifies in the limit of small $\vr$.
The flow within the $(\tilde{u}, \tilde{\lambda}^2)$ plane is then asymptotically decribed by
$$\begin{aligned}
    \frac{d\tilde{u}}{d\ell} &\approx -\frac{(N+8) }{2\pi^2} \tilde{u}^2, \\
    \frac{d\tilde{\lambda}^2}{d\ell} &\approx -\frac{(N+2)}{\pi^2} \tilde{u}\tilde{\lambda}^2 + \frac{N}{2\pi^2} \tilde{\lambda}^4.
\end{aligned}$$
The first equation is independent of $\tilde{\lambda}^2$ and possesses the solution
$$
	\tilde{u}(\ell) = \frac{\tilde{u}_0}{(N+8)\tilde{u}_0 \ell + 1} ,
$$
where $\tilde{u}_0$ is the bare value, $\tilde{u}(0) = \tilde{u}_0$.
This implies that $\tilde{u}$ is always marginally irrelevant with the asymptotic behavior $\tilde{u} \sim \ell^{-1}$.

On the other hand, the Gaussian fixed point is always unstable with respect to the elastic coupling.
Solving the second equation at $\tilde{u} = 0$ one finds
$$
	\tilde{\lambda}^2(\ell) = \frac{\tilde{\lambda}_0^2}{1 - N \tilde{\lambda}_0^2 \ell} \;,
$$
which has a pole at the RG scale $\ell = (\tilde{\lambda}^2_0 N)^{-1}$,
where again the bare value is $\tilde{\lambda}^2(0) = \tilde{\lambda}^2_0$.
The dimensionless elastic coupling thus diverges at a finite RG scale $\ell$,
indicating that the phonon velocity $c_l$ vanishes at a finite scale $\ell$.

The behaviour within the $(\tilde{u}, \tilde{\lambda}^2)$ plane away from the two axes
depends on the number of components $N$ reminiscent of the behaviour for finite $\epsilon$.
The general solution reads
$$
    \tilde{\lambda}^2(\ell) = \begin{dcases}
        \frac{\frac{N-4}{N} \tilde{u}_0}{
            \left(\frac{N-4}{N} \frac{\tilde{u}_0}{\tilde{\lambda}_0^2} - 1\right)
            \tilde{\ell}^{\frac{2 (N+2)}{N+8}}
            + \tilde{\ell}
        } & \text{for}~N \neq 4 \\
        \frac{3 \tilde{u}_0 \tilde{\lambda}_0^2}{\tilde{\ell} \left(3 \tilde{u}_0-\tilde{\lambda}_0^2
           \log \tilde{\ell}\right)} & \text{for}~N = 4 .
    \end{dcases}
$$
where
$$
	\tilde{\ell} = (N+8) \tilde{u}_0 \ell + 1 \;.
$$
For $1 \leq N \leq 4$, we find that the elastic coupling $\tilde{\lambda}^2$
diverges at a finite RG scale implying that the axis
$\tilde{\lambda}^2 = 0$ is unstable with respect to a small elastic coupling. For
$N>4$ the flow is only towards strong coupling provided that the initial
value of the elastic coupling exceeds the threshold value,
$$
	\tilde{\lambda}_0^2 > \frac{N-4}{N} \tilde{u}_0 \;,
$$
which also defines the separatrix separating the flow to strong and to weak coupling.
This is illustrated in [@fig:rg_flow_N_above_4_at_critical_dimension;@fig:rg_numerics_at_crit_dim].

Note hat at finite $\vr$ the slope of the separatrix will be modified.
When solving the full set of equations numerically,
one finds that the threshold value of $\tilde{\lambda}^2$ becomes higher the higher the initial values of $\vr$.
The separatrix is then a curved sheet in the parameter space spanned by $(\vr, \tilde{u}, \tilde{\lambda}^2)$.

TODO: figure of separatrix sheet?

## Microscopic and macroscopic instability of the crystal lattice {#sec:CrystalStability}

So far we have discussed the RG flow of the microscopic fluctuations
of both order parameter field $\vphi$ and the displacement field $\u$.
However, we still need to connect this to the macroscopic, observable, critical behaviour of the system.

From the previous section we know that,
depending on the flavor number $N$ and the initial values of the parameters,
the flow can be either towards strong coupling $\tilde{\lambda}^2 \to \infty$,
towards weak coupling $\tilde{\lambda}^2 \to 0$ or, as an edge case,
if the coupling is exactly on the separatrix shown in [@fig:rg_flow_below_crit_dim_pos_alpha],
towards a finite fixed point value of $\tilde{\lambda}^2$.

If $\alpha \leq 0$ and $\tilde{\lambda}_0^2 \neq 0$
or $\alpha > 0$ the initial value $\tilde{\lambda}_0^2$ is sufficiently large,
the dimensionless elastic coupling $\tilde{\lambda}^2$ diverges.
This does not (necessarily) mean that the dimensionful elastic coupling $\lambda$ becomes large,
but rather that the longitudinal sound velocity $c_l$ becomes small,
as can be seen by example in [@fig:numerical_example] panel (b).
In other words, in an absolute sense the interaction between lattice degrees of freedom does not become stronger,
but the lattice becomes softer, and therefore responds more strongly to the forces exerted on it.
At a finite RG scale $c_l$ vanishes, indicating a microscopic instability.
The bulk crystal becomes unstable against longitudinal fluctuations,
which results in an isostructural transition.
Such a transition is generically first order,
associated with a change in the lattice constant,
and consequently with a jump in volume.
<!--TODO: citation? on isostructural transitions-->

If $\alpha > 0$, there is a region in parameter space, from where the flow is to weak coupling $\tilde{\lambda}^2 \to 0$.
In this case sound velocity is still renormalized, but tends to a finite value,
see [@fig:numerical_example] panel (c).
There is no microscopic instability since all phonon velocities remain finite.

In the edge case of flow towards the fixed points G$^*$ or WF$^*$, see [@fig:regimes],
the value that $c_l$ tends to is $0$.
However, at any finite RG scale the value bulk system remains asymptotically stable.
The situation is then analogous to classical theory by [@BergmanHalperin1976] [@sec:rg-anisotropic] in the $\alpha > 0$ case,
in that the dimensionless coupling approaches a finite fixed point value,
while the phonon velocity asymptotically vanishes.

The microscopic stability, is however not the same as macroscopic stability.
Even though in a truly infinite system all modes are stable at finite sound velocity,
in reality crystal are finite and have collective modes at a scale of the crystal size
that are not captured by the usual phonon picture.
The exact nature of these modes depends on geometry and other non-universal factors,
but in general one finds that some of these modes become unstable when one of the moduli vanishes.
<!--TODO: citation needed-->
Therefore the macroscopic stability of the crystal is not determined by the sound velocities,
but by the elastic moduli, in the isotropic case only $K$ and $\mu$.
They are connected to the microscopic velocities via [@eq:isotropic-sound-velocities].
Since the transversal phonons do not couple to the order parameter field,
the transversal sound velocity is not renormalized,
neither is the shear modulus.
Solving for $K$ yields
$$\begin{aligned}
	K(\ell) = \rho \left(c_l^2(\ell) - \frac43 c_t^2 \right).
\end{aligned}$$
The condition for macroscopic stability is that $K$ must be positive,
but $K$ vanishes once $c_l^2$ reaches the threshold value $\frac{4\mu}{3\rho}$.

The consequence of a vanishing bulk modulus depends on boundary conditions.
In the case of free boundary conditions, where the surface elements are allowed to adjust to applied forces,
the crystal becomes macroscopically unstable.
This would, analogous to the microscopic instability, trigger a first order isostructural transition.

* because the Landau potential for the bulk strain possesses a cubic term.

On the other extreme we may apply pinned boundary conditions,
holding every surface element at a fixed position,
such as suggested in [@BergmanHalperin1976] for the classical equivalent.
The pinned boundary stabilizes the crystal against the macroscopic instability.
In this case we expect the critical behaviour to play out as described above for the microscopic degrees of freedom,
but contrary to the classical phase transition, where one would expect Fisher-renormalized exponent,
as the system approaches criticality and the sound velocity asymptotically vanishes, if $\alpha > 0$,
in the quantum phase transition we expect a microscopic instability occur before the critical point is reached,
even if the system is perfectly isotropic and macroscopically stabilized.
In the case $\alpha < 0$, however, avoiding the macroscopic instability
would allow the observation of a renormalized dynamical exponent for the phonons,
if the initial values of the couplings happen to be close to the separatrix that flows towards G$^*$ or WF$^*$.
The sound velocity has in this case a power law dependence on the corelation length
$$
	c_l \sim e^{-\delta z\,\ell} \sim \xi^{-\delta z}
$$ {#eq:phonon-velocity-power-law}
where $z_u = 1 + \delta z$ is listed in [@tbl:qccl-fixed-points].

One might ask now, if pinning every single point on the surface is truly necessary,
to avoid the $K = 0$ instability, since this is more of a thought experiment than an actual one.
The answer to this question turns out to be complicated enough to declare it out of scope of this work.
TODO: think about this paragraph


At a macroscopic instability where the bulk
modulus $K$ vanishes and eventually turns negative both phonon velocities remain finite.
As a result, the isostructural transition of a crystal is a genuine mean field transition
without critical phonon fluctuations[@Cowley1976].
TODO: we know now that this is more complicated


The above analysis is of course for the somewhat fictitious case,
where we step an $\epsilon$ away from the critical dimension.
The case $\epsilon = 0$ is however quite similar, except that here the corrections are logarithmic.
For $N < 4$ the flow is towards strong coupling with logarithmically diverging $\tilde{\lambda}^2$.

For $N > 4$ it again depends on the initial conditions
and the flow is only towards strong coupling for sufficiently large values of $\tilde{\lambda}^2$.
On the separatrix in [@fig:rg_flow_N_above_4_at_critical_dimension] the elastic coupling now logarithmically vanishes according to
$$
	\tilde{\lambda}^2(\ell) \approx \frac{2\pi^2(N-4)}{N(N+8)} \frac{1}{\ell}.
$$
Together with the RG equation for the phonon velocity [@eq:rg-flow-dimensionless] as $\epsilon = 0$
we conclude that the sound velocity also vanishes logarithmically
$$
	c_l \sim \left[\log \frac{\xi}{\xi_0}\right]^{- \frac{N-4}{2(N+8)}}
$$
as a function of $\xi = \xi_0 e^\ell$, provided $N > 4$.

![Diagram with a closed loop of the $\vphi$ propagator (solid line)
that generates an internal hydrostatic
pressure.](figures/qccl_figures/diagrams_ballon.pdf){#fig:ballon width=10%}

The $\vphi$ degrees of freedom also generate an internal hydrostatic pressure on the bulk strain.
This pressure is proportional to the closed loop of the $\vphi$ propagator, see [@fig:ballon].
The response of the system also depends on the boundary conditions.
For pinned boundary conditions, this internal pressure is compensated
by the external forces imposing the boundary conditions.

TODO: understand expansivity and role of ballon term

For free boundary conditions at constant $P$,
the macroscopic bulk strain $E$ will respond to the internal pressure.
In this manner, the critical $\vphi$ degrees of freedom cause a non-analytic dependence
of the expansivity $E(r)$ on the tuning parameter $r$.
This results in a bulk thermal expansion $\partial_r E$ with characteristic quantum critical signatures [@Zhu2003; @Garst2005].
However, if the bulk modulus becomes small and the system is close to the isostructural instability,
Hooke's law will break down and the elastic response will be non-linear.
