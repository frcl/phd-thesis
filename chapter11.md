\part{Criticality in Elastic Media}


# Introduction to the theory of critical phenomena {#sec:intro-critical-phenomena}

In most situations in our every day lives,
regular thermodynamics is sufficient to explain what is happening around us.
This is because a fundamental assumption of thermodynamics is satisfied:
Fluctuations do not matter in the thermodynamic limit.
However, in the vicinity of a continuous phase transitions this is different.
Here, fluctuations do change the macroscopic behavior of the system.
These fluctuations become correlated over longer and longer length scales,
until at the critical point the correlation length diverges
and beyond this point the system acquires long range order.

As the system approaches the critical point from the disordered phase,
correlated regions are forming and getting larger.
At the scale of the correlation length the system behaves according to renormalized system parameters,
as for example a region of correlated Ising spins will behave quantitatively different than a single Ising spin.
The macroscopic details of the system and its interactions get lost at these large length scales.
This is the cause for the phenomenon of universality:
Approaching a critical point, the system asymptotically shows behavior
that is only determined by symmetry and dimension.
These two factors determine the *universality class* of a continuous phase transition.
As an example the Ising transition at zero field turns out to be in the same universality class
as the critical endpoint of a liquid-vapor transition.

The purpose of this chapter is to give a brief introduction to a theoretical approach
for describing critical phenomena,
focusing on concepts that are needed in later chapters.
The physics of classical phase transitions
in the Landau theory and RG picture will be introduced in [@sec:field-landau-theory;@sec:rg-idea],
as it important context for the discussion in chapter [-@sec:classical-criticality-review].
For the treatment of quantum phase transitions,
the tool of imaginary time formalism is introduced in [@sec:imaginary-time].
These tools are then applied to a basic $\phi^4$ theory in [@sec:example-phi4]
as a baseline for comparison with later results when we extend the theory with elastic coupling
and a $\phi^4$ theory with cubic anisotropy in [@sec:cubic-fixed-point],
which will become relevant when discussing dipolar interaction.

For a more in-depth discussion of the physics of quantum phase transitions
with historical and experimental perspectives see [@Sachdev_2011].
For details on the mathematical subtleties of the renormalization group method I recommend [@Zinn_Justin_2021].


## Field theory and Landau theory {#sec:field-landau-theory}

In statistical mechanics the thermal expectation value of an observable is given by the sum over all states weighted with the probability of the particular state being realized.
In field theory, where states are described by a function of time and space, the field $\phi$,
the sum over states becomes a path integral
$$
    \langle O \rangle = \frac{1}{\mathcal{Z}} \int \mathcal{D}\phi\; O(\phi)\; e^{-\mathcal{S}[\phi]} \;,
$$ {#eq:observable-path-integral}
where $O(\phi)$ is the value of the observable in the particular state $\phi$ and $\mathcal{Z}$,
the normalization factor, is called the *partition function*.
It is given by
$$
    \mathcal{Z} = \int \mathcal{D}\phi\; e^{-\mathcal{S}[\phi]} \;.
$$ {#eq:partition-function}
The symbol $\mathcal{D}\phi$ represents the integration over all possible field configurations.
It can be understood as the limit of infinitely many integrals over independent variables,
representing the field values at discrete points in space.
<!--
   -The mathematical rigor of this limit is at the time of writing still a subject of active research.
   -We not discuss these subtleties here and instead simply profit from its usefulness.
   -TODO: rewrite these two sentences
   -->
The expectation values of operators that are products of field can be written as derivatives
of $\mathcal{F} = - \log\mathcal{Z}$, where $\mathcal{F}$ is called the *free energy*.

The action is assumed to be an analytical function(al)
that can be expanded in powers of $\phi$ and its derivatives.
In a conventional naming of the parameters we write
$$
    \mathcal{S}[\phi] = \int d^dx \left(\mathrm{const.} + h\phi + \frac{r}{2} \phi^2 + \frac{c^2}{2} (\nabla\phi)^2 + \dots\right) \;.
$$ {#eq:general-action-expansion}
The constant in $0^\text{th}$ order only leads to a constant factor
in both enumerator and denominator of [@eq:observable-path-integral] that cancels
and therefore doesn't change observable outcomes.
The $1^\text{st}$ order describes an external field $h$,
which can be used as an experimental knob to probe the system.
It will cause a finite value of $\langle \phi \rangle$, even in the disordered phase.
An important quantity describing this response of the system to an applied field is the susceptibility.
It is given by
$$
    %\chi(x - y) = \partial_{h(y)} \langle\phi(x)\rangle = \langle \phi(x) \phi(y) \rangle \;,
    \chi = \partial_h \langle\phi\rangle = \langle \phi^2 \rangle - \langle \phi \rangle^2 \;.
$$
In the disordered phase at $h = 0$ we have $\langle \phi \rangle = 0$
and the susceptibility is simply the two point correlation function $\langle \phi(x) \phi(y) \rangle$.

Another important experimental knob is the tuning parameter $r$,
which gives in a sense the "distance" to the critical point.
Only at positive $r$ is the expansion of $\mathcal{S}$ around a stable minimum.
If $r$ becomes negative an instability may develop,
spontaneously breaking a symmetry.
For classical phase transitions we can assume $r \propto T-T_c$.
The thermodynamic quantity connected to $r$ is the generalized specific heat $\C$, which goes like
$$
    \C = - \partial_r^2  \mathcal{F}
        = \partial_r^2 \log\mathcal{Z}
        = \langle \phi^4 \rangle - \langle \phi^2 \rangle^2 \;,
$$ {#eq:density-density-correlation}
wich corresponds to the density-density correlation function.
<!--We use the symbol $\C$ to distinguish from the elastic modulus $C$.-->
If we were to take the point of view that fluctuations do not matter,
as will be the case to high enough dimension,
then instead of integrating over all possible states $\phi$,
the system can be sufficiently described to be in one particular state $\psi$,
at which it is at equilibrium
or, in other words, the state which minimizes the free energy.
This state $\psi$ can then be found
via the saddle point approximation of the integral $\mathcal{Z}$
$$
    \mathcal{F} \approx \mathcal{S}[\psi] \;,
$$
(TODO: rename $\psi$ to $\phi_0$)
where $\psi$ is now defined by
$$
    \frac{\delta \mathcal{F}}{\delta \psi} = 0 \;.
$$ {#eq:landau-minimization}
From a phenomenological point of view we might have just stared by expanding $\mathcal{F}$ in $\psi$ and arrived at the same expression
$$
    \mathcal{F} = \int d^dx \left( \frac12 (\nabla\psi)^2 + \frac12 r \psi^2 + u \psi^4 + \dots \right) \;.
$$
Using [@eq:landau-minimization] we can eliminate $\psi$
and are left with a function $\mathcal{F}(r, u, \dots)$ of the parameters,
which can then be used to calculate thermodynamic quantities,
such as $\chi$ and $\C$.


## The renormalization group idea {#sec:rg-idea}

To find out how local system parameters are renormalized at large scales
in the vicinity of the critical point, we employ the tool of the renormalization group.
The name is somewhat misleading, since mathematically there is no group structure here,
at most a semigroup, and,
even though renormalization is an important part of a nontrivial RG analysis,
we can and will introduce the core idea or RG without actually renormalizing anything.

Let us come back to the action [@eq:general-action-expansion].
If the action only has quadratic terms, [@eq:partition-function] is a Gaussian integral
and everything can be calculated explicitly.
However, as soon as interactions are introduced the problem is no longer solvable analytically
and we have to resort to perturbation theory in order to make progress.
As a point of reference we present the Gaussian theory first and relate all subsequent results to that theory,
assuming that higher order terms are small.
$$
\begin{aligned}
    \mathcal{S}_G[\phi] &= \int d^dx\; \frac12 \left(r \phi^2 + c^2 (\nabla\phi)^2\right) \\
        &= \int \frac{d^dk}{(2\pi)^d}\, \frac12 (c^2 k^2 + r) \phi^2
\end{aligned}
$$
An important quantity to consider is the two point correlation function.
In Fourier space it can be directly read of the action
$$
  \langle \phi_k \phi_{-k} \rangle_G = \frac{1}{c^2 k^2 + r} \;.
$$
Transforming back to real space in three dimensions gives the results
$$
  \langle \phi(x) \phi(0) \rangle_G = \frac{1}{4\pi} \frac{e^{-\sqrt{r}x}}{x} \;.
$$
This shows that at finite $r$ the system has an intrinsic length scale,
the correlation length $\xi = r^{-\frac12}$.
It is defined by the long distance behavior of the correlation function
$\langle \phi(x) \phi(0) \rangle \propto e^{-\frac{x}{\xi}}$ as $x\to\infty$.
If $r$ vanishes, the correlation length diverges,
there is no intrinsic length scale anymore.
The correlation function then becomes a power law at large distances.
Specifically $x^{-1}$ in three dimensions,
more generally $x^{2-d}$ in $d$ dimensions.

In general there will be interactions and the correlation function will not be so simple,
but if the interactions are small enough to treat them perturbatively,
it makes sense to describe the system relative to its Gaussian counterpart.

In case of an $O(N)$ symmetry, where a rotation of the vector $\phi$ leaves the action invariant,
all odd orders must vanish, except in the presence of external fields,
which explicitly break the symmetry.
We therefore consider the action
$$
    \mathcal{S}[\phi] = \int d^dx \left(\frac{r}{2} \phi^2 + \frac{c^2}{2} (\nabla\phi)^2 + u \phi^4 + \dots\right)
$$ {#eq:action-general-dots}
where $u$ as well as all higher order coefficients are small.
Usually, a theory like this is derived by taking the scaling limit of a microscopic theory.
The validity of this theory can then only be safely assumed up to some small length scale $a$,
e.g. the lattice constant of a solid,
or equivalently a momentum space cutoff $\Lambda \sim a^{-1}$.

To see how the physics at large scales is renormalized,
we perform a rescaling transformation $x = b x'$.
Exactly at a critical point, where the system becomes self-similar,
this transformation should not change anything.
Away from the critical point the transformation will map to a renormalized theory,
where the function $\phi$ and the parameters $r$, $c$, $u$, $\dots$ are changed appropriately.
For now we assume $\Lambda \to \infty$ and assume the limit of the theory are unchanged by rescaling,
to concentrate on the effect of the scaling itself.

Let $\phi(x) = b^{-s_\phi}\phi(bx)$, where $s_\phi$ is scaling dimension of $\phi$.
We can then restore the original for of the action by defining
$$\begin{aligned}
    \phi(x) &= \phi'(x/b) \\
    r &= b^{2s_\phi-d}r' \\
    c^2 &= b^{2s_\phi-d-2}c'^2 \\
    u &= b^{4s_\phi-3d}u'
\end{aligned}$$
after which we obtain again [@eq:action-general-dots], but with primed symbols.
So the above parameters of the action are, in general, scale dependent.

We must now make a choice.
Since a constant factor in front of the action does not affect any observable outcomes,
and $b$ to any power is a constant in this sense,
we have the freedom to eliminate one of the above scaling relations.
This choice is called the *RG condition* and will effectively set $s_\phi$.
The other relations will then tell us how the parameters scale relative to the one we keep fixed.
A common choice is to fix $c^2$, which implies $s_\phi = (d-2)/2$.
This is consistent with our earlier result for  $\langle \phi(x) \phi(0) \rangle_G$,
which scales with $2s_\phi = d-2$ at long distances.

The other parameters can now be classified according to their scaling dimension.
If the scaling dimension is positive, the parameter is called *relevant*.
If it is negative, the parameter is *irrelevant*.
If it vanishes, the parameter is *marginal*.
For example
$$
    \frac{d\log r}{d\log b} = 2\;,
$$
which means $r$ is relevant in any dimension.
In other cases the scaling dimension may depend on the dimension, such as
$$
    \frac{d\log u}{d\log b} = 4-d\;.
$$
We see that $u$ is relevant in $d < 4$.
In this case the $\phi^4$ term will have a strong influence on the critical behavior.
It is irrelevant for $d > 4$,
in which case $u$ will eventually become negligible at large enough scales
and we end up with a Gaussian theory again.
In the marginal case, $d = 4$, we can not make a statement about the effect the $\phi^4$ term has
on the critical behavior from this superficial argument.
<!--We will be able to say more about this in a later section.-->

In general, when including the effects of the interactions,
the correlation function may scale differently from the Gaussian theory at the critical point.
With perturbative RG the $\phi$ may also have an anomalous scaling dimension, relative to the Gaussian one
$$
    2s_\phi = d - 2 + \eta
$$
where $\eta$ is the *anomalous dimension*.
We can have $\eta \neq 0$ is the $k^2$ term in the above action is renormalized by some interaction,
which we have not included so far.
Therefore, $\eta$ will be given by the flow in $c^2$.
Later in this text however, we will have imaginary time derivatives in the action,
which can also be renormalized.
Then the anomalous dimension is expressed as a so called $Z$-factor, with $Z_\phi = b^\eta$,
and a flow in $c^2$ indicates a change in the anisotropy in space time.
As we have seen, in a Gaussian theory we have $Z_\phi = 1$.
The number $\eta$ is one of the critical exponents.

From the above arguments we can already calculate all the critical exponents for the Gaussian theory.
The anomalous dimension, as described above is
$$
    \eta = \frac{d\log Z_\phi}{d\log b} \qquad \eta_G = 0
$$
The correlation length exponent, defined by $\xi \propto r^{-\nu}$, is
$$
    \nu = \left(\frac{d\log r}{d\log b}\right)^{-1} \qquad \nu_G = \frac12
$$
The specifically heat exponent, defined by $\C \propto r^{-\alpha}$
is connected to the correlation length exponent via the hyperscaling relation
$$
    \alpha = 2 - \nu d \qquad \alpha_G = 2-\frac d2 \;.
$$
Notably, $\alpha_G = 0$ in $d = 4$ dimensions.
Hyperscaling is a consequence of the free energy, $\log\mathcal{Z}$,
being an extensive quantity, while the only relevant length scale is the correlation length $\xi$.
Therefore it must hold $\log\mathcal{Z} \propto \xi^{-d} \propto r^{\nu d}$,
but also $\log\mathcal{Z} \propto r^{2-\alpha}$ via [@eq:density-density-correlation].

The analysis in later chapters will focus on the three exponents $\alpha$, $\nu$ and $\eta$,
but for completeness the remaining canonical exponents are given here.
The susceptibility scales like $\chi \propto r^{-\gamma}$
and previously we established $\chi \propto \xi^{2-\eta} \propto r^{-(2-\eta)\nu}$,
therefore
$$
    \frac\gamma\nu = 2 - \eta \qquad \gamma_G = 1
$$
In the ordered phase the order parameter depends on the tuning parameter
with $\langle \phi \rangle \propto r^\beta$.
From the scaling relation for $\phi$ we know $\phi'(x') = b^{(d-2+\eta)/2} \phi(x)$.
If we evaluate this at $b^* = \xi$ we get
$$
    \beta = (d - 2 + \eta) \nu /2 \qquad \beta_G = \frac{d-2}{2} \;.
$$
In $d = 4$ dimensions, this means $\beta_G = 1$.
<!--TODO: $\delta$-->
<!--
   -On the other hand, considering the effect of an externally applied field at the critical point $r=0$,
   -with a power law of the form $\langle \phi \rangle \propto h^{\frac1\delta}$,
   -we get
   -$$
   -    \frac{\delta}{1+\delta} = \frac1d \frac{d\log h}{d\log b} = \frac{d + 2 - \eta}{2d} \qquad \delta_G = 3
   -    % TODO: this is just wrong so far
   -$$
   -->
<!--
   -TODO: maybe point out pattern\
   -$\partial_h \log\mathcal{Z}|_{h=0} \propto r^\beta$\
   -$\partial_h \log\mathcal{Z}|_{r=0} \propto h^\delta$\
   -$\partial_r \log\mathcal{Z}|_{h=0} \propto \partial_h^2 \log\mathcal{Z}|_{h=0} \propto r^\gamma$\
   -$\partial_r^2 \log\mathcal{Z}|_{h=0} \propto r^\alpha$
   -->
Overall, we can conclude that with all the scaling relations
calculating two exponents from the RG equations is enough in order to fix the complete critical behavior.
For example, calculating $\eta$ from the RG equation for $Z_\phi$ and $\nu$ from the RG equation for $r$


## The momentum shell RG procedure

---

TODO: completely rewrite/merge with previous section

The $Z$-factor contributes to the RG equations via
$$
    Z_\phi \to 1 + \frac{dZ_\phi}{d\ell}\ell + \mathcal{O}(\ell^2)
$$

---

As the system approaches criticality,
the correction length diverges and the system becomes more and more self-similar.
We want to use this fact by mapping the theory onto a scaled version of itself
and analyze the properties of this mapping
in order to find out how the system parameters are renormalized within correlated domains.
There are many ways to produce such mappings,
but a common way is to go to momentum space and integrate out the small wavelength/large momentum modes.
The integrals involved are algebraically divergent in $d < 4$ dimensions
and logarithmically divergent in $d = 4$ dimensions.
The latter case can be remedied by renormalizing and we use an $\epsilon$-expansion
to go to $d = 4 - \epsilon$ dimensions using perturbation theory in $\epsilon$, see [@Wilson_1972].

Here, it becomes important that the momentum integral in $\mathcal{S}$ only goes up to some cutoff $\Lambda$.
In a solid with lattice constant $a$ it is reasonable to assume $\Lambda \sim \frac1a$,
but it should not matter much, since we are only interested in the cutoff independent physics.
We split the fields into modes that are on a momentum space shell between the original and rescaled cutoff $\Lambda/b < \Lambda$,
which we want to integrate out, and the longer wavelength modes, which will remain
$$
    \phi_k = \phi_k^< + \phi_k^> \;,
    % TODO: maybe rewrite with Heavyside function
$$
with the piecewise defined functions
$$
    \phi_k^< = \begin{cases}
        \phi_k & \text{for $0 <k < \Lambda/b$} \\
        0 & \text{for $\Lambda/b < k < \Lambda$}
    \end{cases}
    \qquad \phi_k^> = \begin{cases}
        0 & \text{for $0 <k < \Lambda/b$} \\
        \phi_k & \text{for $\Lambda/b < k < \Lambda$} \;.
    \end{cases}
$$
Inserting this into the action we can rearrange it as
$$
    \mathcal{S}[\phi] = \mathcal{S}_G[\phi^<] + \mathcal{S}_G[\phi^>] + \mathcal{S}_\mathrm{int}[\phi^<, \phi^>] \;,
$$
where $\mathcal{S}_G$ contains all terms up to the Gaussian level.
<!--Since $\phi^< \phi^>$ is zero everywhere, the theory separates up to this level.-->
Our goal is now to rewrite the theory as an action for only $\phi^<$ by averaging over $\phi^>$
$$
\mathcal{Z} = \int \mathcal{D}\phi^<\,\mathcal{D}\phi^>\; e^{-\mathcal{S}_G[\phi^{<}]} e^{-\mathcal{S}_G[\phi^{>}]} e^{-\mathcal{S}_\mathrm{int}[\phi^<, \phi^{>}]}
        \equiv \int \mathcal{D}\phi^<\; e^{-\mathcal{S}'[\phi^{<}]} \;,
$$
where the new action $\mathcal{S}'$ is
$$
    \mathcal{S}'[\phi^<] = \mathcal{S}_G[\phi^<] - \log \left\langle e^{-\mathcal{S}_\mathrm{int}[\phi^<, \phi^>]} \right\rangle_G^> - \log \mathcal{Z}_G\;.
$$ {#eq:reexponentiation-action}
The average here is with respect to the Gaussian theory for $\phi^>$
$$
    \left\langle \dots \right\rangle_G^> = \frac{1}{\mathcal{Z}_G} \int \mathcal{D}\phi^>\; (\dots)e^{\mathcal{S}_G[\phi^{>}]} \;,
$$
In general it is not possible to calculate an integral of this form exactly.
Assuming the interaction is small, we can expand the exponential function
$$
    \left\langle e^{\mathcal{S}_\mathrm{int}[\phi^<, \phi^>]} \right\rangle_G^> = \sum_n \frac{1}{n!}\left\langle \left(\mathcal{S}_\mathrm{int}[\phi^<, \phi^>]\right)^n\right\rangle_G^>
$$
and use Wick's theorem at each order
$$
    \left\langle \phi_{i_1} \dots \phi_{i_n} \right\rangle_G^>
        = \sum_P \left\langle \phi_{P(i_1)} \phi_{P(i_2)} \right\rangle_G^> \dots \left\langle \phi_{P(i_{n-1})} \phi_{P(i_n)} \right\rangle_G^> \;,
$$
where the sum is over all pairings of fields.
Wick's theorem is a consequence of the Gaussian form of the integral
and can be shown by introducing a "force" term $h \cdot \phi$ in the action,
rewriting expectation values as derivatives of the partition function with respect to this force
$$
    \left\langle \phi_{i_1} \dots \phi_{i_n} \right\rangle
        = \partial_{h_{i_1}} \dots \partial_{h_{i_n}} \log\mathcal{Z} \;.
$$
Now using the result of the Gaussian integral
$$
    \mathcal{Z} = \int \mathcal{D}\phi \, e^{-\int \frac12 \phi^T\chi^{-1}\phi + h\cdot \phi} \propto e^{\frac12 h^T\chi h}
$$
we arrive at
$$
    \left\langle \phi_{i_1} \dots \phi_{i_n} \right\rangle_G
        = \sum_P \chi_{P(i_1)P(i_2)}  \dots \chi_{P(i_{n-1})P(i_n)} \;.
$$
It turns out that many of these pairings have give the same contribution.
They can be organized in diagrams with lines representing the different contractions of $\chi$.
<!--TODO: example?-->
The linked cluster theorem states that we only have to include fully connected diagrams.
Furthermore, after renormalization, only one particle irreducible diagrams have to be considered.

Finally, we arrive at the modified action with the renormalized two point correlation function.
The difference between the bare and renormalized correlation function is called the self-energy correction $\Sigma$
$$
    \chi^{-1} = \chi_0^{-1} - \Sigma \;,
$$
And interactions such as the $\phi^4$ term get a vertex correction
$$
    u \delta_{ij} \delta_{kl} = u_0\delta_{ij} \delta_{kl} + \Gamma_{ijkl} \;.
$$
In order to bring the theory back the original phase space we need to scale the momenta
such that $\Lambda/b$ is the new cutoff.
So we write $k' = k/b$ and $\phi'_k = \phi_{k/b}$.
This is the exact scaling transformation that we considered in [@sec:rg-idea].
If the theory is renormalizable, we end up with the same action as before, just with the primed quantities
$$
    S'[\phi'_{k'}] = S[\phi_k] \;.
$$
This leads to the primed parameters, e.g.
$$
    r' = b^{2s_\phi -d} (r - \Sigma(\Lambda)|_{k=0}) \;.
$$
We then find the RG flow equations by considering infinitesimal rescaling $b = 1 + d\ell$
and calculate derivatives with respect to $\ell = \log b$ then give the flow equations.
$$
    \frac{dr}{d\ell}\Big|_{\ell=0} = (2s_\phi - d) r + \frac{d\Sigma}{d\ell}_|{k=0} + \mathcal{O}(d\ell)
$$


## Imaginary time formalism for quantum criticality {#sec:imaginary-time}

In contrast to classical phase transitions, which are usually driven by thermal fluctuations,
quantum phase transitions are abrupt changes in the ground state of a quantum system
in the absence of thermal fluctuations.
It is important to appreciate how different the picture underlying the model
for these quantum phase transitions is from classical statistical mechanics.

Classically, randomness is a consequence of our lack of knowledge of the microscopic details of the system.
The system appears in a random microstate every time it is observed
and given enough time between observations these states will be entirely uncorrelated.
The principle of ergodicity then tells us that every state
that is possible under conservation laws will eventually be realized
and therefore contributes to the partition function and observables calculated from it.
In systems that evolve according to the laws of quantum mechanics
randomness is a more fundamental fact of nature.
The quantum state evolves coherently
and experiences quantum fluctuations in accordance the uncertainty principle.
The quantum state evolves coherently in time up until a characteristic time scale
called the coherence time $\tau_\phi$.
At zero temperature the coherence time may be infinite.
<!--Excitations decay at a characteristic time scale, that is determined by the energy gap.-->
At finite temperature there will additionally be thermal fluctuations,
making the initial quantum state unknown.
<!--
   -TODO: coherence vs. thermalization time
   -
   -In quantum critical regime $\tau_\mathrm{th} \sim \hbar/k_BT$
   -* Quantum critical vs. quantum disordered regime
   -* Characteristic energy/time scale $\tau \propto \xi^{z} \propto r^{-z\nu}$ of quantum dynamics
   -* Characteristic time scale of thermal dynamics: thermalization time $\tau_\mathrm{th}$
   -    * in quantum critical regime $\tau_\mathrm{th} \sim \hbar(k_BT)^{-1}$
   -    * in classical regime $\tau_\mathrm{th} \ll \hbar(k_BT)^{-1}$
   -->
Therefore, we reframe the partition function instead of a sum over all states as a "sum over histories".
Mathematically this is facilitated by the fact that the density matrix,
which can be used to define $\mathcal{Z}$
$$
    \mathcal{Z} = \tr\{e^{-\beta H}\} \;,
$$
is analogous to the time evolution operator
$$
    U(t, t') = e^{-i H (t-t')} \;.
$$
So the partition function of a quantum system is really the trace of the time evolution operator
evaluated at an imaginary time
$$
    \mathcal{Z} = \tr\{ U(-i\beta, 0) \} \;.
$$
In the path integral formulation this becomes
$$
    \mathcal{Z} = \int \mathcal{D}\phi \; e^{-\mathcal{S}} = \int \mathcal{D}\phi \; e^{-\int_0^\beta d\tau L} \;.
$$
Since in this text we only consider the zero temperature limit,
where coherence time is unbounded and inverse temperature diverges,
the $\tau$ integral in the action is on the interval $(0, \infty)$.
The field $\phi(x, \tau)$ is now a function of $\tau$ and we expand also in its derivatives.
Hence, we write
$$
    \mathcal{S} = \int_0^\infty d\tau \int d^d x
        \left( r\phi^2 + (\partial_\tau\phi)^2 + c^2(\nabla\phi)^2 + \dots \right) \;.
$$
Here, we work under the assumption that the time dimension obeys similar symmetry as the spatial ones,
i.e. time-reversal, hence the first nontrivial term is the second derivative.
This does not necessarily need to be true.
Time reversal and inversion symmetry can in principle be broken.
In these cases the leading order can be linear in the derivatives,
if this can be rectified with the other symmetries.
An example would be a ferromagnet, where first order in time derivative is allow.
However, we restrict ourselves to the most generic case, where the above action is valid.

As a consequence of choosing the most generic symmetry,
at the critical point ($r = 0$) the low energy excitations will have a spectrum
described by $\omega = k$.
Generally, at the critical point the is some power law $\omega = k^z$,
where $z$ is called the dynamical exponent.
In our case, we have $z = 1$.
In contrast a ferromagnet would have $z = 2$.
The scaling is then determined by the so called effecive dimension $d_\mathrm{eff} = d + z$.
As a consequence, in the case $z = 1$ the upper critical dimension of the $\phi^4$-theory
is at $d = 3$ spatial dimensions.

Also the scaling relations are modified, e.g.
$$
	\alpha_q = 2 - (d + z) \nu \;.
$$
The velocity $c$ now quantifies the anisotropy between space and time dimension.
In the absence of any Lorentz invariance breaking terms it can be removed
by a scaling of the space or time coordinates.
In later chapters however, we will introduce such Lorentz invariance breaking terms by coupling to elasticity.
A technical consequence, that is worth mentioning here,
is that the anomalous dimension, tracked by $Z_\phi$, will now be given by the renormalization of the imaginary time derivative.


## Example: Lorentz-invariant $\phi^4$-theory {#sec:example-phi4}

In this section we perform the RG procedure for a Lorentz-invariant $O(N)$ symmetric $\phi^4$ theory as baseline example.
The action for a vector field $\vphi$ with $N$ components or "flavors" is given by
$$
    \mathcal{S} = \int d^dr\,d\tau \left(\frac12 \vphi^T(-\partial_\tau^2 - c^2\nabla^2 + r)\vphi + \frac{u}{4!} (\phi^2)^2\right) \;.
$$
For $N = 1$ this model is of the Ising universality class, $N = 3$ corresponds to a Heisenberg model.

The $\phi^4$ interaction is the most generic extension to the Gaussian model.
It is also interesting because this interaction is marginal in $3+1$ dimensions,
a common case for Quantum critical points.

The propagator is an $N\times N$ matrix. Here the bare propagator is already diagonal
$$
    G^{(0)}_{ij}(q, \omega) \equiv \langle \phi_{i,q,\omega}\phi_{j,-q,-\omega} \rangle_0
        = \frac{\delta_{ij}}{\omega^2 + c^2q^2 + r}
$$
For the self-energy correction at one loop order we only have to look at the first order diagram
$$
\begin{aligned}
    \diagramPhiSelfEnergyV \quad&=\quad \diagramPhiSelfEnergyVTadpole \quad+\quad \diagramPhiSelfEnergyVRainbow \\
    \Sigma^{(1L)}_{ij} &= - 2 \frac{u}{4!} \left(2\delta_{ij} \int_{q,\omega} G_{kk} + 4\int_{q,\omega} G_{ij}\right) \\
         &= - \frac{u}{4!}(4N + 8) \int_{q,\omega} \frac{1}{\omega^2 + c^2q^2 + r} \;,
\end{aligned}
$$ {#eq:phi4-self-energy-correction}
again using the shorthand notation from [@eq:q-int-shortcut].
We also defined the integral
$$
    I_n = \int_{q,\omega} \left(\frac{1}{\omega^2 + c^2q^2 + r}\right)^n
$$ {#eq:integral-n-definition}
The integral $I_1$ in $d = 3$ spatial dimensions is
$$
    I_1 = \int_{q,\omega} \frac{1}{\omega^2 + c^2q^2 + r}
        = \int_q \frac12 \frac{1}{\sqrt{c^2q^2 + r}}
        = \frac{1}{(2\pi)^2} \int_0^\Lambda dq \frac{q^2}{\sqrt{c^2q^2 + r}} \;.
$$
Since we are only interested in the physics close to criticality ($r=0$) we can expand $I_1$ in small $r$,
which is up to first order
$$
    I_1 = \frac{1}{8\pi^2 c^3} \left(c^2\Lambda^2 - r\log\Lambda\right) \;.
$$ {#eq:integral-1}
After rescaling, the $\Lambda^2$ term will still be cutoff dependent.
It is a large term that introduces a fast flow
towards a shifted pole of the correction function.
Since we are only interested in the asymptotic, universal features of the flow,
we ignore the $\Lambda^2$ term here and in all further analysis.

Similarly the correction to the vertex function $\Gamma$
has the first non-trivial contribution
$$
\diagramVCorrectionVV{}{}{}{}
    = \diagramVCorrectionVVLoop{}{}{}{}{}{}{}{}
    + \diagramVCorrectionVVPenguin{}{}{}{}{}{}{}{}
    + \diagramVCorrectionVVBox{}{}{}{}{}{}{}{}
$$
$$
\begin{aligned}
    \Gamma^{(2)}_{ijkl} &= \frac12 \left(\frac{u}{4!}\right)^2 \left(2^3\delta_{ij} \delta_{kl} \int_{q,\omega} G_{mn} G_{mn} + 2^5\delta_{ij} \int_{q,\omega} G_{km} G_{ml} + 2^5 \int_{q,\omega} G_{ik} G_{jl}\right) \\
         &= \frac12 \left(\frac{u}{4!}\right)^2 (2^3 N\delta_{ij}\delta_{kl} + 2^5\delta_{ij}\delta_{kl} + 2^5\delta_{ik}\delta_{jl}) \int_{q,\omega} \frac{1}{(\omega^2 + c^2q^2 + r)^2}
\end{aligned}
$$
TODO: address why $\Gamma$ does not depend on external momentum.
The remaining integral can be calculated from the previous integral $I_1$ by noticing
$$
    I_2 = \int_{q,\omega} \frac{1}{(\omega^2 + c^2q^2 + r)^2} = -\frac{\partial I_1}{\partial r} = \frac{1}{8\pi^2 c^3} \log\Lambda \;.
$$ {#eq:integral-2}
Having calculated the corrections to the self-energy and the vertex function,
we can execute step 2 of the RG procedure to write the scaling relations for the parameters of the action
$$
\begin{aligned}
    r' &= b^{2z} \left(r - \frac{u}{6}(N+2) \frac{1}{8\pi^2 c^3} r \log b\right) \\
    u' &= b^{3z-d} \left(u - \frac{u^2}{6}(N+8) \frac{1}{8\pi^2 c^3} \log b\right) \\
    c'^2 &= b^{2z-2} c^2
\end{aligned}
$$
With $z=1$ the equation for $c$ becomes trivial.
This should not be surprising,
since the action was Lorentz-invariant from the start.
Corrections calculated from diagrams that contain only Lorentz-invariant vertices and propagators,
cannot break the invariance.
Therefore, the $k^2$ term must scale as the $\omega^2$ term.

The other two parameters however are modified and taking the derivative with respect to $\ell = \log b$ results in the RG flow equations for $r$ and $\tilde{u} = \frac{u}{2\pi^2 4! c^3}$
$$
\begin{aligned}
    \frac{dr}{d\ell} &= 2 r - (N+2) r \tilde{u} \\
    \frac{d\tilde{u}}{d\ell} &= \epsilon \tilde{u} - (N+8) \tilde{u}^2 \;,
\end{aligned}
$$ {#eq:phi4-rg-equations}
where we set $\epsilon = 3-d$.
<!--TODO: explain $\epsilon$ expansion-->

In analyzing RG equations the first step is to identify fixed point, 
where the $\ell$ derivatives of all parameters vanish,
which, as we have pointed out, correspond to critical points.

Since we neglected the initial flow dominated by the $\Lambda^2$ correction to $r$ and only look at the asymptotic flow that is eventually dominated by the logarithmic divergences,
the equation for $r$ always has the trivial fixed point $r^* = 0$.
The equation for $u$ on the other hand has two fixed points:
The *Gaussian (G) fixed point* ($\tilde{u}^* = 0$) and the *Wilson-Fisher (WF) fixed point* ($\tilde{u}^* = \epsilon/(N+8)$).

We can get a qualitative picture of the RG flow by linearizing the above equations around the fixed points
and investigate their stability.
Around the Gaussian fixed point after inserting $\tilde{u} = \tilde{u}^* + \delta\tilde{u}$ and only keeping linear order in $\delta\tilde{u}$ we have
$$
    \frac{d}{d\ell} \delta\tilde{u} = \epsilon \delta\tilde{u} \;,
$$
with a positive eigenvalue $\epsilon$ indicating an unstable fixed point in the $\tilde{u}$ direction.
Around the WF fixed point on the other hand
$$
    \frac{d}{d\ell} \delta\tilde{u} = (\epsilon - (N+8) \tilde{u}^*) \delta\tilde{u} = -\epsilon \delta\tilde{u} \;,
$$
indicating a stable fixed point.
The flow in $r$ and $\tilde{u}$ around these points is shown in [@fig:rg-flow-wf-r-u].

![RG flow of $u$ and $r$ in $d = 3-\epsilon$ dimensions.](figures/cubic_anisotropy_figures/wilson_fisher_flow_ru_mod_sw.pdf){#fig:rg-flow-wf-r-u width=80%}

There are two distinct domains in the parameter space.
One where $r$ goes to positive infinity, representing the disordered phase,
and where it goes to negative identify, representing the ordered phase.
$u$ on the other hand is always attracted to its WF value.

This is effectively a post-hoc explanation for the phenomenon of universality.
No matter the initial values of $u$,
the system will eventually end up in the same region of the parameter space.

Each fixed point has associated critical exponents,
which can be calculated from the corresponding eigenvalues after linearization.
The anomalous dimension $\eta$ can be calculated from the RG equation for $Z_\phi$,
which we did not bother to write down
because there is no correction to $Z_\phi$ at first order in $\epsilon$ and therefore $\eta = 0$.
This is the case because the only self-energy diagram at one-loop order [@eq:phi4-self-energy-correction]
happens to be momentum and frequency independent.
Starting from order $\epsilon^2$ there will be a finite anomalous dimension.
<!--TODO: maybe cite something here?-->

The correlation length exponent, however, does get a correction of order $\epsilon$:
$$
    \frac{d}{d\ell} \delta r = \frac1\nu \delta r
        \qquad \nu_\mathrm{WF} = \frac12 + \frac14 \frac{N+2}{N+8}\epsilon + \mathcal{O}(\epsilon^2)\;.
$$
We conclude then for the critical exponent $\alpha$
$$
\begin{aligned}
    \alpha_\mathrm{WF} = 2 - (d+z)\nu_\mathrm{WF}
        = 2 - (4-\epsilon)\nu_\mathrm{WF}
        = \frac12 \frac{4-N}{N+8}\epsilon + \mathcal{O}(\epsilon^2)
\end{aligned}
$$
and the other critical exponents can be calculated in first order in $\epsilon$ in a similar fashion.
Note that $\alpha_\mathrm{WF}$ changes sign as a function of $N$ at a critical flavor number $N_c = 4$.

At the upper critical dimension ($\epsilon = 0$) we can solve the RG flow equations analytically.
The equation for $\tilde{u}$ reduces to
$$
    \frac{d\tilde{u}}{d\ell} = - (N+8) \tilde{u}^2 \;,
$$
which is solved by
$$
    \tilde{u}(\ell) = \frac{1}{(N+8) \ell + \tilde{u}_0^{-1}} \;.
$$
which the initial value $\tilde{u}_0 = u(\ell=0)$.
Subsequently, after inserting this result into [@eq:phi4-rg-equations]
and solving the resulting equation for $r$,
it turns out that $r$ has no longer a pure power law dependence on $b$,
but instead acquires a logarithmic correction
$$
    r \propto  b^{2} (\log b)^{-\frac{N+2}{N+8}} \equiv b^{\frac1\nu} (\log b)^{\frac1{\tilde{\nu}}}
$$
So the scaling is no longer captured by a single critical exponent $\nu = \frac12$,
but additionally one needs to specify the logarithmic scaling
with an exponent $\tilde{\nu} = -(N+2)/(N+8)$.
Similar extensions can be defined for the other critical exponents.
<!--TODO: calculate $\tilde\alpha$-->

## Cubic anisotropy and cubic fixed point {#sec:cubic-fixed-point}
As a next logical step one might ask
how important the $O(N)$ symmetry of the interaction is for the above results.
To shed some light on this question we extend the theory with cubic anisotropy.
This will become important later when we discuss the effect of dipolar interactions,
such as are present in ferromagnets and ferroelectrics.
<!--The RG analysis of cubic anisotropy is also discussed in [@Cardy_1996].-->

We now extend the model by including a cubic anisotropy term in the action
$$
    \mathcal{S} = \int d^dr\,d\tau \left(\frac12 \vphi^T(-\partial_\tau^2 - c^2\nabla^2 + r)\vphi + u (\phi^2)^2 + v \sum_i \phi_i^4\right) \;.
$$
Note that here we do not include the conventional factor of $4!$
<!--TODO: maybe remove everywhere-->
for reasons that will become apparent later.
The bare propagator is of course the same as in the isotropic theory
$$
   G^{(0)}_{ij}(q, \omega) = \frac{\delta_{ij}}{\omega^2 + c^2q^2 + r} \;.
$$
<!--TODO: maybe revisit paper that was cited in Chandra 2020-->
The two interactions $u$ and $v$ can be written as a single vertex
$$
    V_{ijkl} = u \delta_{ij}\delta_{kl} + v g_{ijkl} \;,
$$
where $g_{ijlk}$ is $1$ for $i=j=k=l$ and zero otherwise.
The diagrams contributing to self energy and vertex corrections look the same as in the isotropic theory.
However, we must now carefully write down the correct tensor contractions
and identify the all terms with the isotropic and anisotropic part of the vertex function
$$
    \Gamma^{(n)}_{ijkl} = \Gamma^{(n)}_u \delta_{ij}\delta_{kl} + \Gamma^{(n)}_v g_{ijkl}\;.
$$
As before, let us start with the self-energy correction
$$
\begin{aligned}
    \Sigma^{(1)}_{ij} &= 2 (2V_{ijkl} + 4V_{iklj})\int_q G_{kl} \\
        &= 2 \big(2(u\delta_{ij}N + vg_{ijkl}\delta_{kl})
            + 4(u\delta_{ik}\delta_{jk} + vg_{iklj}\delta_{kl}) \big) I_1 \\
        &= (4(N + 2)u + 12v) \delta_{ij} I_1
\end{aligned}
$$
with $I_1$ as in [@eq:integral-1]
and we used
$$
    g_{iklj}\delta_{kl} = g_{ijkl}\delta_{kl} = \delta_{ij} \;.
$$
Later we will also need
$$
    g_{ijkl}g_{klmn} = g_{ijmn} \;.
$$
Calculating the vertex correction needs more attention to detail in this case.
Here we label the diagrams to make sure we get the indices right
$$
\diagramVCorrectionVV{i}{j}{k}{l}
    = \diagramVCorrectionVVLoop{i}{j}{k}{l}{i'}{j'}{k'}{l'}
    + \diagramVCorrectionVVPenguin{i}{j}{k}{l}{i'}{j'}{k'}{l'}
    + \diagramVCorrectionVVBox{i}{j}{k}{l}{i'}{j'}{k'}{l'}
$$
$$
\begin{aligned}
    \Gamma^{(2)}_{ijkl}
        &= \frac12 \left[2^3 V_{iji'k'} V_{j'l'kl} + 2^5 V_{iji'k'}V_{j'kl'l} + 2^5 V_{ii'jk'}V_{j'kl'l}\right]\int_q G_{i'j'}G_{k'l'} \\
        &= \frac12 \big[2^3 (Nu^2\delta_{ij}\delta_{kl} + uv\delta_{ij}\delta_{kl} + uv\delta_{ij}\delta_{kl} + v^2g_{ijkl}) \\
            &\qquad+ 2^5 (u^2\delta_{ij}\delta_{kl} + uvg_{ijkl} + uv\delta_{ij}\delta_{kl} + v^2g_{ijkl}) \\
            &\qquad+ 2^5 (u^2\delta_{ik}\delta_{jl} + uvg_{ikjl} + uvg_{ikjl} + v^2g_{ikjl})\big] I_2 \\
        &= 4\big[((N+8)u^2 + 6uv)\delta_{ij}\delta_{kl} + (12uv + 9v^2)g_{ijkl}\big] I_2 \;,
\end{aligned}
$$
where $I_2$ is the same as in [@eq:integral-2].
Finally, we end up with the RG equations
for $(\tilde{u}, \tilde{v}) = (\frac{1}{2\pi^2 c^3} u, \frac{1}{2\pi^2 c^3} v)$
$$\begin{aligned}
    \frac{dr}{dl} &= 2 r - (N + 2)\tilde{u}r - 3 \tilde{v}r \\
    \frac{d\tilde{u}}{dl} &= \epsilon \tilde{u} - (N+8)\tilde{u}^2 - 6\tilde{u}\tilde{v} \\
    \frac{d\tilde{v}}{dl} &= \epsilon \tilde{v} - 12\tilde{u}\tilde{v} - 9\tilde{v}^2 \;.
\end{aligned}$$ {#eq:rg-flow-cubic-anisotropy}
The above equations reduce to the equations for an isotropic $\phi^4$-theory for $\tilde{v} = 0$
as one would expect.
Since $\tilde{v} = 0$ is also a fixed point of the last equation,
the Gaussian and WF fixed points are still present.
There are, however, two new fixed points:
One at $(\tilde{u}^*, \tilde{v}^*) = (0, \epsilon/9)$,
which we call Ising (I) fixed point,
since it corresponds to a set of $N$ completely decoupled Ising models,
and $(\tilde{u}^*, \tilde{v}^*) = (\frac{\epsilon}{3N}, \frac{\epsilon(N-4)}{9N})$,
which we call the Cubic (C) fixed point.

Analysing the stability of the fixed points is crucial for determining the critical behavior.
Since we have now two coupled equations, after linearization around a fixed point we get a matrix,
describing the flow close to the fixed points.
The eigenvalues determine the stability along the principle directions, given by the eigenvectors.

<!--
   -The Gaussian fixed point is equally unstable in both $\tilde{u}$ and $\tilde{v}$ direction,
   -$$
   -    \frac{d}{d\ell} \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix}
   -        = \begin{pmatrix} \epsilon & 0 \\ 0 & \epsilon \end{pmatrix}
   -        \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix}
   -$$
   -with two positive eigenvalues.
   -->
For the WF fixed point we get
$$
    \frac{d}{d\ell} \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix}
        = \begin{pmatrix} -\epsilon & -\frac{6\epsilon}{N+8} \\ 0 & -\frac{(4-N)\epsilon}{N+8} \end{pmatrix}
        \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix} \;.
$$
Before introducing the anisotropy, the WF fixed point was stable.
Now the stability depends on the flavor number $N$.
<!--
   -I fixed point
   -$$
   -    \frac{d}{d\ell} \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix}
   -        = \begin{pmatrix} \frac\epsilon3 & 0 \\ -\frac{4\epsilon}{3} & -\epsilon \end{pmatrix}
   -        \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix}
   -$$
   -->
Similarly for the C fixed point
$$
    \frac{d}{d\ell} \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix}
        = \begin{pmatrix} -\frac{(N+8)\epsilon}{3N} & -\frac{2\epsilon}{N} \\ -\frac{4(N-4)\epsilon}{3N} & -\frac{(N-4)\epsilon}{N} \end{pmatrix}
        \begin{pmatrix} \delta \tilde{u} \\ \delta \tilde{v} \end{pmatrix}
$$
with the two eigenvalues
$$
    \lambda_C^{(1)} = -\epsilon
    \qquad \lambda_C^{(2)} = \frac{(4-N)\epsilon}{3N}
$$
This gives us already a good qualitative understanding of the RG flow.
Below a critical flavor number $N_c = 4$ (see [@fig:rg-flow-cubic-below-nc]),
the WF fixed point is stable.
The C fixed point on the other hand is repulsive and effectively separates the parameter space into two regions.
For positive or not too large negative initial values of $v$ cubic anisotropy becomes irrelevant.
For strong negative anisotropy on the other hand, the system experiences runaway flow,
where cubic anisotropy eventually destabilizes the system at the quartic level.
From a Landau theory perspective this would mean a vanishing quartic term
necessitating the inclusion of a $\phi^6$ term.
This generically describes a first order transition.

![Stability of WF fixed point below critical $N$](figures/cubic_anisotropy_figures/cubic_anisotopy_rg_flow_below_Nc_neg_v_mod_sw.pdf){#fig:rg-flow-cubic-below-nc width=80%}

Above $N_c$ (see [@fig:rg-flow-cubic-above-nc])
the C fixed point moves to the positive $v$ values and becomes stable.
The WF fixed point becomes repulsive in turn and for negative initial values of $v$
the situation is again one of runaway flow.
But as long as the initial value of $v$ is positive the system will now be attracted to C
and as a result we would observe the critical exponents associated with C.
For the correlation length exponent this means
$$
    \nu_C = \frac12 + \frac{N-1}{6N} \epsilon + \mathcal{O}(\epsilon^2) \;.
$$
Consequently for the specific heat exponent
$$
    \alpha_C = \frac{4-N}{6N} \epsilon + \mathcal{O}(\epsilon^2) \;.
$$
There is still not $Z_\phi$ at first order in $\epsilon$, therefore $\eta = 0$.

![Stable cubic fixed point above critical $N$](figures/cubic_anisotropy_figures/cubic_anisotopy_rg_flow_above_Nc_neg_v_mod_sw.pdf){#fig:rg-flow-cubic-above-nc width=80%}

In conclusion, the effect of cubic anisotropy on critical behavior depends strongly on the flavor number $N$.
Below a critical value $N_c = 4$ we can safely ignore the effect of cubic anisotropy and the critical exponents are unchanged.
However, above $N_c$ the critical behavior is dominated by a new fixed point with different critical exponents.
