target = -t latex
includes = --include-in-header inc/header.tex --include-before-body inc/pre-body.tex
draft_filters = -F pandoc-crossref
filters = -F pandoc-crossref --citeproc

thesis: config.yaml references.yaml chapter*.md appendix*.md
	pandoc $(target) $(includes) $(filters) \
		-o thesis.pdf \
		--number-sections \
		config.yaml \
		chapter*.md \
		appendix*.md

%.pdf: %.md
	pandoc $(target) $(includes) $(draft_filters) \
		--number-sections \
		-M fontsize:12pt \
		-M linestretch:1.33 \
		-M classoption:draft \
		-o $@ $<

all: thesis

.PHONY: all thesis final refs/references.bib
