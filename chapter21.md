\part{Chiral Magnetoelasticity}

# Chiral magnets and elastic coupling in the micromagnetic framework

## The continuum limit of magnetism
When describing ferromagnetism,
one often discusses a Heisenberg-type model,
with a symmetric exchange interaction.
In the case of chiral magnets, broken inversion symmetry allows also for an antisymmetric exchange,
which is called *Dzyaloshinsky-Moriya interaction* (DMI), resulting from spin orbit coupling
in second order perturbation theory [@Dzyaloshinsky_1958;@Moriya_1960].
The Hamiltonian is then of the form
$$
	H = - J \sum_{\langle mn\rangle} \S_m\cdot\S_n
		+ \sum_{\langle mn\rangle} \vec{D}_{mn}\cdot(\S_m\times\S_n) \;.
$$
A positive $J$ describes a ferromagnet, while a negative $J$ describes an antiferromagnet.
We will only consider ferromagnetic systems here.
The vector $\vec{D}_{mn}$ describes the DMI for the specific bond between the sites $m$ and $n$.

The Heisenberg exchange term favors aligned spins,
while the DMI term favors orthogonal spins, where the exact orientation depends on the vector $\vec{D}_{mn}$.
The competition between these to couplings, often together with other energy terms such as Zeeman or anisotropy,
can stabilize a variety of modulated spin textures,
such as TODO.
TODO: citations
If DMI is weak, which is to be expected in many systems (TODO: when is SOC weak?),
the length scale of the modulation becomes large.
One class of important systems, the so called B20 chiral magnets,
such as \ch{MnSi}, \ch{FeGe} and \ch{Cu_2OSeO_3},
has helices with a period of to order of $60\inu{nm}$.
TODO: length scales in other materials?
On these scales it makes little sense to consider every individual spin,
but instead we will move to a semi-classical, continuum description.

First, we interpolate spins to continuous functions of space $\S_n \to \S(\R_n)$,
with lattice site position $\R_n$.
We then suppose the distance between neighbouring spins is small
and use a series expansion in the lattice constants.
Rewriting the sum over pairs first as a sum over site and bond vectors $\nnvec$, then as an integral,
the exchange interaction becomes
$$\begin{aligned}
	\sum_{\langle mn\rangle} \S_m\cdot\S_n
		&= \frac12 \sum_{n}\sum_{\nnvec} \S(\R_n) \cdot \S(\R_n+\nnvec) \\
		%&= \frac12 \sum_{i}\sum_{\nnvec} \S(\R_i) \cdot \left(\S(\R_i)
		%	+ (\nnvec \cdot \nabla) \S(\R_i) + \frac12 (\nnvec \cdot \nabla)^2 \S(\R_i)\right) + \O(\delta^3)\\
		&= \frac{1}{2V_\mathrm{PUC}} \int d^3r \sum_{\nnvec} \left(\S(\r)^2
			 + \frac12 \S(\r) \cdot (\nnvec \cdot \nabla)^2 \S(\r)\right) + \O(\delta^3)
\end{aligned}$$
where $V_\mathrm{PUC}$ is the volume of the primitive unit cell.
The factor $1/2$ compensates the double counting of bonds
from summing over all bonds at every site.
Here, we already used the fact that the first order term vanishes, which can be seen by integrating by parts.
The quantity $\S^2$ is a partial constant that can be ignored. TODO: justification?
What is left is evaluating the sum over bonds for the specific crystal structure.
For a cubic system we find the exchange energy
TODO: $\S \to \M$, $M_s$
$$
	E_\mathrm{ex} = \int d^3r A \left(\partial_i \M(\r) \cdot \partial_i \M(\r)\right)
$$
with $A = J/(2a)$ for a simple cubic lattice with only nearest neighbour interaction
and lattice constant $a$.

For DMI we apply the analogous reasoning,
with the important distinction that the DMI vector is a function of the bond vector $\vec{D}(\nnvec)$.
We use this to define the DMI tensor $D_{ij} = D_i(\hat{e}_j)$ and write
$$\begin{aligned}
	\sum_{\langle mn\rangle} \vec{D}_{mn}\cdot(\S_m\times\S_n)
		&= \frac12 \sum_{n} \sum_{\nnvec}
			\epsilon_{ijk} D_i(\nnvec) S_j(\R_n) S_k(\R_n + \nnvec) \\
	&= \frac{1}{2V_\mathrm{PUC}} \int d^3r \sum_{\nnvec}
		\epsilon_{ijk} D_{il} S_j(\r) \partial_l S_k(\r) + \O(\delta^2)
\end{aligned}$$
where now the first order is leading.
The $0$th order term vanishes with $\S\times\S$.
We define the Lifshitz invariants
$$
	w_{ij} = \epsilon_{ikl} M_i \partial_j M_l(\r) \;.
$$
The DMI Energy can then be compactly written as
$$
	E_\mathrm{DMI} = \int d^3r D_{ij} w_{ij}
$$
<!--
 -For a simple cubic lattice we have six nearest neighbours
 -$\nnvec \in \{a\x, a\y, a\z, -a\x, -a\y, -a\z\}$.
 -->
There are different types of DMI, depending on the form of the tensor.
Assuming $D_{ij} = D\delta_{ij}$ (DMI vector parallel to bond) gives a Bloch-type coupling
$$
    E_\mathrm{Bloch}
        = \int d^3r D \tr\{w\}
        = \int d^3r D \M \cdot (\nabla \times \M)
$$
where $D$ is now just a constant number.
This is the most symmetric type of DMI, since it only breaks inversion symmetry,
but not rotational symmetry.
The sign of $D$ determines the chirality.

If on the other hand have $D_{ij} = D\epsilon_{ijk} d_k$
with some anisotropy axis $\hat{d}$, this gives a Neél-type DMI
$$
    E_\text{Neél}
        = \int d^3r D \epsilon_{ijk} w_{ij} d_k
        = \int d^3r D \left( (\M\cdot\nabla)(\hat{d}\times\M) - (\M\cdot\hat{d})(\nabla\cdot\M) \right)
$$

TODO: clarify sum convention (only used in some cases)


## The minimal micromagnetic model
TODO: chose symbol for classical energy density
$$
    E = A(\partial_i\m)^2 + D\m\cdot(\nabla\times\m) - K (\e_a\cdot\m)^2 - \h\cdot\m
$$

TODO:
* discuss phase diagram
* discuss helix and skyrmion solutions
* cite paper on $K$-$h$ diagram

## Magnetoelastic coupling

### Coupling from exchange term
In general the Heisenberg coupling depends on the bond,
or at least the distance and relative orientation of the spins
$$
    H_\text{ex} = \sum_{\langle ij \rangle} J_{ij} \S_i\cdot\S_j
        = \sum_{\langle ij \rangle} J(\r_i-\r_j) \S(\r_i)\cdot\S(\r_j)
$$
using $\r_i = \R_i + \u_i$ and rewriting $\R_j = \R_i + \nnvec$,
where $\nnvec$ goes over all nearest neighbours of the $i$th site,
we can transform the sum to an integral over $\r = \R_i$
$$
\begin{aligned}
    &\sum_{\langle ij \rangle} J(\r_i-\r_j) \S(\r_i)\cdot\S(\r_j) \\
        &= \frac12 \sum_{\R}\sum_{\nnvec} J\big(\nnvec + \u(\R)-\u(\R+\nnvec)\big) \S\big(\R+\u(\R)\big)\cdot\S\big(\R + \nnvec + \u(\R+\nnvec)\big)
        %&= \frac12 \int d^3r \sum_{\nnvec} J\big(\nnvec + \u(\r)-\u(\r+\nnvec)\big) \S\big(\r+\u(\r)\big)\cdot\S\big(\r + \nnvec + \u(\r+\nnvec)\big)
\end{aligned}
$$
<!--TODO: technically the integral is only valid after the limit of small $\nnvec$.-->
<!--
   -$$
   -\begin{aligned}
   -    J(\r_i-\r_j) &\S(\r_i)\cdot\S(\r_j)
   -        = J(\R_i-\R_j) \S(\R_i)\cdot\S(\R_j) \\
   -        &+ \{[(\u_i-\u_j) \cdot\nabla] J(\R_i-\R_j)\} \S(\R_i)\cdot\S(\R_j) \\
   -        &+ J(\R_i-\R_j)\left(
   -            [(\u_i\cdot\nabla) \S(\R_i)]\cdot\S(\R_j)
   -            + \S(\R_i) [(\u_j\cdot\nabla) \S(\R_j)]
   -        \right) \\
   -        &+ \O(u^2)
   -\end{aligned}
   -$$
   -->
TODO: maybe make picture of cube with displacement vectors.
TODO: comment on possible non-analyticity of $J$ around $0$, only $S$s are expanded in $\nnvec$

We now have two small parameters: The displacement $\u$ and the lattice constants $\nnvec$.
In analogy to the rigid lattice we can look at the different orders in $\nnvec$ to get the leading contributions to the continuum theory.
We can then expand in $\u$ to get the result of the rigid theory in $0$th order (in $\u$)
and the leading me coupling in first order.
In $0$th (in $\nnvec$) order we have
$$
    J \, S(\r+\u(\r))^2
        = J \, S(\r)^2
        + J \, (\u(\r) \cdot \nabla) S(\r)^2
        + \O(u^2)
$$
Integrating by parts and ignoring the boundary term we can rewrite
$$
    (\u(\r) \cdot \nabla) S(\r)^2 \to - (\nabla\cdot\u(\r)) S(\r)^2 = - \tr\strain(\r) S(\r)^2
$$
This is the generic strain-density coupling also discussed in the critical phenomena part.

In 1st order in $\nnvec$ we get in the rigid term
$$
        J \, \S \cdot (\nnvec \cdot \nabla) \S
$$
which vanishes under the integral as can be seen by using integration by parts.
In the linear $\u$ term we get
$$
        J \, \S \cdot (\nnvec \cdot \nabla) \S
$$

In the $2$nd order (in $\nnvec$) we get in the rigid limit ($0$th order in $\u$)
TODO: this assumes already that $J(\nnvec) = J$ for all $\nnvec$
$$
    \frac{J}{2} \delta_\alpha\delta_\beta \S(\r)\cdot \partial_\alpha\partial_\beta \S(\r)
$$
This is just the previously discussed rigid exchange energy.
To go further we have to assume some lattice symmetry/nearest neighbours.
For cubic crystal lattices (checked for sc, bcc or fcc) lattice for example we have
<!--
   -$$
   -    \sum_{\nnvec} \delta_\alpha\delta_\beta \partial_\alpha\partial_\beta = 2a^2\Delta
   -$$
   -because
   -->
$$
    \sum_{\nnvec} \delta_\alpha\delta_\beta = 2a^2\delta_{\alpha\beta}
$$
Then in first order in $\u$ we get two terms that go with the derivative of $J$
$$
    \frac12 \left(
        -\delta_\rho \partial_\rho u_\lambda \partial_\lambda J \delta_\sigma \partial_\sigma S^2
        -\delta_\rho \delta_\sigma \partial_\rho \partial_\sigma u_\lambda \partial_\lambda J S^2
    \right)
$$
which cancel up to a boundary term.
The remaining terms are
$$
\begin{aligned}
    &\frac{J}{2} \delta_\rho \delta_\sigma \big(
        u_\lambda \partial_\lambda \S \cdot \partial_\rho \partial_\sigma \S
        + 2 \partial_\rho u_\lambda \S \cdot \partial_\sigma \partial_\lambda \S
        + \partial_\sigma \partial_\rho u_\lambda \S \cdot \partial_\lambda \S
        + u_\lambda \S \cdot \partial_\sigma \partial_\rho \partial_\lambda \S
    \big) \\
    &\quad\to ~ J \big(
        - \partial_\lambda u_\lambda \S \cdot \Delta \S
        + \partial_\rho u_\lambda (\S \cdot \partial_\rho \partial_\lambda \S
        - \partial_\rho \S \cdot \partial_\lambda \S)
    \big)
\end{aligned}
$$


In the end we get
$$
\begin{aligned}
    H_\text{ex} = \int d^3r \Big(&
        J(1 - \tr\strain) S^2
        + \iota_{ij} \strain_{ij} S^2 \\
        &+ \frac{Ja^2}{2} \big[
            (1 - \tr\strain) \S\cdot\Delta\S
            + \strain_{ij} (\S\cdot\partial_i\partial_j\S - \partial_i\S\cdot\partial_j\S)\big]
    \Big)
\end{aligned}
$$
with $\iota_{ij} = \sum_{\nnvec} (\delta_i \partial_j J + \delta_j \partial_i J)/2$.
Later, when we impose $m^2 = 1$ we can safely ignore the first line in this equation.

### Coupling from DMI term
$$
    H_\text{DMI}
        = \sum_{\langle ij \rangle} \vec{D}_{ij} \cdot (\S_i \times \S_j) \\
        = \sum_{\langle ij \rangle} \vec{D}(\r_i - \r_j) \cdot \big(\S(\r_i) \times \S(\r_j)\big) %\\
        %= \int d^3r \mathcal{H}_\text{DMI}
$$
$\r_i = \R_i + \u_i$, $\R_j = \R_i + \nnvec$ and $\u_j = \u(\R_i + \nnvec)$,
$$
\begin{aligned}
    H_\text{DMI} = \frac12 \sum_{\R} \sum_{\nnvec}
            \epsilon_{\alpha\beta\gamma} &D_\alpha\big(\nnvec + \u(\R)-\u(\R+\nnvec)\big) \\
                &S_\beta\big(\R+\u(\R)\big) S_\gamma\big(\R + \nnvec + \u(\R + \nnvec)\big))
\end{aligned}
$$
Expansion in $\nnvec$ and $\u$

* $0$th order in $\nnvec$: $\propto \S(\r+\u)\times\S(\r+\u) = 0$
* $1$st order in $\nnvec$
    - $\u = 0$ term:
      $$
        \epsilon_{\alpha\beta\gamma} D_\alpha(\nnvec) S_\beta(\r) (\nnvec\cdot\nabla) S_\gamma(\r)
      $$
      for $\vec{D}(\nnvec) \parallel \nnvec$, let's say $D_\alpha(\nnvec) = D\delta_\alpha/a$ (already done last section)
      $$
        \sum_{\nnvec} \frac{D}{a} \epsilon_{\alpha\beta\gamma} \delta_\alpha
                S_\beta(\r) \delta_\lambda \partial_\lambda S_\gamma(\r)
            = Da \epsilon_{\alpha\beta\gamma}
                S_\beta(\r) \partial_\alpha S_\gamma(\r)
      $$
    - linear $\u$ term: $\propto \tr\strain \S\cdot(\nabla\times\S)$ for $\vec{D}(\nnvec) \parallel \nnvec$?
      one term with $D$ derivative
      $$
        \epsilon_{\alpha\beta\gamma} u_\lambda\partial_\lambda D_\alpha(\nnvec) S_\alpha S_\gamma = 0
      $$
      vanishes because its $\S\times\S$. The remaining terms are
      $$
        \u\cdot\nabla S_\beta \nnvec\cdot\nabla S_\gamma
        + S_\beta (\nnvec\cdot\nabla)\u\cdot\nabla S_\gamma
        + S_\beta \nnvec\cdot\nabla(\u\cdot\nabla S_\gamma)
      $$
      we can use Integration by parts in the hamilonian density (and ignore the surface term) to do the following
      $$
        u_\lambda\partial_\lambda S_\beta \delta_\rho\partial_\rho S_\gamma
        \to  - S_\beta \partial_\lambda (u_\lambda\delta_\rho\partial_\rho S_\gamma)
      $$
      we obtain
      $$
        \sum_{\nnvec} D_\alpha(\nnvec) \delta_\rho \epsilon_{\alpha\beta\gamma} (
            - \partial_\lambda u_\lambda S_\beta \partial_\rho S_\gamma
            + \partial_\rho u_\lambda S_\beta \partial_\lambda  S_\gamma
        )
      $$
      Assuming again $D_\alpha(\nnvec) = D\delta_\alpha/a$ we get
      $$
        \sum_{\nnvec} D_\alpha(\nnvec) \delta_\beta = Da \delta_{\alpha\beta}
      $$
* $2$nd order in $\nnvec$
    - $\u = 0$ term: $\delta_\lambda\delta_\rho [\partial_\lambda\partial_\rho (D_\alpha(\nnvec) S_\beta(\r) S_\gamma(\r + \nnvec))]_{\nnvec\to0}$

Result to first order in lattice constant
$$
    H_\text{DMI} = \int d^3r Da \Big(
        (1 - \tr\strain) \S\cdot (\nabla\times\S) + \strain_{ij} w_{ij}
    \Big)
$$
where we defined
$$
    w_{ij} = \epsilon_{ikl} S_k \partial_j S_l
$$


other TODOs:

* Continuum limit with volume $V$, is $H$ energy per unit cell or whole crystal? How are final couplings $J$ and $D$ related to microscopic ones?
* Phenomenological derivation of interaction based on point group
    - Orders in spin orbit coupling as "small" parameter expansion
    - Easy way to get $\strain \lambda mm$ term

TODO: form of $\lambda$, $\mu$ and $\nu$ tensors

### Classifictation of magnetoelastic couplings
All up to second order in spin orbit coupling

Magnetostriction
$$
	E_\mathrm{me} = \strain_{ij} \lambda_{ijkl} m_k m_l
$$
For a cubic chiral magnet with point group 23 the tensor $\lambda$ has four independent components.
However, if we assume $m^2 = 1$,
we can reduce the number of components that actually contribute to three TODO
$$
    \lambda^{V} = \begin{pmatrix}
        \lambda_{11} & \lambda_{12} & -\lambda_{12} & 0 & 0 & 0 \\
        -\lambda_{12} & \lambda_{11} & \lambda_{12} & 0 & 0 & 0 \\
        \lambda_{12} & -\lambda_{12} & \lambda_{11} & 0 & 0 & 0 \\
        0 & 0 & 0 & \lambda_{44} & 0 & 0 \\
        0 & 0 & 0 & 0 & \lambda_{44} & 0 \\
        0 & 0 & 0 & 0 & 0 & \lambda_{44}
    \end{pmatrix} \;.
$$
Cite [@Callen_1965]

This type of ME coupling is the most commonly used one because it is usually the strongest,
because it is the lowest in a gradient expansion.

Magnetoelastic DMI
$$
	E_{edmi} = \strain_{ij} \mu_{ijkl} w^s_{kl} + \mu_a (\strain_{12} w^a_{12} + \strain_{23} w^a_{23} + \strain_{31} w^a_{31})
$$
where we introduced the symmetrized and antisymmetrized Lifshitz invariants,
$w^s_{ij} = (w_{ij} + w_{ji})/2$ and $w^a_{ij} = (w_{ij} - w_{ji})/2$.
Cite [@Kitchaev_2018] for DMI type symmetry analysis

Magnetoelastic exchange
$$
	E_{eex} = \strain_{ij} \nu_{ijkl} \partial_k \m \cdot \partial_l \m
$$
with four independent parameters in space group 23.


There is another type of ME coupling that is only relevant at surfaces,
called magnetorotation.
Magnetorotation couples to the antisymmetrized displacement gradient [@eq:elastic-rotations],
describing local rotations.
$$
	E_{mrot} = \lambda_r (\omega_{12} m_1 m_2 + \omega_{23} m_2 m_3 + \omega_{31} m_3 m_1)
$$
cite [@Maekawa_1976].
In general there will be also a rotational version of the coupling to Lifshitz invariants
as well as the exchange-type terms.
However, since there is no interest in these (TODO: why not?) we will not mention them further.

### Chiral magnetostriction
The above theory suggests that in a chiral magnet
even in the absence of microscopic variations of the magnetization
there can be nontrivial effect of magnetoelastic coupling
on the static configuration of the system.

For strong enough external magnetic fields (above a critical field $H_{c2}$)
the magnetic state will be a constant polarized field.
In this scenario the only non-vanishing term of the above model is the elastic anisotropy
and it then only contains the spatially constant part of the strain
$$
    \mathcal{H}_\mathrm{int} = E_{ij} \lambda_{ijkl} m_k m_l \;.
$$
which is minimized by the expression in [@eq:macro-strain-solution].
Assuming for example a constant magnetization $\m = (0, 0, 1)$
as a result of a magnetic field applied in $[001]$ direction,
we obtain in the following strain
$$
\begin{aligned}
    E_{11} &= \frac{C_{12} \lambda_{11} + (C_{11} + 2C_{12}) \lambda_{12}}{C_{11}^2 + C_{11} C_{12} - 2 C_{12}^2} \\
    E_{22} &= \frac{C_{12} \lambda_{11} - (C_{11} + 2C_{12}) \lambda_{12}}{C_{11}^2 + C_{11} C_{12} - 2 C_{12}^2} \\
    E_{33} &= - \frac{(C_{11} + C_{12}) \lambda_{11}}{C_{11}^2 + C_{11} C_{12} - 2 C_{12}^2}
\end{aligned}
$$
while the three shear strain components vanish.
Notice that the strains in $x$ and $y$ direction differ by the sign of the $\lambda_{12}$ term.
The effect of magnetostriction is not only a tetragonal distortion,
as would be excepted for centrosymmetric magnets,
but an orthorhombic distortion.
In any reasonable material this would be forbidden by symmetry,
but in an anisotropic chiral magnet,
because of simultaneously broken inversion and rotational symmetry this is allowed.
Interestingly, because the interaction is quadratic in magnetization,
inverting the direction of the magnetic field does not change the strain at all.

![Schematic picture of the chiral magnetostriction effect (exaggerated).
  Left is the unpolarized and unstrained crystal,
  in the middle the polarization is along one of the crystal axes
  and the strain breaks chiral symmetry,
  on the right the polarization is along the $[111]$ axis,
  the strain is not chiral since not crystal axis is favored.](figures/chiral_magnetostiction.svg)


If the magnetic field is instead applied along the $[111]$ direction,
this chiral effect on magnetostriction vanishes,
resulting in a rhombohedral distortion.
This is reasonable since none of the crystal axes has a distinguished role.

In contrast, for polarization in the $[110]$ direction or directions of lower symmetry,
a discrepancy in the in plane strain is excepted trivially,
even in the absence chiral magnetoelastic anisotropy,
since e.g. a field in the $[110]$ direction leads to a monoclinic distortion.
But only in the special case of a chiral magnet, will the effect persist even in the $[001]$ direction.


### Helix orientation in chiral magnetoelastic potential
<!--
   -* Motivation: In helical state eAniso induces a helical displacement
   -    * twice the period bc quadratic coupling
   -* Idea: Helix reorientation is determined by effective potential with non-analytic coupling
   -* Method: Integrating out displacement field
   -* Results
   -->
In the case of a magnetic helix the elastic anisotropy term $m_i m_j$ depends on space coordinates,
which means that the induced displacement field is nontrivial.
For elastic DMI and elastic exchange it turns out that $w_{ij}$
and $\partial_i\m \cdot\partial_j\m$ are all constant in space for a helical magnetization.
As a result they only contribute on the level of macroscopic strain.
<!--
   -Assuming that the resulting distortion of the unit cell is small enough
   -that the anisotropy introduced as a renormalization of the exchange and DMI term can be neglected
   - TODO: check this assumption
    - correct thing to do might be to calculate E, renormalize D, A, and …
    - Conjecture: this is equivalent to including mixed terms \lambda\nu \lambda\mu
   -->
The elastic anisotropy however induces a helical displacement with wavevector $2\q$
where $\q$ is the wavevector of the magnetic helix.
$$
    \m(\r) = \sin(\q\cdot\r) \e_2 + \cos(\q\cdot\r) \e_3
$$ {#eq:helix-ansatz}
in an orthonormal basis where $\e_1 \parallel \q$ and $\e_2,\e_3 \perp \q$.
<!--
   -TODO: write down displacement field modes?
   -$$
   -    \u(\r) = \dots
   -$$
   -->
There are two distinct modes of the displacement field that are induced by the magnetic helix:
A circular polarized displacement wave that is most prominent in the $[111]$ direction but vanishes in the $[100]$ direction
<!--The phase is changed by the competition between $\lambda_{12}$ and $\lambda_a$.-->
with amplitude $A = \sqrt{\lambda_{12}^2 + \lambda_a^2}$ and phase $\phi = \arg(\lambda_{12}i + \lambda_a)$
and a longitudinal displacement wave that only exists for $\lambda_{12} \neq 0$ and vanishes in the $[111]$ direction.

These directions dependent lattice distortions leads us to expect interesting effects on the helix orientation.
Therefore we use the ansatz in [@eq:helix-ansatz]
for an arbitrary wavevector $\q = Q(\sin\psi \cos\chi, \sin\psi \sin\chi, \cos\psi)$
to get the magnetoelastic energy as a function of helix orientation.
$$
    \mathcal{V}(\psi, \chi) = \int d^3r \; \mathcal{H}(\psi, \chi)
$$
For simplicity we assume that the harmonic part of the lattice potential is isotropic,
i.e. $A = 2C_{44}/(C_{11} - C_{12}) = 1$.
The result is shown graphically in [@fig:helix-potential] and can be written in terms of spherical harmonics.
$$
\begin{aligned}
    \mathcal{V}(\psi, \chi) &=
        \lambda_a^2\Big(f^a_{40} Y_{40}(\psi,\chi) + f^a_{44} [Y_{44}(\psi,\chi) + Y_{4-4}(\psi,\chi)] \\
            &\qquad + f^a_{60} Y_{60}(\psi,\chi) + f^a_{64} [Y_{64}(\psi,\chi) + Y_{6-4}(\psi,\chi)] \\
            &\qquad + f^a_{80} Y_{80}(\psi,\chi) + f^a_{84} [Y_{84}(\psi,\chi) + Y_{8-4}(\psi,\chi)] \\
            &\qquad + f^a_{88} [Y_{88}(\psi,\chi) + Y_{8-8}(\psi,\chi)]\Big) \\
        &+ \lambda_a\lambda_{12} \Big(f^m_{62} [Y_{62}(\psi,\chi) + Y_{6-2}(\psi,\chi)] \\
            &\quad\qquad + f^m_{66} [Y_{66}(\psi,\chi) + Y_{6-6}(\psi,\chi)]\Big) \\
        &+ \lambda_{12}^2\Big(f^c_{40} Y_{40}(\psi,\chi) + f^c_{44} [Y_{44}(\psi,\chi) + Y_{4-4}(\psi,\chi)] \\
            &\qquad + f^c_{60} Y_{60}(\psi,\chi) + f^c_{64} [Y_{64}(\psi,\chi) + Y_{6-4}(\psi,\chi)]\Big) \\
        &+ \mathrm{const.}
\end{aligned}
$$
<!--TODO: coeffients in appendix?-->
<!--where $c = \sqrt{\pi}/(C_{11}C_{44})$.-->

The most interesting feature of this potential is that the chiral magnetoelastic coupling
has a clear preference for a helix in the $[110]$ direction.
<!--
   -This is unusual since it can not be produced by a simple cubic anisotropy.
   -Rather it is an effect of the non-analytic form of the effective coupling [@eq:effective-anisotropy]
   -in combination with the non-centrosymmetric nature of $\lambda$.
   -->
<!--TODO: this is wrong, the function is analytic, what is real reason for [110] minimum?-->

<!--A helix oriented in the $[110]$ direction has been observed in iron doped $\mathrm{MnSi}$\cite{Kindervater_2020}.-->
<!--There are also other examples of effects that prefer the $[110]$ direction\cite{Pfleiderer_2004}.-->
<!--TODO: phrasing-->

* Effective potential for helix $Q$
    * Minimum at $[111]$ direction for $\lambda_{12} < \lambda_{11} - 2\lambda_{44}$

$$
\begin{aligned}
    \lambda_{12} &\equiv \lambda_0 \sin\xi \\
    \lambda_a &\equiv \lambda_0 \cos\xi
\end{aligned}
$$

![Effective helix potential from lattice distortions for different values of the coupling parameters.
  (a) For purely centrosymmetric coupling ($\lambda_{12} = 0$) the minimum is in the $[111]$ direction like in $\mathrm{MnSi}$.
  (b) For mixed coupling the potential has a chiral twist.
  (c) For purely chiral coupling ($\lambda_a = 0$) the minimum is in $[110]$ direction.](figures/helix_potential.svg){#fig:helix-potential width=100%}

Effect of eDMI and eEx ($w^s_{ij} = \mathrm{const.}$, $w^a_{ij} = 0$)
<!--
   -$$
   -\begin{gathered}
   -    \frac{\Lint^\mathrm{DM} + \Lint^\mathrm{ex}}{V} = \gamma_{11} (q_1^4 + q_2^4 + q_3^4) \\
   -        + (\gamma_{12} + \gamma_{21} + 4 \gamma_{44}) \left(q_1^2 q_2^2 + q_2^2 q_3^2 + q_2^2 q_1^2\right)
   -\end{gathered}
   -$$
   -where
   -$$
   -    \gamma = \frac{1}{Q^2}\; \mu^s : C^{-1} : \mu^s + \nu : C^{-1} : \nu
   -$$
   -->
