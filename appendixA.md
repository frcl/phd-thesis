\appendix

# Calculation of self-energy diagram with transversal and longitudinal projectors {#sec:appendix-self-energy-diagram}

TODO: put ps and qs in vector notation

In the calculation of the self-energy correction for the isotropic ferroelectric with elastic coupling
in [@sec:rg-isotropic-ferroelectric] the following diagram appears.
\def\DiagramScale{1.2}
$$
    \diagramPhiSelfEnergyLLlabeled{i}{j}{k}{l}{i'}{j'}{k'}{l'}{p} %{q}
$$
It resolves to the expression
$$
    \Sigma_{kk'}(\nu, p) = \frac4\rho \lambda_{ijkl} \lambda_{i'j'k'l'}
        \int_{q,\omega} G^0_{ll'}(p-q,\nu-\omega) q_i D^0_{jj'}(q,\omega) q_{i'} \;.
$$
up to a combinatorial factor.
The dependence of the integral on the external momentum
is complicated by the transversal projector in the Green's function
$$
   G^0_{ij}(q, \omega) = \frac{\delta_{ij} - \hat{q}_i \hat{q}_j}{\omega^2 + c_0^2q^2 + r_0} \;.
$$
We will therefore need to calculate products of projectors on different momentum vectors such as
$$
    \left(\delta_{ij} - \frac{(p-q)_i (p-q)_j}{(p-q)^2}\right)
        \left[\frac{q_kq_l}{q^2} D_l(q,\omega) + \left(\delta_{kl} - \frac{q_k q_l}{q^2}\right) D_t(q,\omega)\right]
$$
However, this also simplifies the calculation somewhat,
since the self energy correction also split into longitudinal and transversal part
$$
    \Sigma_{ij}(\nu, p) = \sigma_l(\nu, p) \hat{p}_i \hat{p}_j
        + \sigma_t(\nu, p) (\delta_{ij} - \hat{p}_i \hat{p}_j) \;,
$$
and we are only interested in the correction to the transversal modes.
Therefore we extract the scalar function $\sigma_t$ using the relation
$$
    \tr\Sigma(\nu, p) - \hat{p}_i \Sigma_{ij}(\nu, p) \hat{p}_j = (d-1)\sigma_t(\nu, p) \;.
$$
We will then assume that we can ignore the longitudinal mode and only renormalize the transversal one
$$
    Z_\phi^{-1}(\omega^2 + c^2 q^2 + r) = \omega + c_0^2 q^2 + r_0 - \sigma_t(\omega, q) \;.
$$ {#eq:phi-prop-renorm}
Working out the algebra we find
$$
\begin{aligned}
    \sigma_t(\nu, p)
        &= \frac4\rho \int_{q,\omega} \frac{q^2 }{(\nu - \omega)^2 + c^2(p-q)^2 + r}
            \bigg[\frac{f_l(p,q)}{\omega^2 + c_l^2 q^2}
            + \frac{f_t(p,q)}{\omega^2 + c_t^2 q^2} \bigg]
\end{aligned}
$$
with
$$
\begin{aligned}
    f_l(p,q) &= \frac{\lambda_1^2}{d - 1} \left(d - 1 + \frac{q^2}{(p-q)^2}
            \left((\hat{p}\cdot\hat{q})^2 - 1\right) \right) \\
        &\quad+ \frac{4\lambda_1\lambda_2}{d - 1} \left(\frac{q^2 - (p\cdot q)}{(p-q)^2} - 1\right)
            \left((\hat{p}\cdot\hat{q})^2 - 1\right) \\
        &\quad+ \frac{4\lambda_2^2}{d-1} \left(\frac{p^2((\hat{p}\cdot \hat{q})^2 - 1) + (p-q)^2}{(p-q)^2} - 1\right)
                \left((\hat{p}\cdot\hat{q})^2 - 1\right) \\
    f_t(p,q) &= \frac{\lambda_2^2}{d-1} \bigg(
        \frac{(\hat{p}\cdot\hat{q})^2 [p^2(8 - d - 4(\hat{p}\cdot\hat{q})^2) + q^2] + (d-4)p^2 - q^2}{(p-q)^2} \\
        &\qquad\qquad\quad+ d\left(1 - (\hat{p}\cdot\hat{q})^2\right)\bigg)
\end{aligned}
$$
However, since the problem is spherically symmetric,
$\sigma_t$ can not depend on the direction of external momentum $\hat{p}$.
We can therefore choose $p$ to be in $z$-direction such that $\hat{p}\cdot\hat{q} = \cos\theta$.
<!--and write $f_{l/t}(\hat{p}\cdot\hat{q}) = f_{l/t}(\cos\theta)$.-->
$$
\begin{aligned}
    f_l(p,q,\theta) &= \frac{\lambda_1^2}{d - 1} \left(d - 1 - \frac{q^2}{p^2 + q^2 - 2pq\cos\theta}
            \sin^2\theta \right) \\
        &\quad-\frac{4\lambda_1\lambda_2}{d-1} \left(\frac{q^2 - pq\cos\theta}{p^2 + q^2 - 2pq\cos\theta}
            - 1\right) \sin^2\theta \\
        &\quad-\frac{4\lambda_2^2}{d-1} \left(\frac{p^2\cos^2\theta + q^2 - 2pq\cos\theta)}{p^2 + q^2 - 2pq\cos\theta} - 1\right)
                \sin^2\theta \\
    f_t(p,q,\theta) &= \frac{\lambda_2^2}{d-1} \bigg(\frac{\cos^2\theta
        [p^2(8 - d - 4\cos^2\theta) + q^2]
            + (d-4)p^2 - q^2}{p^2 + q^2 - 2pq\cos\theta} \\
                &\qquad\qquad\quad+ d\sin^2\theta\bigg)
\end{aligned}
$$
Assuming that $\sigma_t$ already has the form required by [@eq:phi-prop-renorm] we can simplify the calculation
by expanding in $\nu$, $p$ and $r$ separately
$$
    \sigma_t(p,\nu)
        \sim \frac12\frac{\partial^2\sigma_t}{\partial \nu^2}\bigg|_{\substack{\nu=0\\p=0\\r=0}} \;\nu^2
        + \frac12\frac{\partial^2\sigma_t}{\partial p^2}\bigg|_{\substack{\nu=0\\p=0\\r=0}} \;p^2
        + \frac{\partial\sigma_t}{\partial r}\bigg|_{\substack{\nu=0\\p=0\\r=0}} \;r
$$
at least at the level of logarithmic divergences.
Keeping in mind that the self energy is *subtracted* from the bare propagator,
we can then identify
$$
\begin{aligned}
     1 - Z_\phi^{-1} &= \frac12\frac{\partial^2\sigma_t}{\partial \nu^2}\bigg|_{\substack{\nu=0\\p=0\\r=0}}
        = \frac4\rho \int_{q,\omega} \frac{q^2(3\omega^2 - c^2 q^2)}{(\omega^2 + c^2 q^2)^3}
            \bigg[\frac{f_l(0,q,\theta)}{\omega^2 + c_l^2 q^2}
            + \frac{f_t(0,q,\theta)}{\omega^2 + c_t^2 q^2} \bigg] \\
    r_0 - r &= \frac{\partial\sigma_t}{\partial r}\bigg|_{\substack{\nu=0\\p=0\\r=0}}
        = -\frac4\rho \int_{q,\omega} \frac{q^2}{(\omega^2 + c^2 q^2)^2}
            \bigg[\frac{f_l(0,q,\theta)}{\omega^2 + c_l^2 q^2}
            + \frac{f_t(0,q,\theta)}{\omega^2 + c_t^2 q^2} \bigg]
\end{aligned}
$$
For the $\nu$ and $r$ derivatives we notice $f_{l/t}(0,q,\theta) = f_{l/t}(\theta)$ and write
$$
\begin{aligned}
    f_l(\theta) &= \lambda_1^2 \left(1 - \frac{\sin^2\theta}{d - 1} \right) \\
    f_t(\theta) &= \lambda_2^2 \sin^2\theta
\end{aligned}
$$
With this we can straightforwardly calculate the integrals
and combine everything to get
$$
\begin{aligned}
    Z_\phi^{-1} &= 1 + \frac{16K_4}{3\rho} \left[
        \frac{3d-5}{2(d-1)} \frac{\lambda_1^2}{cc_l(c+c_l)^3}
            + \frac{\lambda_2^2}{cc_t(c+c_t)^3}
    \right] \log b \\
    r &= Z_\phi b^{-2z} \bigg(r
        - \frac{K_4}{6c^3} \left(d + 1 - \frac2d\right) u r \log b \\
        &\qquad\qquad+ \frac{8K_4}{3\rho} \left[
        \frac{3d-5}{2(d-1)} \frac{2c + c_l}{c^3c_l(c+c_l)^2}\lambda_1^2
            + \frac{2c + c_t}{c^3c_t(c+c_t)^2}\lambda_2^2
    \right] r \log b\bigg)
\end{aligned}
$$
Only for the $c$ correction we have to actually calculate derivatives of $f_{l/t}$ as well
$$
    c_0^2 - c^2 = \frac12\frac{\partial^2\sigma_t}{\partial p^2}\bigg|_{\substack{\nu=0\\p=0\\r=0}}
        = \frac2\rho \int_{q,\omega} \left(\frac{\partial^2}{\partial p^2}\frac{q^2 }{\omega^2 + c^2 (p-q)^2}
            \bigg[\frac{f_l(p,q,\theta)}{\omega^2 + c_l^2 q^2}
            + \frac{f_t(p,q,\theta)}{\omega^2 + c_t^2 q^2} \bigg]\right)_{p=0}
$$
As a result we get
$$
\begin{aligned}
    c^2 = Z_\phi b^{2-2z} \bigg(c^2& - \frac{8K_4}{15\rho(d-1)}
        \bigg[\frac{(5d-9) c - 2 c_l}{c_l(c+c_l)^3}\lambda_1^2 \\
            &+ \frac{8}{cc_l(c+c_l)^2} \left((c+2c_l)\lambda_1 + 4 (c+c_l)\lambda_2\right)\lambda_2 \\
            &+ 2\frac{(4d - 18)c^2 + (7d-33) cc_t + (4d-16)c_t^2}{cc_t(c+c_t)^3}\lambda_2^2\bigg] \log b\bigg)
\end{aligned}
$$
Finally, we arrive at the RG flow equations for the parameters of the propagator
of the longitudinal optical mode:
$$
\begin{aligned}
    \frac{dZ_\phi}{d\ell} &= -\frac{16K_4}{3\rho} \left[\frac{\lambda_1^2}{cc_l(c+c_l)^3}
        + \frac{\lambda_2^2}{cc_t(c+c_t)^3} \right] \\
    \frac{dc^2}{d\ell}
        &= - \frac{8K_4}{15\rho} \bigg[\frac{13 c - c_l}{c_l(c+c_l)^3}\lambda_1^2
            + \frac{4}{cc_l(c+c_l)^2} \big((c+2c_l)\lambda_1 + 4 (c+c_l)\lambda_2\big)\lambda_2 \\
            &\qquad\qquad\qquad+ \frac{4(c^2 - 3 c c_t - c_t^2)}{cc_t(c+c_t)^3}\lambda_2^2\bigg] \\
    \frac{dr}{d\ell}
        &= \bigg(2 - \frac{5K_4}{9c^3} u + \frac{8K_4}{3\rho} \bigg[
                \frac{3c+c_l}{c^3(c+c_l)^3} \lambda_1^2
                + \frac{3c+c_t}{c^3(c+c_t)^3}\lambda_2^2\bigg] \bigg) r \\
\end{aligned}
$$
