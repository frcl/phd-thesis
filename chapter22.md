# Magnetoelastic effects on chiral surface twists

## Surface twist in the field polarized/conical/helical state

$$
    \mathcal{H} = A(\partial_i\m)^2 + D\m\cdot(\nabla\times\m) - K (\e_a\cdot\m)^2 - \h\cdot\m
$$ {#eq:minimal-micromagnetic-model}

* $\h \parallel \e_a \parallel \y$, $\s \parallel \x$
* Ansatz
  $$
    \m = (\sin\theta(x), \cos\theta(x), 0)^T
  $$
  Can be written as rotation matrix times bulk state $R_{\s}(\theta(\s\cdot\r))\m_0$.
* if $K = 0$ then sin-Gordon-equation
  $$
    2A\theta'' = h \sin\theta
  $$
  BCs $\theta'(0) = D/(2A)$, $\theta(x\to\infty) = 0$
  $$
    \theta(x) = 4\arctan\left( e^{-\sqrt{\frac{h}{2A}} (x-x_0)} \right)
  $$
  where $x_0 = - \sqrt{h/(2A)} \acosh(2\sqrt{2Ah/D^2})$
* if $K \neq 0$ then double sine-Gordon-equation
  $$
    2A\theta'' = h \sin\theta + K \sin^2\theta
  $$
  see Müller et al.
* Something about bound magnon states?

### Surface twist in the helical phase
* $h = 0$, $\s \parallel \z$, maybe $\e_a \parallel \x$, $K < 0$ to stablize helix?
* ansatz
  $$
    \m = (\sin\chi(z), \sin\chi(z)\cos(Q x), \sin(Qx))^T
  $$
  Can be written again in rotation matricies $R_{\s}(\theta(\s\cdot\r)) R_{\q}(\theta(\q\cdot\r)) \m_0$.
  $m_0$ here is a constant vector tilted by cone angle, e.g. $\m_0 = (\cos\theta, \sin\theta, 0)^T$.
* Result is again a sine-Gordon-equation
  $$
    2A\chi'' = h\sin\chi
  $$
* TODO: when using conical ansatz this can include the FP state as the $\theta = 0$ case,
  maybe merge sections
* TODO: maybe cite marias thesis?

* cite: Butenko et al. 2010?
    * no this should be cited in the magnetic phase diagram section

### Magnetoelastic effect on the helix surface twist
* maybe analytical solution possible?

## Skyrmion surface twist
* $\s \parallel \h \parallel \e_a \parallel \z$
* Ansatz: triple-q-state
  $$
    \m(\r) = \sum_q \left( (\hat{\q} \sin\chi(z) + \z\times\hat{\q} \cos\chi(z)) \sin(\q\cdot\r)
        + \z \cos(\q\cdot\r) \right)
  $$ {#eq:triple-q-state}
  TODO: check if definition of $\chi$ fits convention
* Result is again a sine-Gordon-equation
  $$
    2A\chi'' = 2 D Q \sin\chi
  $$
  BC is $\chi'(0) = -D/(2A)$
* Assuming $Q = D/(2A)$ we get
  $$
    \chi'' = 2 Q^2 \sin\chi
  $$ {#eq:helicity-sine-gordon}
* Solution goes like
  $$
    \chi(z) = 4\arctan\left( e^{-\sqrt2Q(z-z_0)} \right)
  $$
  with a length scale $L_\text{twist} = 1/(\sqrt2Q)$
* Here the twist is independent of the magnetic field. This is because
  the twist is pruely in plane for Bloch-type DMI and, contrary to the FP and
  helix setup the magnetic field is perpendicular to the surface.

If we are only interested in the length scale of changes in the helicity,
in the above example $\sqrt{2}Q$,
it is interesting to note that this scale was already encoded in the equation of state [@eq:helicity-sine-gordon],
which is valid everywhere in the bulk, not just close to the surface.
The surface merely acts as a perturbation, that decays according to the bulk physics.
Practically, it is therefore not necessary to solve [@eq:helicity-sine-gordon] exactly,
to extract the relevant length scale as will be shown below.
Insead we imagine an infinitesimal perturbation of the helicity somewhere along the skyrmion string in the bulk of the system
and ask how does this perturbation decays along the string?
<!--The answer is simply given by the poles of the correlation function.-->

We can use the ansatz [@eq:triple-q-state] to reduce the energy funtional [@eq:minimal-micromagnetic-model] to an effective one-dimensional theory
$$
    \int d^3r \mathcal{H} = \int dz \left((\partial_z\chi)^2 - Q\partial_z\chi + 4Q^2\cos\chi\right) + \text{const.}
    % TODO: check factors
$$
For a samll perturbation of the helicity from its bulk value (Bloch),
we can expand the energy in $\chi$ and focus on the quadradic part of it.
$$
    \int dz \left((\partial_z\chi)^2 - 2Q^2\chi^2\right)
$$
at this point---since we are in the bulk---we can simply fourier transform and obtain
$$
    \int dq \left(q^2 + 2Q^2\right) \chi_q \chi_{-q}
$$
So the correlation function
$$
    \langle \chi_q \chi_{-q} \rangle = \frac{1}{q^2 + 2Q^2}
$$
has a pole at $q = \pm i L_\text{twist}^{-1} = \pm i\sqrt2 Q$.
This will come in handy when adding other energy terms to the above theory,
since now the influce on the length scale can found by inserting the ansatz,
exanding in $\chi$ and Fourier-transforming said terms.

Comparison with experiment
* Zhang et al 2018
* van der Laan 2021
* more literature research

### Interfacial DMI
$$
    \mathcal{H} = s_i \epsilon_{ijk} w_{jk} = m_3 \partial_\alpha m_\alpha - m_\alpha \partial_\alpha m_3
    % TODO: check
$$
* Results of ubermag notebook

## Effect of magnetostatics on surface twists
???
* Result of oommf/mumax3 calculation
* TODO: adapt reciprocal space calculation frm
* TODO: calculate analytically contribution to bulk $\chi$ correlation function? maybe its integrodiff
